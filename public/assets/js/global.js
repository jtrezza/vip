$(document).ready(function(){
	$('.datepicker').datepicker({
        format:'yyyy-mm-dd'
    }).on('changeDate',function(obj){
        $(this).datepicker('hide');
    });

    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		return $(this).ekkoLightbox();
	});
});

function cambioOficina()
{
	var oficina = $('#oficina_id').val();

	$('#closer_id').html('<option value="0">TODOS</option>');
	if(oficina != '0'){
		$.ajax({
			url:'closers_by_office',
			type:'get',
			data:{oficina:oficina},
			dataType:'html',
			success:function(r)
			{
				$('#closer_id').html(r);
			}
		});
	}

}