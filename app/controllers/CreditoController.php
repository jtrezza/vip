<?php

class CreditoController extends \BaseController {

    /**
     * Muestra un formulario para hacer una búsqueda de
     * créditos por la cédula del propietario
     * @return mixed
     */
    public function searchByDoc()
    {
        return View::make('creditos/search_by_doc', array(
            'activo'=>'empresa'
        ));
    }

    public function searchNomina()
    {
    	$oficinas_a = Oficina::oficinasArray();
    	$oficinas = array(0=>'TODAS');
    	foreach($oficinas_a as $i => $oa){
    		$oficinas[$i] = $oa;
    	}
        return View::make('creditos/search_nomina', array(
            'activo'=>'empresa',
            'oficinas_a'=>$oficinas,
        ));
    }

    public function closersByOffice()
    {
    	$oficina = Input::get('oficina');
    	$closers = Closer::where('oficina_id',$oficina)->orderBy('nombre')->get();
    	$options = "<option value='0'>TODOS</option>";
    	foreach($closers as $c){
    		$options .= "<option value='$c->id'>$c->nombre</option>";
    	}
    	return $options;
    }

    /**
     * Muestra la lista de resultados obtenidos de buscar
     * créditos pertenecientes a la cédula introducida en
     * el formulario que muestra el método 'searchByDoc'
     * @return mixed
     */
    public function searchByDocResult()
    {
        $cedula = Input::get('cedula_titular');
        $creditos = Credito::with('closer')->where('cedula_titular',"$cedula")->orderBy('fecha', 'desc')->paginate(10);

        return View::make('creditos/search_by_doc_result', array(
            'activo'=>'empresa',
            'creditos'=>$creditos
        ));
    }

    public function searchNominaResult()
    {
    	
    	$desde = Input::get('desde');
    	$hasta = Input::get('hasta');
    	$oficina = Input::get('oficina_id');
    	$closer = Input::get('closer_id');

    	$abonos = Abono::with('credito')
    		->whereIn('credito_id', function($query){
    			$query->select('id')->from('creditos')
                    ->where('devolucion', 0)
                    ->whereBetween('fecha',array(Input::get('desde'), Input::get('hasta')))
    				->orderBy('closer_id');
    		})
			->groupBy('credito_id')->orderBy('created_at')->get();

    	if($oficina != 0 && $closer != 0){

    		$abonos = Abono::with('credito')
    		->whereIn('credito_id', function($query){
    			$query->select('id')->from('creditos')
                    ->where('devolucion', 0)
                    ->whereBetween('fecha',array(Input::get('desde'), Input::get('hasta')))
    				->where('closer_id', Input::get('closer_id'))->orderBy('closer_id');
    		})
			->groupBy('credito_id')->orderBy('created_at')->get();

    	}else if($oficina != 0){

    		$abonos = Abono::with('credito')
    		->whereIn('credito_id', function($query){
    			$query->select('id')->from('creditos')
                    ->where('devolucion', 0)
                    ->whereBetween('fecha',array(Input::get('desde'), Input::get('hasta')))
    				->whereIn('closer_id', function($query){
    					$query->select('id')->from('closers')->where('oficina_id',Input::get('oficina_id'));
    				})->orderBy('closer_id');
    		})
			->groupBy('credito_id')->orderBy('created_at')->get();
    	}
		
		$total_total = 0;
		$total_comision = 0;

		foreach($abonos as $a){
			$porcentaje = round($a->valor * 100 / $a->credito->valor_contrato);
			$a->porcentaje = $porcentaje;
			$porcentaje_comision = 0;
			
			if($porcentaje == 100){
				if($a->credito->valor_contrato >= 6000000){
					$porcentaje_comision = $a->credito->closer->porcentaje_100_mayor_seis;
				}else if($a->credito->valor_contrato >= 4000000){
					$porcentaje_comision = $a->credito->closer->porcentaje_100_mayor_cuatro;
				}else if($a->credito->valor_contrato >= 1440000){
					$porcentaje_comision = $a->credito->closer->porcentaje_100_mayor_uno_cuatro_cuarenta;
				}
			}else if($a->porcentaje >= 50){
				$porcentaje_comision = $a->credito->closer->porcentaje_50;
			}
			$a->porcentaje_comision = $porcentaje_comision;
			$a->total_a_pagar = round($a->credito->valor_contrato * $porcentaje_comision / 100);
			
			$total_total += $a->credito->valor_contrato;
			$total_comision += $a->total_a_pagar;
		}

        return View::make('creditos/search_nomina_result', array(
            'activo'=>'empresa',
            'abonos'=>$abonos,
            'total_total'=>$total_total,
            'total_comision'=>$total_comision
        ));
    }

    public function nuevoPago()
    {
        print_r(Input::get('id_credito'));
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(in_array(Auth::user()->role_id, array(1, 3))){
			$creditos = Credito::with('closer')->orderBy('fecha', 'desc')->paginate(10);
		}else{
			$creditos = Credito::with('closer')->where('user_id', Auth::user()->id)->orderBy('fecha', 'desc')->paginate(10);
		}
		
		return View::make('creditos.index', array(
			'creditos'=>$creditos,
			'activo'=>'empresa'
			)
		);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        if(Auth::user()->role_id == 3){
            return Redirect::to('/no_auth');
        }
        $credito = new Credito();
		$closers_a = Closer::closersArray();
		return View::make('creditos.form', array(
			'credito'=> $credito,
			'closers_a'=>$closers_a,
			'action'=>'Nuevo',
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        if(Auth::user()->role_id == 3){
            return Redirect::to('/no_auth');
        }
        $credito = new Credito;
        $data = Input::all();

        // Revisamos si la data es válida
        if ($credito->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $credito->fill($data);
            if($credito->saldo == null || $credito->saldo == 0 || $credito->saldo == ''){
            	$credito->saldo = $credito->valor_contrato;
            	$credito->user_id = Auth::user()->id;
            }
            $credito->servicio_cliente .= '|';
            $credito->observaciones .= '|';

            $credito->save();

            $valor_primer_abono = (is_numeric(Input::get('primer_abono'))) ? Input::get('primer_abono') : 0;

            if($valor_primer_abono > 0){
                $primer_abono = new Abono();
                $primer_abono->credito_id = $credito->id;
                $primer_abono->valor = $valor_primer_abono;
                $primer_abono->nuevo_saldo = 0;
                $primer_abono->save();

                $suma_abonos = Abono::where('credito_id', $primer_abono->credito_id)->sum('valor');
                $nuevo_saldo = $credito->valor_contrato - $suma_abonos;

                $credito->saldo = $nuevo_saldo;
                $credito->save();

                $primer_abono->nuevo_saldo = $nuevo_saldo;
                $primer_abono->save();
            }
            
            Return Redirect::to('panel/creditos');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.creditos.create')->withInput()->withErrors($credito->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$credito = Credito::find($id);
		$abonos = Abono::where('credito_id', $id)->orderBy('id')->get();
		if (is_null ($credito))
		{
			App::abort(404);
		}
		$closers_a = Closer::closersArray();
        $servicio_cliente = explode('|', $credito->servicio_cliente);
        array_pop($servicio_cliente);
        $observaciones = explode('|', $credito->observaciones);
        array_pop($observaciones);
		return View::make('creditos.detalle', array(
			'credito'=> $credito,
			'abonos'=>$abonos,
			'closers_a'=>$closers_a,
			'action'=>'Detalle',
			'activo'=>'empresa',
            'servicio_cliente'=>$servicio_cliente,
            'observaciones'=>$observaciones
			)
		);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        if(Auth::user()->role_id != 1){
            return Redirect::to('/panel/no_auth');
        }
        $credito = Credito::find($id);
		if (is_null ($credito))
		{
			App::abort(404);
		}
        $credito->servicio_cliente = '';
        $credito->observaciones = '';
		$closers_a = Closer::closersArray();
		return View::make('creditos.form', array(
			'credito'=> $credito,
			'closers_a'=>$closers_a,
			'action'=>'Editar',
			'activo'=>'empresa'
			)
		);
		//return View::make('admin/users/form')->with('user', $user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        if(Auth::user()->role_id != 1){
            return Redirect::to('/no_auth');
        }
        $credito = Credito::find($id);
        if (is_null ($credito))
        {
            App::abort(404);
        }
        $servicio_cliente = $credito->servicio_cliente;
        $observaciones = $credito->observaciones;
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($credito->isValid($data))
        {
            $credito->fill($data);
            $credito->servicio_cliente = $servicio_cliente.Input::get('servicio_cliente').'|';
            $credito->observaciones = $observaciones.Input::get('observaciones').'|';
            $credito->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.creditos.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.creditos.edit', $credito->id)->withInput()->withErrors($credito->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $credito = Credito::find($id);
        
        if (is_null ($credito))
        {
            App::abort(404);
        }

        $credito->delete();

        return Redirect::route('panel.creditos.index');
	}


}
