<?php

class OficinaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$oficinas = Oficina::orderBy('nombre')->paginate(10);
		return View::make('oficinas.index', array(
			'oficinas'=>$oficinas,
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $oficina = new Oficina();
        return View::make('oficinas.form', array(
                'oficina'=> $oficina,
                'action'=>'Nuevo',
                'activo'=>'empresa'
            )
        );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $oficina = new Oficina;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        // Revisamos si la data es válida
        if ($oficina->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $oficina->fill($data);

            $oficina->save();

            Return Redirect::to('panel/oficinas');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
            return Redirect::route('panel.oficinas.create')->withInput()->withErrors($oficina->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        Return Redirect::to('panel/oficinas');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $oficina = Oficina::find($id);
        if (is_null ($oficina))
        {
            App::abort(404);
        }
        return View::make('oficinas.form', array(
                'oficina'=> $oficina,
                'action'=>'Editar',
                'activo'=>'empresa'
            )
        );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $oficina = Oficina::find($id);
        if (is_null ($oficina))
        {
            App::abort(404);
        }

        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        // Revisamos si la data es válido
        if ($oficina->isValid($data))
        {
            $oficina->fill($data);
            $oficina->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.oficinas.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.oficinas.edit', $oficina->id)->withInput()->withErrors($oficina->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $oficina = Oficina::find($id);

        if (is_null ($oficina))
        {
            App::abort(404);
        }

        $oficina->delete();

        return Redirect::route('panel.oficinas.index');
	}


}
