<?php

class PlanController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$planes = Plan::with('plan_tipo')->orderBy('title')->paginate(10);
		return View::make('planes.index', array(
			'planes'=>$planes,
			'activo'=>'planes'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$plan = new Plan();
		$planes_tipos_a = PlanTipo::planesTiposArray();
		return View::make('planes.form', array(
			'plan'=> $plan, 
			'planes_tipos_a'=>$planes_tipos_a,
			'action'=>'Nuevo',
			'activo'=>'planes'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$filename = 'no_image.jpg';

		if (Input::hasFile('picture'))
		{
			$destinationPath = 'uploads';
		    $file = Input::file('picture');
			$name = sha1($file->getClientOriginalName()).'_'.sha1(microtime());
			$filename = $name.'.'.$file->getClientOriginalExtension();
			$upload_success = Input::file('picture')->move($destinationPath, $filename);
		}
		
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $plan = new Plan;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        // Revisamos si la data es válida
        if ($plan->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $plan->fill($data);
            $plan->picture = $filename;
            
            $plan->save();
            
            Return Redirect::to('panel/planes');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.planes.create')->withInput()->withErrors($plan->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel/planes');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$plan = Plan::find($id);
		if (is_null ($plan))
		{
			App::abort(404);
		}
		$planes_tipos_a = PlanTipo::planesTiposArray();
		return View::make('planes.form', array(
			'plan'=> $plan, 
			'planes_tipos_a'=>$planes_tipos_a,
			'action'=>'Editar',
			'activo'=>'planes'
			)
		);
		//return View::make('admin/users/form')->with('user', $user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$plan = Plan::find($id);
        if (is_null ($plan))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        $filename = 'no_image.jpg';

		if (Input::hasFile('picture'))
		{
			$destinationPath = 'uploads';
		    $file = Input::file('picture');
			$name = sha1($file->getClientOriginalName()).'_'.sha1(microtime());
			$filename = $name.'.'.$file->getClientOriginalExtension();
			$upload_success = Input::file('picture')->move($destinationPath, $filename);
		}
        
        // Revisamos si la data es válido
        if ($plan->isValid($data))
        {
            $plan->fill($data);
            $plan->picture = $filename;
            $plan->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.planes.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.planes.edit', $plan->id)->withInput()->withErrors($plan->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$plan = Plan::find($id);
        
        if (is_null ($plan))
        {
            App::abort(404);
        }
        
        $plan->delete();

        return Redirect::route('panel.planes.index');
	}


}
