<?php

class CruceroController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cruceros = Crucero::orderBy('title')->paginate(10);
		return View::make('cruceros.index', array(
			'cruceros'=>$cruceros,
			'activo'=>'cruceros'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$crucero = new Crucero();
		return View::make('cruceros.form', array(
			'crucero'=> $crucero, 
			'action'=>'Nuevo',
			'activo'=>'cruceros'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$filename = 'no_image.jpg';

		if (Input::hasFile('picture'))
		{
			$destinationPath = 'uploads';
		    $file = Input::file('picture');
			$name = sha1($file->getClientOriginalName()).'_'.sha1(microtime());
			$filename = $name.'.'.$file->getClientOriginalExtension();
			$upload_success = Input::file('picture')->move($destinationPath, $filename);
		}
		
        $crucero = new Crucero;
        $data = Input::all();

        // Revisamos si la data es válida
        if ($crucero->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $crucero->fill($data);
            $crucero->picture = $filename;
            
            $crucero->save();
            
            Return Redirect::to('panel/cruceros');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.cruceros.create')->withInput()->withErrors($crucero->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel/cruceros');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$crucero = Crucero::find($id);
		if (is_null ($crucero))
		{
			App::abort(404);
		}
		return View::make('cruceros.form', array(
			'crucero'=> $crucero, 
			'action'=>'Editar',
			'activo'=>'cruceros'
			)
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$crucero = Crucero::find($id);
        if (is_null ($crucero))
        {
            App::abort(404);
        }
        $filename = 'no_image.jpg';

		if (Input::hasFile('picture'))
		{
			$destinationPath = 'uploads';
		    $file = Input::file('picture');
			$name = sha1($file->getClientOriginalName()).'_'.sha1(microtime());
			$filename = $name.'.'.$file->getClientOriginalExtension();
			$upload_success = Input::file('picture')->move($destinationPath, $filename);
		}
        
        $data = Input::all();
        
        if ($crucero->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $crucero->fill($data);
            $crucero->picture = $filename;
            // Guardamos el usuario
            $crucero->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.cruceros.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.cruceros.edit', $crucero->id)->withInput()->withErrors($crucero->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$crucero = Crucero::find($id);
        
        if (is_null ($crucero))
        {
            App::abort(404);
        }
        
        $crucero->delete();

        return Redirect::route('panel.cruceros.index');
	}


}
