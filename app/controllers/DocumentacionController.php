<?php

class DocumentacionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$documentacion = Documentacion::find(1);

		return View::make('documentacion.form', array(
			'documentacion'=> $documentacion, 
			'action'=>'Editar',
			'activo'=>'documentacion'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		/*$seccion_empresa = new SeccionEmpresa();
		return View::make('secciones_empresa.form', array(
			'seccion_empresa'=> $seccion_empresa, 
			'action'=>'Nuevo',
			'activo'=>'empresa'
			)
		);*/
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        /*$seccion_empresa = new SeccionEmpresa;
        $data = Input::all();

        // Revisamos si la data es válida
        if ($seccion_empresa->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $seccion_empresa->fill($data);
            $seccion_empresa->save();
            
            Return Redirect::to('panel/empresa');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.empresa.create')->withInput()->withErrors($seccion_empresa->errors);
        }*/
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel/documentacion');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$documentacion = Documentacion::find(1);

		return View::make('documentacion.form', array(
			'documentacion'=> $documentacion, 
			'action'=>'Editar',
			'activo'=>'documentacion'
			)
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$documentacion = Documentacion::find($id);
        if (is_null ($documentacion))
        {
            App::abort(404);
        }
        
        $data = Input::all();
        
        if ($documentacion->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $documentacion->fill($data);
            // Guardamos el usuario
            $documentacion->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.documentacion.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.documentacion.edit', $documentacion->id)->withInput()->withErrors($documentacion->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		/*$seccion_empresa = SeccionEmpresa::find($id);
        
        if (is_null ($seccion_empresa))
        {
            App::abort(404);
        }
        
        $seccion_empresa->delete();

        return Redirect::route('panel.empresa.index');*/
	}


}
