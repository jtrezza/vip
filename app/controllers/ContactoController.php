<?php

class ContactoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('contacto', array(
			'action'=>'Nuevo',
			'activo'=>'contacto'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('contacto', array(
                'action'=>'Nuevo',
                'activo'=>'contacto'
            )
        );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        Mail::send('mail.contacto', array('nombre'=>Input::get('nombre'), 'telefono'=>Input::get('telefono'),
            'numero_contrato'=>Input::get('numero_contrato'), 'ciudad'=>Input::get('ciudad'), 'email'=>Input::get('email'),
                'comentarios'=>Input::get('comentarios')),
            function($message){$message->to(PaginaPrincipal::find(1)->email_notificaciones, 'VIP Tours')->subject('Sección de contacto página web');
            });

        Return View::make('contacto_registrado');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('contacto');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        Return Redirect::to('contacto');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        App::abort(404);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        App::abort(404);
	}


}
