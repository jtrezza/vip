<?php

class CloserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$closers = Closer::with('oficina')->orderBy('nombre')->paginate(10);
		return View::make('closers.index', array(
			'closers'=>$closers,
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $closers = new Closer();
		$oficinas_a = Oficina::oficinasArray();
		return View::make('closers.form', array(
			'closer'=> $closers,
			'oficinas_a'=>$oficinas_a,
			'action'=>'Nuevo',
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $closer = new Closer;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        // Revisamos si la data es válida
        if ($closer->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $closer->fill($data);

            $closer->save();
            
            Return Redirect::to('panel/closers');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.closers.create')->withInput()->withErrors($closer->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel/closers');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $closer = Closer::find($id);
		if (is_null ($closer))
		{
			App::abort(404);
		}
		$oficinas_a = Oficina::oficinasArray();
		return View::make('closers.form', array(
			'closer'=> $closer,
			'oficinas_a'=>$oficinas_a,
			'action'=>'Editar',
			'activo'=>'empresa'
			)
		);
		//return View::make('admin/users/form')->with('user', $user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $closer = Closer::find($id);
        if (is_null ($closer))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($closer->isValid($data))
        {
            $closer->fill($data);
            $closer->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.closers.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.closers.edit', $closer->id)->withInput()->withErrors($closer->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $closer = Closer::find($id);
        
        if (is_null ($closer))
        {
            App::abort(404);
        }

        $closer->delete();

        return Redirect::route('panel.closers.index');
	}


}
