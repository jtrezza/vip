<?php

class PaginaPrincipalController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $datos = PaginaPrincipal::find(1);
        return View::make('admin/principal', array(
                'datos'=> $datos
            )
        );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $datos = new PaginaPrincipal();

        return View::make('admin/principal', array(
                'datos'=> $datos
            )
        );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $datos = new PaginaPrincipal();
        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        // Si la data es valida se la asignamos al usuario
        $datos->fill($data);

        $datos->save();

        Return Redirect::to('panel/creditos');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $datos = PaginaPrincipal::find($id);
        if (is_null ($datos))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();

        $datos->fill($data);
        $datos->save();
        // Y Devolvemos una redirección a la acción show para mostrar el usuario
        return Redirect::route('panel.index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $credito = Credito::find($id);
        
        if (is_null ($credito))
        {
            App::abort(404);
        }

        $credito->delete();

        return Redirect::route('panel.credito.index');
	}


}
