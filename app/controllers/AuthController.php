<?php

class AuthController extends BaseController {

    public function showLogin()
    {
        // Verificamos que el usuario no esté autenticado
        if (Auth::check())
        {
            // Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
            return Redirect::to('panel');
        }
        // Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
        return View::make('admin.login');
    }

    public function postLogin()
    {
        // Guardamos en un arreglo los datos del usuario.
        $userdata = array(
            'username' => Input::get('username'),
            'password'=> Input::get('password')
        );
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
        if(Auth::attempt($userdata, Input::get('remember-me', 0)))
        {
            // De ser datos válidos nos mandara a la bienvenida
            if(Auth::user()->role_id == 1){
                return Redirect::to('panel');
            }else{
                return Redirect::to('ventas');
            }
            
        }
        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
        return Redirect::to('login')
            ->with('mensaje_error', 'Tus datos son incorrectos')
            ->withInput();
    }

    public function logOut()
    {
        Auth::logout();
        return Redirect::to('login')
            ->with('mensaje_error', 'Tu sesión ha sido cerrada.');
    }

    public function changePassword()
    {
        return View::make('admin.change_password');
    }
    public function updatePassword()
    {
        $user = User::find(Auth::user()->id);

        if (is_null ($user))
        {
            return Redirect::to('/panel/cambiar_contrasena')->withErrors(array('Usuario no encontrado.'));
        }

        $data = Input::all();

        $pass_ant = $data['contrasena_anterior'];

        // Revisamos si la data es válido
        if ($data['nueva_contrasena'] == $data['nueva_contrasena_confirmation'] && $data['nueva_contrasena'] != '')
        {
            if (Hash::check($pass_ant, $user->password))
            {
                $user->password = $data['nueva_contrasena'];
                // Guardamos el usuario
                $user->save();
                // Y Devolvemos una redirección a la acción show para mostrar el usuario
                if(Auth::user()->role_id == 1){
                    return Redirect::to('panel');
                }else{
                    return Redirect::to('ventas');
                }
            }else{
                return Redirect::to('/panel/cambiar_contrasena')->withErrors(array('La contraseña anterior no es correcta.'));
            }
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::to('/panel/cambiar_contrasena')->withErrors(array('Las contraseñas no coinciden o ha dejado campos en blanco.'));
        }
    }
}