<?php

class SolicitudSeguroController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$solicitud_seguro = new SolicitudSeguro();
		return View::make('seguro', array(
			'solicitud_seguro'=> $solicitud_seguro, 
			'action'=>'Nuevo',
			'activo'=>'seguro'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$solicitud_seguro = new SolicitudSeguro();
		return View::make('seguro', array(
			'solicitud_seguro'=> $solicitud_seguro, 
			'action'=>'Nuevo',
			'activo'=>'seguro'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
        $solicitud_seguro = new SolicitudSeguro();
        $data = Input::all();

        // Revisamos si la data es válida
        if ($solicitud_seguro->isValid($data))
        {
            $solicitud_seguro->fill($data);
            $solicitud_seguro->save();

            Mail::send('mail.seguro', array('nombre'=>Input::get('nombre'), 'cedula'=>Input::get('cedula'),'telefono'=>Input::get('telefono'),
                'email'=>Input::get('email'),'ciudad'=>Input::get('ciudad'),'numero_tiquete'=>Input::get('numero_tiquete'),
                'vuelo'=>Input::get('vuelo'),'fecha_salida'=>Input::get('fecha_salida'),'fecha_regreso'=>Input::get('fecha_regreso')),
                function($message){$message->to(PaginaPrincipal::find(1)->email_notificaciones, 'VIP Tours')->subject('Solicitud de seguro vía página web');
            });

            Return Redirect::to('seguro_success');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('seguro.create')->withInput()->withErrors($solicitud_seguro->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('seguro');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		Return Redirect::to('seguro');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        App::abort(404);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        App::abort(404);
	}


}
