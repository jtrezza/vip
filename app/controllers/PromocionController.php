<?php

class PromocionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$promociones = Promocion::orderBy('title')->paginate(10);
		return View::make('promociones.index', array(
			'promociones'=>$promociones,
			'activo'=>'promociones'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$promocion = new Promocion();
		return View::make('promociones.form', array(
			'promocion'=> $promocion, 
			'action'=>'Nuevo',
			'activo'=>'promociones'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$filename = 'no_image.jpg';

		if (Input::hasFile('picture'))
		{
			$destinationPath = 'uploads';
		    $file = Input::file('picture');
			$name = sha1($file->getClientOriginalName()).'_'.sha1(microtime());
			$filename = $name.'.'.$file->getClientOriginalExtension();
			$upload_success = Input::file('picture')->move($destinationPath, $filename);
		}
		
        $promocion = new Promocion;
        $data = Input::all();

        // Revisamos si la data es válida
        if ($promocion->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $promocion->fill($data);
            $promocion->picture = $filename;
            
            $promocion->save();
            
            Return Redirect::to('panel/promociones');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.promocion.create')->withInput()->withErrors($promocion->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel/promociones');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$promocion = Promocion::find($id);
		if (is_null ($promocion))
		{
			App::abort(404);
		}
		return View::make('promociones.form', array(
			'promocion'=> $promocion, 
			'action'=>'Editar',
			'activo'=>'promociones'
			)
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$promocion = Promocion::find($id);
        if (is_null ($promocion))
        {
            App::abort(404);
        }
        
        $filename = 'no_image.jpg';

		if (Input::hasFile('picture'))
		{
			$destinationPath = 'uploads';
		    $file = Input::file('picture');
			$name = sha1($file->getClientOriginalName()).'_'.sha1(microtime());
			$filename = $name.'.'.$file->getClientOriginalExtension();
			$upload_success = Input::file('picture')->move($destinationPath, $filename);
		}

        $data = Input::all();
        
        if ($promocion->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $promocion->fill($data);
            $promocion->picture = $filename;
            $promocion->save();
            
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.promociones.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.promociones.edit', $promocion->id)->withInput()->withErrors($promocion->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$promocion = Promocion::find($id);
        
        if (is_null ($promocion))
        {
            App::abort(404);
        }
        
        $promocion->delete();

        return Redirect::route('panel.promociones.index');
	}


}
