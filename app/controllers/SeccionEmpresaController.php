<?php

class SeccionEmpresaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$secciones_empresa = SeccionEmpresa::orderBy('title')->paginate(10);
		return View::make('secciones_empresa.index', array(
			'secciones_empresa'=>$secciones_empresa,
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$seccion_empresa = new SeccionEmpresa();
		return View::make('secciones_empresa.form', array(
			'seccion_empresa'=> $seccion_empresa, 
			'action'=>'Nuevo',
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
        $seccion_empresa = new SeccionEmpresa;
        $data = Input::all();

        // Revisamos si la data es válida
        if ($seccion_empresa->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $seccion_empresa->fill($data);
            $seccion_empresa->save();
            
            Return Redirect::to('panel/empresa');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.empresa.create')->withInput()->withErrors($seccion_empresa->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		Return Redirect::to('panel/empresa');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$seccion_empresa = SeccionEmpresa::find($id);
		if (is_null ($seccion_empresa))
		{
			App::abort(404);
		}
		return View::make('secciones_empresa.form', array(
			'seccion_empresa'=> $seccion_empresa, 
			'action'=>'Editar',
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$seccion_empresa = SeccionEmpresa::find($id);
        if (is_null ($seccion_empresa))
        {
            App::abort(404);
        }
        
        $data = Input::all();
        
        if ($seccion_empresa->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $seccion_empresa->fill($data);
            // Guardamos el usuario
            $seccion_empresa->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.empresa.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.empresa.edit', $seccion_empresa->id)->withInput()->withErrors($seccion_empresa->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$seccion_empresa = SeccionEmpresa::find($id);
        
        if (is_null ($seccion_empresa))
        {
            App::abort(404);
        }
        
        $seccion_empresa->delete();

        return Redirect::route('panel.empresa.index');
	}


}
