<?php

class AbonoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

        if(Auth::user()->role_id == 3){
            return Redirect::to('/no_auth');
        }
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $abono = new Abono;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $credito = Credito::find($data['credito_id']);
        $nuevo_saldo = $credito->saldo - $data['valor'];
        $data['nuevo_saldo'] = $nuevo_saldo;

        // Revisamos si la data es válida
        if ($abono->isValid($data))
        {
        	if($data['valor'] > $credito->saldo){
        		return Redirect::to('panel/nuevo_pago/'.$data['credito_id'])->withInput()
        			->withErrors(array('Está intentando abonar un valor mayor al saldo actual.'));
        	}
            // Si la data es valida se la asignamos al usuario
            $abono->fill($data);
            $credito->saldo = $nuevo_saldo;

            $credito->save();
            $abono->save();
            
            Return Redirect::to('panel/creditos');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::to('panel/nuevo_pago/'.$data['credito_id'])->withInput()->withErrors($abono->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$abonos = Abono::where('credito_id',$id)->paginate(10);
		$credito = Credito::find($id);
		return View::make('abonos.index', array(
			'abonos'=>$abonos,
			'activo'=>'empresa',
			'credito'=>$credito,
			)
		);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        if(!in_array(Auth::user()->role_id, array(1, 2))){
            return Redirect::to('/panel/no_auth');
        }
        $abono = Abono::find($id);
        if (is_null ($abono))
        {
            App::abort(404);
        }
        $id_credito = $abono->credito_id;
        return View::make('creditos/nuevo_pago', array(
            'activo'=>'empresa',
            'id_credito'=>$id_credito,
            'abono'=>$abono
        ));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        if(!in_array(Auth::user()->role_id, array(1, 2))){
            return Redirect::to('/no_auth');
        }
        $abono = Abono::find($id);
        if (is_null ($abono))
        {
            App::abort(404);
        }
        $data = Input::all();

        // Revisamos si la data es válido
        if (is_numeric($data['valor']))
        {
            $abono->valor = $data['valor'];
            $abono->save();

            $credito = Credito::find($abono->credito_id);
            $suma_abonos = Abono::where('credito_id', $abono->credito_id)->sum('valor');
            $nuevo_saldo = $credito->valor_contrato - $suma_abonos;

            $credito->saldo = $nuevo_saldo;
            $credito->save();

            return Redirect::route('panel.creditos.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.abonos.edit', $abono->id)->withInput()->withErrors(array('El valor a abonar debe ser numérico.'));
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $abono = Abono::find($id);

        if (is_null ($abono))
        {
            App::abort(404);
        }
        $credito = Credito::find($abono->credito_id);
        $abono->delete();

        $suma_abonos = Abono::where('credito_id', $credito->id)->sum('valor');
        $nuevo_saldo = $credito->valor_contrato - $suma_abonos;

        $credito->saldo = $nuevo_saldo;
        $credito->save();

        return Redirect::route('panel.creditos.index');
	}


}
