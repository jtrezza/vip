<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios = User::with('Oficina')->paginate(10);
        $roles = array(1=>'Gerante', 2=>'Administrador', 3=>'Asesor');
		return View::make('users.index', array(
			'usuarios'=>$usuarios,
			'activo'=>'empresa',
            'roles'=>$roles
			)
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$usuario = new User();
		$oficinas_a = Oficina::oficinasArray();
		$roles_a = Role::rolesArray();
		return View::make('users.form', array(
			'usuario'=> $usuario,
			'oficinas_a'=>$oficinas_a,
			'roles_a'=>$roles_a,
			'action'=>'Nuevo',
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $usuario = new User;
        $data = Input::all();

        // Revisamos si la data es válida
        if ($usuario->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $usuario->fill($data);
            $usuario->password = $usuario->username;
            $usuario->save();
            
            Return Redirect::to('panel/usuarios');
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
			return Redirect::route('panel.usuarios.create')->withInput()->withErrors($usuario->errors);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuario = User::find($id);
		if (is_null ($usuario))
		{
			App::abort(404);
		}
		$oficinas_a = Oficina::oficinasArray();
		$roles_a = Role::rolesArray();

		return View::make('users.form', array(
			'usuario'=> $usuario,
			'oficinas_a'=>$oficinas_a,
			'roles_a'=>$roles_a,
			'action'=>'Editar',
			'activo'=>'empresa'
			)
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$usuario = User::find($id);
        if (is_null ($usuario))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        
        // Revisamos si la data es válido
        if ($usuario->isValid($data))
        {
            $usuario->fill($data);
            $usuario->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::route('panel.usuarios.index');
        }
        else
        {
            // En caso de error regresa a la acción edit con los datos y los errores encontrados
            return Redirect::route('panel.usuarios.edit', $usuario->id)->withInput()->withErrors($usuario->errors);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $user = User::find($id);

        if (is_null ($user))
        {
            App::abort(404);
        }

        $user->delete();
        return Redirect::route('panel.usuarios.index');
	}


}