<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailNotificacionesColumnPaginaPrincipalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pagina_principal', function($table)
        {
            $table->string('email_notificaciones');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pagina_principal', function($table)
        {
            $table->dropColumn('email_notificaciones');
        });
	}

}
