<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsComisionesClosersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('closers', function($table)
        {
            $table->string('tipo');
            $table->double('porcentaje_100_mayor_seis');
            $table->double('porcentaje_100_mayor_cuatro');
            $table->double('porcentaje_100_mayor_uno_cuatro_cuarenta');
            $table->double('porcentaje_50');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('closers', function($table)
        {
            $table->dropColumn('tipo');
            $table->dropColumn('porcentaje_100_mayor_seis');
            $table->dropColumn('porcentaje_100_mayor_cuatro');
            $table->dropColumn('porcentaje_100_mayor_uno_cuatro_cuarenta');
            $table->dropColumn('porcentaje_50');
        });
	}

}
