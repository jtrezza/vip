<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('creditos', function($table)
        {
            $table->increments('id');

            $table->string('numero');
            $table->date('fecha');
            $table->integer('gl');
            $table->integer('valor_contrato');
            $table->integer('saldo');
            $table->string('nombres_titular');
            $table->string('apellidos_titular');
            $table->string('cedula_titular');
            $table->string('cootitulares');
            $table->string('direccion_titular');
            $table->string('telefono_titular');
            $table->string('celular_titular');
            $table->string('email_titular');
            $table->string('vigencia');
            $table->string('duracion');
            $table->string('producto_solicitado');
            $table->text('liner');
            $table->integer('closer_id');
            $table->string('estado')->default('ACTIVO');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('creditos');
	}

}
