<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServclienteObservacionesColumnsCredito extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('creditos', function($table)
        {
            $table->string('servicio_cliente');
            $table->string('observaciones');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('creditos', function($table)
        {
            $table->dropColumn('servicio_cliente');
            $table->dropColumn('observaciones');
        });
	}

}
