<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBeneficiariosColumnCreditosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//remember_token
        Schema::table('creditos', function($table)
        {
            $table->string('beneficiarios');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('creditos', function($table)
        {
            $table->dropColumn('beneficiarios');
        });
	}

}
