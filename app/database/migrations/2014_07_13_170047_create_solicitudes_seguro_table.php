<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesSeguroTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solicitudes_seguro', function($table)
        {
            $table->increments('id');
            
            $table->string('nombre');
            $table->string('cedula');
            $table->string('telefono');
            $table->string('email');
            $table->string('ciudad');
            $table->string('numero_tiquete');
            $table->string('vuelo');
            $table->date('fecha_salida');
            $table->date('fecha_regreso');
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solicitudes_seguro');
	}

}
