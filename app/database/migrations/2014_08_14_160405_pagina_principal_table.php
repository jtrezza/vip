<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaginaPrincipalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pagina_principal', function($table)
        {
            $table->increments('id');

            $table->string('titulo_uno');
            $table->string('descripcion_uno');
            $table->string('ver_mas');
            $table->string('titulo_dos');
            $table->string('detalle_dos');
            $table->string('titulo_tres');
            $table->string('detalle_tres');
            $table->string('url_video');

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('pagina_principal');
	}

}
