<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacto', function($table)
        {
            $table->increments('id');
            
            $table->string('nombre');
            $table->string('telefono');
            $table->string('no_contrato');
            $table->string('ciudad');
            $table->string('email');
            $table->text('comentarios');
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacto');
	}

}
