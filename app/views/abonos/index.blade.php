@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Abonos <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Número</th>
			<th>Cédula</th>
			<th>Titular</th>
			<th>Valor contrato</th>
			<th>Fecha Abono</th>
			<th>Valor abono</th>
			<?php if(in_array(Auth::user()->role_id, array(1, 2))): ?>
			<th>Editar</th>
			<th>Eliminar</th>
			<?php endif; ?>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($abonos as $abono)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$credito->numero}}</td>
				<td>{{$credito->cedula_titular}}</td>
				<td>{{$credito->nombres_titular . ' ' . $credito->apellidos_titular}}</td>
				<td>{{ "$ ".number_format($credito->valor_contrato, 0, ',','.') }}</td>
				<td>{{$abono->created_at}}</td>
				<td>{{ "$ ".number_format($abono->valor, 0, ',','.') }}</td>
				<?php if(in_array(Auth::user()->role_id, array(1, 2))): ?>
                <td><a href="{{ route('panel.abonos.edit', $abono->id) }}" class="btn btn-info">Editar</a></td>
                <td>
                    {{ Form::model($abono, array('route' => array('panel.abonos.destroy', $abono->id), 'method' => 'DELETE', 'role' => 'form')) }}
                    {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                </td>
                <?php endif; ?>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $abonos->links() }}
</div>

<h3>Saldo: $<?= number_format($credito->saldo); ?></h3>
@stop