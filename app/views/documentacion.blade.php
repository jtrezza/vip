@extends('layout')
@section('content')
<style type="text/css">
p{
    text-align: justify;
}
</style>
<section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Documentación</h1>
                </div>
            </div>
        </div>
    </section><!--/#title-->
<div style="padding:4em 2em 2em 2em;">
	<div class="row">
		<div class="col-sm-8">
			{{ Form::open(array('url' => 'documentacion_pais', 'class'=>'form-horizontal', 'role'=>'form')) }}
			<div class="form-group">
			    <label for="pais" class="col-sm-3 control-label">Seleccione un país</label>
			    <div class="col-sm-6">
			      {{ Form::select('pais', $array_paises, 2, array('class' => 'form-control', 'id'=>'pais')) }}
			    </div>
			</div>

			  <div class="form-group">
			    <div class="col-sm-offset-3 col-sm-10">
			      <button type="submit" class="btn btn-success">Consultar</button>
			    </div>
			  </div>
			{{ Form::close() }}
		</div>
		<div class="col-md-4 aside-der" style="padding-left:3em;">
			@include('aside')
		</div>
	</div>
	<div class="row" style="padding-left:4em;">
		<div class="col-md-12"></div>
	</div>
</div>
@stop