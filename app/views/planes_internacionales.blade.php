@extends('layout')
@section('content')
<section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Planes internacionales</h1>
                </div>
            </div>
        </div>
    </section><!--/#title-->
<div style="padding:4em 2em 2em 2em;">
	<div class="row">
		<div class="col-sm-8">
			<div class="blog">
				@foreach ($planes as $p)
					@include('planes.mosaico', array('p'=>$p))
				@endforeach
            </div>
		</div>
		<div class="col-md-4 aside-der" style="padding-left:3em;">
			@include('aside')
		</div>
	</div>
	<div class="row" style="padding-left:4em;">
		<div class="col-md-12">{{ $planes->links() }}</div>
	</div>
</div>
@stop