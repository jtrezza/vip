@extends('layout')
@section('content')


<div class="row">
	<div class="col-md-8">
		<div class="text-center">
			<h1>Seguro de viaje <br /><small>¡Solicitud exitosa!</small></h1>
		</div><br />
		<div>
			<div class="alert alert-success">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <strong>Su solicitud ha sido recibida.</strong>
			</div>
		</div>
	
	</div>
	<div class="col-md-4 aside-der">
		@include('aside')
	</div>
</div>
@stop