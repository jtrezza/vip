<html>
    <h2>Datos de la solicitud: </h2>
    <table>
        <tr>
            <td><strong>Nombre: </strong></td>
            <td>{{$nombre}}</td>
        </tr>
        <tr>
            <td><strong>Cédula: </strong></td>
            <td>{{$cedula}}</td>
        </tr>
        <tr>
            <td><strong>Teléfono: </strong></td>
            <td>{{$telefono}}</td>
        </tr>
        <tr>
            <td><strong>e-mail: </strong></td>
            <td>{{$email}}</td>
        </tr>
        <tr>
            <td><strong>Ciudad: </strong></td>
            <td>{{$ciudad}}</td>
        </tr>
        <tr>
            <td><strong>Número de tiquete: </strong></td>
            <td>{{$numero_tiquete}}</td>
        </tr>
        <tr>
            <td><strong>Vuelo: </strong></td>
            <td>{{$vuelo}}</td>
        </tr>
        <tr>
            <td><strong>Fecha de salida: </strong></td>
            <td>{{$fecha_salida}}</td>
        </tr>
        <tr>
            <td><strong>Fecha de regreso: </strong></td>
            <td>{{$fecha_regreso}}</td>
        </tr>
    </table>
</html>