<html>
    <h2>Datos de la solicitud: </h2>
    <table>
        <tr>
            <td><strong>Nombre: </strong></td>
            <td>{{$nombre}}</td>
        </tr>
        <tr>
            <td><strong>Teléfono: </strong></td>
            <td>{{$telefono}}</td>
        </tr>
        <tr>
            <td><strong>Número de contrato: </strong></td>
            <td>{{$numero_contrato}}</td>
        </tr>
        <tr>
            <td><strong>Ciudad: </strong></td>
            <td>{{$ciudad}}</td>
        </tr>
        <tr>
            <td><strong>e-mail: </strong></td>
            <td>{{$email}}</td>
        </tr>
        <tr>
            <td><strong>Comentarios: </strong></td>
            <td>{{$comentarios}}</td>
        </tr>
    </table>
</html>