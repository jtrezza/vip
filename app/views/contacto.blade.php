@extends('layout')
<?php
    $form_data = array('route' => 'contacto.store', 'method' => 'POST', 'class'=>'form-horizontal');
?>
@section('content')

<div style="padding:4em 2em 2em 2em;">
<div class="row">
	<div class="col-md-8">
			<h1 class="text-center">Contacto<br /><small>Contacte con nosotros</small></h1>
		<br />
		<div>
			@include ('errors', array('errors' => $errors)) 
		</div>
		{{ Form::open($form_data, array('role' => 'form')) }}
		    <div class="form-group">
		      	{{ Form::label('nombre', 'Nombre *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('nombre', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('telefono', 'Teléfono *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('telefono', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('numero_contrato', 'Número de contrato *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('numero_contrato', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('ciudad', 'Ciudad *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('ciudad', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('email', 'e-mail *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('email', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('comentarios', 'Comentarios *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::textarea('comentarios', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="col-sm-12">
		    	{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary', 'style'=>'float:right;')) }}    
		    </div>
		{{ Form::close() }}
	</div>
	<div class="col-md-4 aside-der">
		@include('aside')
	</div>
</div>
</div>
@stop