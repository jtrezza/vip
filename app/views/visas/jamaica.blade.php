<div class=Section1>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>General<o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:18.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:silver;mso-fareast-language:
  ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Dirección: Avenida 19 No. 108-45 oficina
  304<br>
  Teléfonos: 612 3598 - 612 3396.<br>
  Fax: 612 3479.<br>
  Correo electrónico: emjacol@cable.net.co - consuladojamaica@cable.net.co<br>
  Horario de <span class=SpellE>atencion</span>: Para solicitud de visas Lunes
  a Viernes 8:30 am a 1:00 pm, para entrega de resultados Lunes a Viernes 2:30
  pm a 4:00 pm.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Los ciudadanos colombianos que deseen
  viajar a Jamaica por turismo, asuntos culturales o de negocios cuya visita no
  exceda los 30 días, podrán ingresar a la isla sin visa.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>NEGOCIOS</span><span style='font-size:
  10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  • Pasaporte vigente<br>
  • Formulario de solicitud de visa debidamente diligenciado<br>
  • Dos fotografías 3.5 x 4.5 cm.<br>
  • Fotocopia de la hoja de datos y de la última del pasaporte</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  • Fotocopia de la visa de Estados Unidos (si aplica)<br>
  • Reserva aérea ida y vuelta<br>
  • Carta laboral<br>
  • Extractos bancarios de los últimos tres meses<br>
  <br>
  ESTUDIANTES</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  • Pasaporte vigente<br>
  • Formulario de solicitud de visa debidamente diligenciado<br>
  • 1 Fotografía 3 x 4 cm a color<br>
  • Fotocopia de la hoja de datos y de la última del pasaporte<br>
  • Copia de la visa de Estados Unidos (si aplica)<br>
  • Copia de reserva aérea valida por 1 año<br>
  • Carta de aceptación de la escuela o institución a la que el estudiante va a
  asistir<br>
  • Certificado de la institución educativa<br>
  • Registro civil<br>
  • Certificación bancaria<br>
  • Sí es menor de edad: carta de autorización de salida del país<br>
  <br>
  DERECHOS CONSULARES:</span><span style='font-size:10.0pt;mso-bidi-font-size:
  11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  Valor visa $ 70.000<br>
  <br>
  <br>
  </span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta
  como informativo de tipo general y en todos los casos debe confirmarse
  previamente a la iniciación de cualquier trámite por cuanto los consulados se
  reservan el derecho de modificar procedimientos y requerimientos sin previo
  aviso.<br>
  <br>
  </span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:18.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:silver;mso-fareast-language:
  ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
   1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
    <td style='padding:0cm 0cm 0cm 0cm'></td>
    <td style='padding:0cm 0cm 0cm 0cm'></td>
   </tr>
   <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
    <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
     width="100%" style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:
     1184;mso-padding-alt:7.5pt 7.5pt 7.5pt 7.5pt'>
     <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
      <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
      line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
      mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
      color:black;mso-fareast-language:ES'>Los documentos deben ser escaneados
      y enviados previamente por correo electrónico a la Embajada en Bogotá.<br>
      <br>
      Días festivos 2014: Enero 1, Marzo 5, Abril 18 - 21, Mayo 23, Agosto 1 -
      4, Octubre 24, Diciembre 25 - 26.</span><span style='font-size:12.0pt;
      font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
      mso-fareast-language:ES'><o:p></o:p></span></p>
      </td>
     </tr>
    </table>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>