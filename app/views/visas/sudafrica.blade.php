<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES'>General<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los ciudadanos colombianos que viajen en
tránsito por Sudáfrica requieren de visa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO Y/O NEGOCIOS:<br>
&#9679; Pasaporte vigente mínimo un (1) año<br>
&#9679; Carnet Internacional de Vacunación contra la fiebre amarilla<br>
&#9679; Dos (2) fotos de 4 x 5 <span class=SpellE>cms</span> en fondo blanco<br>
&#9679; Formulario de solicitud de visa debidamente diligenciado<br>
&#9679; Carta laboral indicado cargo, sueldo y tiempo de servicio<br>
&#9679; Extractos bancarios de los tres (3) últimos meses<br>
&#9679; <span class=SpellE>Printer</span> de la reserva y fotocopia del tiquete<br>
&#9679; Seguro médico o fotocopia de la tarjeta de crédito, si tiene cobertura<br>
&#9679; Para viajes de negocios adjuntar la carta de invitación desde Sudáfrica<br>
&#9679; Reserva de alojamiento<br>
&#9679; Carta de invitación con dirección y teléfono del anfitrión, prueba de
residencia legal y documento de identidad<br>
&#9679; Carta de responsabilidad de gastos si el viaje tiene patrocinio.<br>
&#9679; Para menores de edad: carta de responsabilidad de gastos, permiso de
salida autenticado y Registro Civil de nacimiento<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TRANSITO:<br>
<br>
La duración del tramite es aproximadamente de 10 días hábiles.<br>
<br>
&#9679; Pasaporte, debe tener una vigencia mínima de 30 días después de la
fecha de culminación del viaje previsto y un mínimo de 5 hojas en blanco.<br>
&#9679; Formulario de solicitud de visa debidamente diligenciado (debe estar
impreso en una <span class=SpellE>sóla</span> hoja).<br>
&#9679; Dos fotografías recientes de 4x5 cm en fondo blanco.<br>
&#9679; Original y fotocopia del Certificado Internacional de la vacuna contra
la fiebre amarilla.<br>
&#9679; Carta laboral indicando cargo, sueldo y tiempo de servicio.<br>
&#9679; Extractos bancarios de los 3 últimos meses con sello del banco.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; <span class=SpellE>Printer</span> de la reserva o fotocopia del
itinerario.<br>
&#9679; Copia de la visa del país a visitar, en caso que la visa la estampen a
la llegada y va de turismo debe presentar carta de invitación autenticada o
confirmación de hotel, si va negocios carta de invitación en hoja membrete de
la <span class=SpellE>compañia</span>; y el <span class=SpellE>printer</span>
donde certifique que la visa del país a visitar se la otorgan a la entrada.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Certificado del seguro médico o fotocopia de la tarjeta de crédito
indicando cobertura en Sudáfrica.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Para menores de 21 años: Presentar carta de responsabilidad de gastos,
registro de nacimiento, autorización de salida del país y certificado de
estudios.<br>
&#9679; Para independientes: Certificado de ingresos mensuales expedida por
contador (anexando copia de la tarjeta profesional del contador).<br>
<br>
La visa es otorgada según criterio de la Embajada.<br>
<br>
Las solicitudes de visa se reciben sólo con 30 días antes de la fecha de viaje.<br>
<br>
Los documentos que el Consulado reciba incompletos pueden ser devueltos y
generará un nuevo costo de correo.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: Aproximadamente 15 días hábiles.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES<br>
La visa de turismo y tránsito con pasaporte colombiano no tiene ningún costo.<br>
Valor correo Bogotá - Caracas - Bogotá $213.500 (correo hasta 500 gramos).<br>
Valor impuesto envío a Venezuela $4.400.<br>
Para extranjeros, el Consulado cobra la visa dependiendo del país.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaporte dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visas.
Si el itinerario de viaje implica varias visas, el número de hojas libres de
debe ajustar a esta condición.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:12.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;background:#DDEAF3;mso-fareast-language:ES'>CONSULADO DE
    SUDAFRICA EN CARACAS, VENEZUELA</span><span style='font-size:12.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    <span style='background:#DDEAF3'>Dirección: Avenida Tamanaco con calle <span
    class=SpellE>Sorocalma</span> y Ave Venezuela</span><br>
    <span style='background:#DDEAF3'>Edificio <span class=SpellE>Atrium</span>,
    PH 1ª, Urbanización El Rosal, Chacao 1060</span><br>
    <span style='background:#DDEAF3'>Teléfonos: 9520227/9513613</span><br>
    <span style='background:#DDEAF3'>Fax: (58-212) 9520026</span></span><span
    style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
    "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
    mso-fareast-language:ES'><o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal;background:#DDEAF3'><span style='font-size:12.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'>VIGENCIA:<br>
    La visa se otorga por tres (3) meses, múltiples estradas, y se admiten solicitudes
    solo con 45 días de antelación al viaje.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal;background:#DDEAF3'><span style='font-size:12.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'>Todos los viajeros que ingresan a Sudáfrica deben presentar la
    certificación válida de la vacuna de fiebre amarilla. El Ministerio de
    Salud (DOH) ha elaborado una serie de pautas sobre la prevención de la fiebre
    amarilla, que se requiere sean implementadas en todos los puertos de
    ingreso a Sudáfrica.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>