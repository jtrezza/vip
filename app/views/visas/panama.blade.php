<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los ciudadanos colombianos que viajen a
Panamá en calidad de turismo no necesitan visa, los requisitos son:<br>
<br>
• Demostrar a la entrada medios económicos suficientes para la estadía.<br>
• Pasaporte vigente mínimo seis meses.<br>
• Tiquete de entrada y salida.<br>
• Recomendable el Certificado Internacional de la vacuna contra la fiebre
amarilla (mayores de un año y menores de 65 años).</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
• Deben presentar mínimo USD 500.oo en efectivo, cheque viajero o tarjeta
internacional.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>REQUISITOS PARA LAS NACIONALIDADES QUE
REQUIEREN VISA PARA INGRESAR A PANAMA<br>
<br>
Consulado en Bogotá:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Calle 92 No. 7A - 40<br>
Teléfonos: 2575068</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Fax: 2575067<br>
Página Web: http://www.empacol.org/site/<br>
Horario de atención: Lunes a viernes de 9:00 a.m. a 1:00 p.m.<br>
<br>
El trámite tiene una duración de 48 horas.<br>
<br>
Toda persona, sin importar su nacionalidad, que posea VISA debidamente expedida
por los Estados Unidos de Norteamérica, Australia, Canadá, Reino Unido y de
cualquiera de los estados que conformen la Unión Europea, y haya utilizado por
lo menos una vez para ingresar al territorio del estado otorgante, podrá
ingresar al territorio nacional de Panamá sin VISA.<br>
<br>
Si la comunicación anterior no aplica en su caso, debe tramitar visa.<br>
<br>
Visa de turismo:<br>
<br>
•Pasaporte original y copia completa con un mínimo tres meses de vigencia.<br>
•Formulario totalmente diligenciado.<br>
•Tres fotografías 5x5.<br>
•Reservación aérea comprobada con itinerario de continuación del viaje o tiquete
electrónico.<br>
•Reserva de hotel, confirmada.<br>
•Certificado Internacional de la vacuna contra la fiebre amarilla.<br>
•Copia del documento de identificación o carné de residencia del país donde
tenga su domicilio.<br>
•Prueba de solvencia económica para su manutención y sustento según el termino
de estadía en el país, la cual no podrá ser inferior a USD 500 y se debe
demostrar con: certificación bancaria con estado de cuenta de los últimos tres
meses que refleje saldos <span class=SpellE>disponibles,tarjeta</span> de
crédito con el estado de cuenta de los últimos tres meses que refleja saldos
disponibles, declaración de renta del último año y cualquier otra .<br>
•Certificación laboral, indicando cargo, sueldo y tiempo de servicio (adjuntar
último desprendible de nómina).<br>
•Para pensionados: Resolución de la pensión y último comprobante de pago.<br>
•Para independiente: Certificado original de la Cámara de Comercio o
declaración de renta del último año.<br>
•Para estudiantes: Certificado de estudios que indique curso y período de
vacaciones.<br>
•Para menores de edad: Registro de nacimiento y permiso de salida del país.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
•Para personas a cargo: Carta de responsabilidad de gastos, registros de
nacimiento y matrimonio para demostrar parentesco.<br>
•Si es invitado por Residentes temporales o permanentes o panameños desde
Panamá: Debe enviar declaración jurada, <span class=SpellE>adjutando</span>
copia autenticada de la cédula de identidad personal del nacional; copia de los
datos generales del pasaporte y del documento de identidad personal extranjero;
copia del recibo de pago por la prestación de servicios públicos, donde conste
la ubicación de la residencia del responsable.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
•Si es invitado por una <span class=SpellE>compañia</span>: Carta de
invitación; si la <span class=SpellE>compañia</span> es privada, Certificado de
Registro Público; copia de aviso de operaciones; copia de la clave de <span
class=SpellE>operacion</span> de la zona libre de Colón; copia de pago por la
prestación de servicios públicos donde conste la ubicación de la empresa
responsable; solvencia económica.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite tiene una duración de 48 horas.<br>
<br>
Toda persona, sin importar su nacionalidad, que posea VISA debidamente expedida
por los Estados Unidos de Norteamérica, Australia, Canadá, Reino Unido y de
cualquiera de los estados que conformen la Unión Europea, y haya utilizado por
lo menos una vez para ingresar al territorio del estado otorgante, podrá
ingresar al territorio nacional de Panamá sin VISA.<br>
<br>
Si la comunicación anterior no aplica en su caso, debe tramitar visa.<br>
<br>
Derechos Consulares:<br>
<br>
• Al momento de radicar los documentos debe cancelar $255.000 y el valor del <span
class=SpellE>courier</span> del envío de documentos a panamá.<br>
<br>
<br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal><span style='font-size:10.0pt;line-height:115%;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>Este contenido debe
tenerse en cuenta como informativo de tipo general y en todos los casos debe
confirmarse previamente a la iniciación de cualquier trámite por cuanto los
consulados se reservan el derecho de modificar procedimientos y requerimientos
sin previo aviso.</span></p>

</div>