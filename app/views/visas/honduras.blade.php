<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Carrera 12 No 119-52 Barrio <span
class=SpellE>Multicentro</span>.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Teléfonos: 629 3277 - 629 3302 - 620 9528.<br>
Fax: 629 3277 Ext. 114.<br>
Horario de atención: Lunes a viernes de 9:00 a.m. a 12:00 m. y de 2:00 p.m. a
4:00 p.m.<br>
E-mail: info@embajadadehonduras.org.co<br>
Página Web: www.embajadadehonduras.org.co<br>
<br>
A partir del 12 de septiembre de 2007, entró en vigor la supresión recíproca
del requisito de visa entre Colombia y Honduras. El acuerdo establece que los
nacionales de ambos países contratantes, titulares de pasaportes corrientes
vigentes, podrán entrar, permanecer y salir del territorio del otro país sin
requisito de visa, por un período de hasta noventa (90) días, contados a partir
de la fecha de entrada, para fines de turismo, tránsito o negocios. La
permanencia podrá ser extendida por las autoridades competentes, de conformidad
con los respectivos requerimientos de inmigración.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Sin embargo, se deben cumplir con ciertas
regulaciones, siendo de estricto cumplimiento según exigencia de las
autoridades migratorias de Honduras:<br>
• Pasaporte con una vigencia mayor a seis meses.<br>
• Pasaje de ida y vuelta o de continuación de viaje a otro país con su
correspondiente visa, si fuera el caso.<br>
• Todo viajero nacional o extranjero que salga o ingrese a Honduras, debe
presentar el certificado internacional de la vacuna contra la fiebre amarilla,
el cual será reconocido como válido sólo si la vacuna ha sido aplicada mínimo
diez (10) días antes de su ingreso a Honduras y su fecha de aplicación no sea
mayor a diez (10) años. Presentar certificado médico en caso de poder no
aplicarse dicha vacuna.<br>
• &quot;Reserva de hotel&quot; o &quot;Carta de invitación&quot; suscrita por la
empresa, institución o persona anfitrión, detallando el motivo del viaje,
nombre, dirección, teléfono, fecha del mismo, dirección exacta y número de
teléfono donde se dirigirá en Honduras, tiempo de estadía en el país y/o
presentar &quot;reserva de hotel o constancia de hospedaje&quot; de la
persona/institución que invita al país. (Disposición de las Aerolíneas).<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Las personas cuya nacionalidad no se
encuentre dentro de la excepción de visa, siendo residentes permanentes en
Colombia podrán hacer el trámite de la visa en la embajada de Bogotá.<br>
<br>
Para consultar la documentación para aplicar a una visa de estudio, trabajo,
residencia etc., a tramitar directamente a su arribo a Honduras, puede ingresar
a la página web www.gobernacion.gob.hn (Sección “Migración y Extranjería”).<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta
    como informativo de tipo general y en todos los casos debe confirmarse
    previamente a la iniciación de cualquier trámite por cuanto los consulados
    se reservan el derecho de modificar procedimientos y requerimientos sin previo
    aviso.</span><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
    mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>