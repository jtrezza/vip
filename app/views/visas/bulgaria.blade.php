<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>El trámite de la visa debe realizarlo personalmente el
solicitante.<br>
<br>
EMBAJADA DE BULGARIA EN BRASIL:<br>
Dirección: SEN 8 Asa Norte.<br>
CEP: 70800-911 Brasil</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Teléfonos: (61) 322 36193 - 322 39849<br>
E-mail: embassy.brasilia@mfa.bg</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
La Embajada de Bulgaria en Brasil no está todavía autorizada a hacer los
trámites de visas para ciudadanos de Colombia, Ecuador y Venezuela. En casos
esporádicos la Embajada podrá ayudar.<br>
<br>
Todos los solicitantes de visa que tengan la intención de viajar a Bulgaria
están obligados a presentarse ante la Embajada para la toma de huellas
dactilares y fotografía. La cita se debe programar a través del siguiente
correo eleonora.dimitrova@mfa.bg<br>
<br>
Todos los viajeros (de cualquier nacionalidad) que ingresan a Bulgaria deben
presentar un seguro de asistencia <span class=SpellE>medica</span>
internacional.<br>
<br>
Los ciudadanos que tengan una visa <span class=SpellE>Schengen</span> vigente
de múltiples entradas NO necesitan visa para Bulgaria, si su estancia es de 90
días dentro de los 6 últimos meses, sin embargo la <span class=SpellE>policia</span>
de fronteras, se reserva el derecho de solicitar la presentación de solvencia <span
class=SpellE>ecónomica</span> ( tarjetas de crédito, extractos bancarios, EUR
100 por día ).<br>
<br>
TURISMO:<br>
<br>
- Pasaporte válido al menos tres meses después de la fecha prevista de salida
de Bulgaria y pasaporte anterior, si aplica. Debe tener dos páginas en blanco
para estampar visa. Si el pasaporte no contiene ninguna visa o sellos de
entrada y salida del país, deberá presentar el Certificado de Movimientos
Migratorios de los últimos 5 años expedido por la Unidad Administrativa
Especial, en su respectivo sobre sellado.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Copia del pasaporte vigente.<br>
- Formulario diligenciado a máquina o letra imprenta.<br>
- Una fotografía de 3.5 x 4.5 a color fondo blanco.<br>
- Original y copia de la reserva aérea ida y vuelta.<br>
- Original y copia de la reserva de alojamiento durante la estadía en la
República de Bulgaria y en todos los países que se van a visitar durante el
viaje.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
- Documento que compruebe el propósito del viaje (invitación simple o oficial,
viaje organizado por agencia, asistencia al congreso u otro evento en la
República de Bulgaria), original y copia.<br>
- Fotocopia de los tres (3) últimos extractos bancarios.<br>
- Constancia de recursos suficientes para cubrir los gastos de subsistencia,
con valor mínimo de 50 Euros por cada día de permanencia, conforme a lo
solicitado para la visa o el equivalente de ésta suma en otra moneda libremente
convertible, pero no menos de 500 Euros o el equivalente de ésta suma en otra
moneda libremente convertible o un documento de los servicios de prepago para
el turismo. Copia de la tarjeta de crédito y estado de cuenta bancaria.<br>
- La prueba de la <span class=SpellE>poseción</span> de fondos suficientes para
cubrir los gastos de alojamiento por un importe mínimo de 50 euros por cada día
de estancia, conforme a lo solicitado por la solicitud de visado o el
equivalente de ésta suma en otra moneda libremente convertible o un documento
de prepago noches de alojamiento en un establecimiento de alojamiento colectivo
turístico, o una invitación estándar por el cual un individuo o colectivo
búlgaro dijo que dicha persona realice para cubrir todos los gastos que puedan
surgir durante la estancia del extranjero en el territorio de la República de
Bulgaria<br>
- Constancia de recursos suficientes para cubrir los costos de salir del país,
o un billete, y en el caso de la entrada de un vehículo de motor operados por
la persona, un mínimo de 100 Euros o el equivalente de ésta suma en otra moneda
libremente convertible.<br>
- Seguro médico internacional válido para la estadía en Bulgaria con una
cobertura mínima de 30.000 Euros que cubra todos los gastos de repatriación,
asistencia médica urgente y hospitalización urgente para el periodo indicado en
la visa, hecho en el país de origen. Debe presentar el recibo de la adquisición
del seguro médico internacional. La póliza del seguro médico y el recibo se
presenta en original y copia. Si no presenta estos documentos del seguro la
visa no se le entregará.<br>
- Carta de la empresa indicando el cargo, sueldo, tiempo de servicio, motivo
del viaje y periodo de vacaciones, con firma y sello del empleador.<br>
- Desprendibles de nómina de los últimos tres meses, con firma y sello del
empleador.<br>
- En caso de visitar familiares o amigos: formato de invitación oficial
certificada en las oficinas regionales de la policía. En esta invitación
oficial la persona que invita puede garantizar que se hace responsable por el pago
del alojamiento y gastos de estadía de la persona invitada incluyendo el seguro
médico.<br>
- Para trabajadores independientes: Es indispensable presentar el certificado
de la Cámara de Comercio y certificación de ingresos elaborada por contador,
adjuntando copia de la tarjeta profesional.<br>
- Para personas a cargo (hijos, esposa, etc.), adjuntar carta de
responsabilidad de gastos.<br>
- Para menores de edad: Se debe adjuntar el registro civil de nacimiento,
certificado de estudio y carta de autorización de salida del país firmada por
los padres, autenticada en notaría.<br>
<br>
<br>
DERECHOS CONSULARES:<br>
<br>
Valor visa 210 Reales brasileños</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La solicitud de visa se puede
    presentar no más de 3 meses antes del comienzo del viaje previsto.<br>
    Si después de visitar Bulgaria no regresa al país de origen, debe presentar
    la visa o permiso de entrada válidos de ese país.</span><span
    style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
    "Times New Roman";mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>