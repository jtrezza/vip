<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO Y/O NEGOCIOS:<br>
- Un (1) formulario con dos (2) fotografías recientes de 3 x 4 <span
class=SpellE>cms.</span> Una (1) fotografía adicional será solicitada en la
entrevista<br>
Pasaporte y copia de los datos biográficos con vigencia no menor a 90 días
después de expirar la visa<br>
- Fotocopia de la cédula de ciudadanía, tarjeta de identidad, o Registro Civil
de nacimiento para menores sin Tarjeta de Identidad<br>
- Extractos bancarios de los últimos tres (3) meses. No se aceptan extractos de
tarjeta de crédito o balances generales o informes de pérdidas y ganancias ó
Certificado de Ingresos o de Libertad como soporte financiero<br>
La cartas de responsabilidad económica legalizadas deben anexar pruebas de
solvencia económica (menores y amas de casas)<br>
- Seguro Médico Internacional con cobertura mínima de 30.000 euros al momento
de retirar el pasaporte con la visa aprobada<br>
Permiso de salida para menores, firmado por ambos padres y copia de los
pasaportes de los padres<br>
- Reserva de hotel para toda la estadía en el territorio <span class=SpellE>Schengen</span>
y reserva aérea<br>
Las reservas de hotel deben cubrir todos los días y las fechas del ciclo
completo del viaje, especialmente si se planea visitar más de un país <span
class=SpellE>Schengen</span><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>EMPLEADOS:<br>
- Certificación de empleo declarando que el solicitante no ha sido despedido,
cargo, <span class=SpellE>antigedad</span>, salario y periodo de vacaciones o
licencia. Además, cargo, dirección y teléfono de quien firma la carta<br>
- Para independientes, Certificado de Cámara de Comercio y si se trata del
dueño de una empresa, carta <span class=SpellE>membreteada</span> en la que se
le presente como tal<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NEGOCIOS:<br>
Además de los requisitos anteriores, anexar Registro Mercantil de Cámara de
Comercio<br>
- Carta de responsabilidad por los gastos y motivo de la visita<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>ESTUDIANTES:<br>
- El carnet de estudiante y carta de aprobación del permiso/licencia en papel
oficial del colegio con dirección, número telefónico, contacto y cargo de quien
firma<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VISITA FAMILIAR/TURISMO CON INVITACION:<br>
- Invitación con menos de tres (3) meses legalizada por la alcaldía respectiva,
declarando:<br>
- Duración prevista y propósito de la visita<br>
- Confirmación de que la persona invitada regresará a Colombia tan pronto
expire la visa<br>
- Copia del pasaporte y/o permiso de residencia de la persona que invita. Si <span
class=SpellE>aplica,presentar</span> los extractos bancarios de los tres (3)
últimos meses y carta de empleo/Cámara de Comercio del anfitrión<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: 20 días aproximadamente<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VALOR: Estos valores suele cambiar por lo
que se deben confirmar al inicio del trámite<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TRANSITO: Los colombianos necesitan visa
de tránsito aeroportuario para los países del área BENELUX (Bélgica, Países
Bajos y Luxemburgo) aún si no se sale del aeropuerto<br>
- Un formulario de aplicación con una (1) fotografía reciente de 3 x 4 <span
class=SpellE>cms</span><br>
- Pasaporte válido por lo menos 90 días después de expirar la visa<br>
- Fotocopia de la página principal del pasaporte y de la cédula de ciudadanía<br>
- Visa válida para el destino final y copia de la misma<br>
- Reserva de vuelo confirmado hacia y desde su destino final<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los derechos consulares no reembolsables
son de 50 euros, valor que se cancela en la entrevista. Los derechos tienen que
ser pagados en pesos colombianos a la tasa de cambio establecida por el
gobierno de Holanda<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACIÓN: 20 días aproximadamente<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visas.
Si el itinerario de viaje implica varias visas, el número de hojas libres se
debe ajustar a esta condición.<br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Todos los solicitantes deben
    presentarse a la embajada, especialmente los menores sin importar su edad,
    por su propia seguridad. Las solicitudes enviadas por correo o con terceros
    sin importar sin son familiares serán devueltas.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Solicitud de citas al teléfono (1) 638
    4243, de lunes a jueves entre 1:00 y 4 p.m., o por la web abajo indicada.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Una visa expedida por cualquier país <span
    class=SpellE>Schengen</span> permite al portador de esta, viajar libremente
    dentro del área <span class=SpellE>Schengen</span>, si no se expresa lo
    contrario en la visa. Los países <span class=SpellE>Schengen</span>
    incluyen: Alemania, Austria, Bélgica, Dinamarca, Finlandia, Francia,
    Grecia, Islandia, Italia, Luxemburgo, Países Bajos (Holanda), Noruega,
    Portugal, España y Suecia. Esta visa no permite al portador trabajar en
    estos países.<br>
    No hay un derecho legal para una visa y esta no es garantía para entrar a
    los territorios <span class=SpellE>Schengen</span>. La autorización final
    es competencia de los funcionarios de inmigración en la frontera.<br>
    La Embajada Real de los Países Bajos se reserva el derecho de pedir por
    escrito al solicitante presentación personal a su regreso a Colombia.
    Deberá presentarse con la carta original en la que se le hace la solicitud,
    pasaporte, tiquete de embarque y copia de la visa y de las páginas del
    pasaporte donde se encuentre el sello de ingreso al área <span
    class=SpellE>Schengen</span><o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>CONSULADO EN MEDELLIN<br>
    Dirección Calle 52 # 47 - 42 Oficina 1001 Edificio <span class=SpellE>Coltejer</span><br>
    <span class=SpellE>Telefóno</span> 514 07 56<br>
    Fax 514 07 56<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>EMBAJADA DE LOS PAISES BAJOS EN BOGOTA<br>
    Dirección : Carrera 13 No.93-40 piso 5<br>
    Teléfono : 638 4200<br>
    El horario de atención : lunes a viernes de 08:30 a 11:30 <span
    class=SpellE>a.m</span><br>
    entrega de pasaportes: lunes a jueves a las 14:30 <span class=SpellE>p.m</span>
    y viernes a las 12:00am<br>
    Página Web: www.mfa.nl/bog<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>