<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>EMBAJADA DE AUSTRALIA EN SANTIAGO DE CHILE<br>
Dirección: Edificio Isidora 2000, Isidora Goyenechea 3621 Torre B Piso 12.<br>
Teléfono: 562 550 3600.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Fax: 562 331 5955.<br>
Pagina Web: www.chile.embassy.gov.au<br>
Para consulta de visas o ciudadanía:
http://www.immi.gov.au/contacts/forms/americas/</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
<span class=SpellE>Call</span> Center: 161-32167603<br>
Correo electrónico: immigration.santiago@dfat.gov.au<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Nuevo centro de recepción de visas en
Medellín y está ubicado en el Centro Comercial El Punto de la Oriental, Carrera
46 #47-46 local 6010.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Horario de lunes a viernes de 9:00 a.m. a
6.00 p.m.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Más información Página Web: https://australia.visaops.net<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
La duración del trámite es de 45 días calendario, aproximadamente.<br>
<br>
La visa es otorgada según criterio de la Embajada.<br>
<br>
Los ciudadanos colombianos pueden tramitar la visa &quot;<span class=SpellE>Label</span>
Free&quot;, en este caso no debe enviar su pasaporte original, se debe anexar
una copia a color certificada ante notario de todas las páginas del pasaporte y
del documento de identidad. Una vez la visa sea otorgada le enviarán una carta
de notificación por correo electrónico.<br>
<br>
En el siguiente link encontrará las nacionalidades que requieren certificado de
vacunación contra la Fiebre Amarilla para ingresar a Australia
http://www.health.gov.au/yellowfever<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Visa de Visitante (subclase 600)<br>
Cómo Postular a la visa de Visitante (subclase 600)<br>
<br>
La visa de Visitante (subclase 600) permite visitar Australia para realizar
actividades de turismo o negocios. Es una visa temporal.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Esta visa tiene cuatro categorías:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Categoría Turista: para las personas que
viajan a Australia para pasar unas vacaciones, recreación o para visitar a familiares
y amigos. Si solicita la visa en Australia, debe estar en Australia cuando la
visa sea decidida. Si solicita la visa fuera de Australia, usted debe estar
fuera de Australia cuando la visa sea decidida. Información sobre cómo
postularse en el siguiente link:
phttp://www.chile.embassy.gov.au/sclecastellano/201303232.html.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Categoría Visita de Negocios: para las
personas de negocios que viajan a Australia para una visita de negocios breve.
Esto incluye ir a una conferencia, negociación o reunión. Usted debe estar
fuera de Australia cuando postule y cuando la visa sea decidida. Información
sobre cómo postularse en el siguiente link:
http://www.chile.embassy.gov.au/sclecastellano/201303232.html<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Categoría Familiar Patrocinado: para las
personas que viajan a Australia para visitar a su familia. Usted debe tener un
patrocinador a quien se le pueda pedir una garantía. Usted debe estar fuera de
Australia cuando postule y cuando la visa sea decidida. No podrá solicitar otra
visa durante su estadía en Australia. Para mayor información, visite la página
web en <span class=SpellE>inglés:http</span>://<span class=SpellE>www.immi.gov.au</span>/Visas/<span
class=SpellE>Pages</span>/600.aspx<br>
<br>
Con esta visa no se puede trabajar o prestar servicios a una empresa u
organización en Australia o vender bienes o servicios al público. Si tiene la
intención de llevar a cabo trabajo remunerado a corto plazo en Australia debe
solicitar la nueva visa de Trabajo Temporal (actividad de estadía corta)
(subclase 400).<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para todos los casos se debe diligenciar
el formulario 956 y anexar el itinerario del viaje.<br>
<br>
Requisitos visa de tránsito:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Formulario 876 debidamente
diligenciado.<br>
&#9679; Formulario 956 debidamente diligenciado y firmado por el pasajero (sólo
aplica si va a tramitar la solicitud de la visa por medio de una agencia de
viajes).<br>
&#9679; Foto reciente tamaño<br>
&#9679; Fotocopia del pasaporte a color de todas las páginas del pasaporte
autenticadas.<br>
&#9679; Reservas de viaje que indiquen fecha de ingreso y salida de Australia.<br>
&#9679; Una autorización para viajar al exterior ante notario para los menores
de 18 años que viajen solos o con uno de sus padres.<br>
&#9679; Fotocopia a color de <span class=SpellE>a</span> visa para entrar al
país de destino ( si aplica).<br>
&#9679; Registro civil de nacimiento para menores de edad.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; La Embajada de Australia tiene la facultad de solicitar documentación
adicional para su evaluación en caso que lo requiera, la cual puede incluir los
mismos requisitos de la visa de turismo.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Visa de Trabajo Temporal (Actividad de Estadía Corta) (subclase 400)<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La visa de Trabajo Temporal (actividad de
estadía corta) (subclase 400) es una visa temporal que le permite ingresar a
Australia con el fin de:<br>
&#9679; Trabajar a corto plazo, en un trabajo altamente especializado y que no
sea continuo<br>
&#9679; Participar en un evento o eventos específicos, por invitación de una
organización australiana<br>
&#9679; Con esta visa usted puede permanecer en Australia durante la validez de
su visa, que por lo general es de seis semanas o menos, aunque puede llegar a
ser hasta tres meses en algunos casos.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Usted deberá estar fuera de Australia al
momento de solicitar este tipo de visa y cuando la visa sea decidida.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Puede traer a su familia con usted a
Australia si los incluye en su solicitud de visa. Para postular. adicional a
los documentos de turismo o negocios deben anexar:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Formulario 1400<br>
&#9679; Formulario 956 debidamente diligenciado y firmado por el pasajero (sólo
aplica si va a tramitar la solicitud de la visa por medio de una agencia de
viajes).<br>
&#9679; Fotocopia a color de todas las páginas del pasaporte autenticadas<br>
&#9679; Fotocopia a color del documento de identidad por ambos lados
autenticado<br>
&#9679; Foto reciente tamaño pasaporte.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>En caso de participar en eventos &quot;
culturales o sociales &quot;<br>
<br>
&#9679; Carta del organismo/ institución que invita, detallando nombre y fechas
del evento, su rol y tareas a realizar. La carta deberá indicar además, el rol
en el evento del organismo/ institución que le invita.<br>
&#9679; Si postula como deportista amateur o para asistir/apoyar a un
deportista o equipo amateur: Carta de invitación indicando su participación/rol
en un evento amateur.<br>
<br>
En caso de realizar trabajos &quot; altamente especializados &quot;<br>
<br>
&#9679; Carta de oferta laboral o contrato detallando el cargo, duración del
trabajo, su rol y tareas a realizar. La carta deberá indicar además el rol en
el evento del organismo/institución que le invita.<br>
&#9679; Si va a realizar un trabajo como independiente deberá presentar una
copia de su programa e itinerario<br>
&#9679; Si está postulando para trabajar en la industria de la producción de
espectáculos/entretenimiento : Evidencia de que el espectáculo/producto no se
mostrará/distribuirá en Australia. Ejemplo: Contrato de distribución.<br>
<br>
En caso de colaborar en una &quot;emergencia nacional&quot;<br>
<br>
&#9679; Una carta de respaldo de una Institución de Emergencia del Gobierno de
Australia.<br>
<br>
Acompañantes: miembros de su grupo familiar<br>
<br>
El postulante puede incluir en su postulación a su pareja e hijos y/o los hijos
dependientes de su pareja. Deberá proporcionar los siguientes documentos de los
miembros de su familia:<br>
&#9679; Fotocopia a color de todas las páginas de los pasaportes certificadas
ante Notario Público<br>
&#9679; Fotocopia a color de los documentos de identidad por ambos lados
certificados ante Notario Público.<br>
&#9679; Foto reciente tamaño pasaporte ( con no más de 6 meses de anterioridad)<br>
&#9679; Si su pareja está incluida/o en su postulación: Copia de certificado/
acta de matrimonio o evidencia de relación de convivencia/unión libre, como por
ejemplo: cuentas de banco con ambos nombres , cuentas con nombres de ambos y
misma dirección, etc.<br>
&#9679; Si sus hijos están incluidos en su postulación : Certificado de
nacimiento con los nombres de ambos padres.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VALOR:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Valor visa de turismo y negocios (
subclase 600 ) USD 130.<br>
&#9679; Valor visa de trabajo temporal (subclase 400) USD 165.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Para la forma de pago debe cumplir las siguientes condiciones que exige
la Embajada:<br>
&#9679; Valor correo Bogotá - Santiago de Chile - Bogotá $ 85.400* hasta 500
gramos.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>FORMA DE PAGO:<br>
- Cheque bancario en dólares americanos, debe ser emitido por un banco que
tenga representación en Estados Unidos (el cheque debe tener la información
impresa que lo confirme).<br>
- El cheque debe ser emitido a nombre de &quot;Embajada de Australia&quot; o
&quot;<span class=SpellE>Australian</span> <span class=SpellE>Embassy</span>&quot;.<br>
- No se aceptan cheques personales, ni &quot;cheques de gerencia&quot;.<br>
- Si la agencia de viajes realiza el <span class=SpellE>tramite</span> del
cheque ante el banco tiene una duración de tres días hábiles.<br>
- La Visa de Tránsito es gratuita.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Valor visa de turismo y negocios ( Subclase 600) AUD $ 130</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Valor visa de trabajo temporal (estadía corta) (Subclase 400) AUD $ 165<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Pago con tarjeta de <span class=SpellE>credito</span>:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>En dólares australianos ($AUD) Visa, <span
class=SpellE>MasterCard</span>, American Express, <span class=SpellE>Diners</span>
Club y JCB<br>
Al pagar con la tarjeta de crédito debe:<br>
Aceptar los cobros asociados con gastos bancarios y gastos por conversión de
divisas aplicados por el mismo banco.<br>
Anotar el monto en dólares australianos (AUD), en la sección correspondiente al
pago, en su formulario de postulación.<br>
Las personas que tengan familiares o amigos en Australia pueden realizar el
pago de los derechos consulares directamente en cualquier oficina de DIAC (<span
class=SpellE>Department</span> of <span class=SpellE>Immigration</span> and <span
class=SpellE>Citizenship</span>) en Australia. Se debe adjuntar copia del
recibo de pago a su postulación de visa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Notas importantes para pagar con tarjeta
de crédito:<br>
<br>
Ingresar de manera legible los números de la tarjeta en la sección de su
formulario llamada &quot; <span class=SpellE>Payment</span> <span class=SpellE>details</span>/Detalles
de pago&quot;<br>
No debe ingresar los 3 dígitos de seguridad/ verificación de su tarjeta<br>
Debe completar claramente todos los campos en la sección del formulario &quot; <span
class=SpellE>Payment</span> <span class=SpellE>details</span>/Detalles de
pago&quot;<br>
Debe tener cupo suficiente para pagar el arancel de visa correspondiente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>Tourist</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'> (e676) Visa Online <span class=SpellE>Applications</span><br>
La actual postulación en línea para la visa <span class=SpellE>eTourist</span>
(subclase e676) se mantendrá disponible a quienes tengan pasaportes elegibles y
viajen a Australia con fines turísticos. Los países de la región de América del
Sur elegibles para postular en línea a la visa e676 son Argentina, Brasil y
Chile.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Visa Electrónica (subclase 601)<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La visa Electrónica permite visitar
Australia con fines turísticos o de negocios si tiene un pasaporte de un país
determinado.<br>
Los fines turísticos incluyen vacaciones, turismo y visitar familia y/o amigos.<br>
Los propósitos de negocio pueden incluir asistir a una conferencia, negociación
o un viaje de exploración de negocios.<br>
Con esta visa no puede trabajar o prestar servicios a una empresa u
organización en Australia o vender bienes o servicios al público. Si tiene la
intención de llevar a cabo trabajo a corto plazo en Australia, debe solicitar
la nueva visa de Trabajo Temporal (actividad de estadía corta) (subclase 400).<br>
Para mayor información, visite la página web en inglés: <span class=SpellE>Electronic</span>
<span class=SpellE>Travel</span> <span class=SpellE>Authority</span> (ETA) (<span
class=SpellE>subclass</span> 601)<br>
https://www.eta.immi.gov.au/ETA/etas.jsp<br>
<br>
<br>
Visa <span class=SpellE>eVisitor</span> (subclase 651)<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La visa <span class=SpellE>eVisitor</span>
permite visitar Australia con fines turísticos o de negocios si tiene un
pasaporte de un país determinado.<br>
Los fines turísticos incluyen vacaciones, turismo y visitar familia y/o amigos.<br>
Los propósitos de negocio pueden incluir asistir a una conferencia, negociación
o un viaje de exploración de negocios.<br>
Con esta visa no puede trabajar o prestar servicios a una empresa u
organización en Australia o vender bienes o servicios al público. Si tiene la
intención de llevar a cabo trabajo a corto plazo en Australia, debe solicitar
la nueva visa de Trabajo Temporal (actividad de estadía corta) (subclase 400).<br>
Para mayor información, visite la página web en inglés: <span class=SpellE>eVisitor</span>
(<span class=SpellE>subclass</span> 651)<br>
http://www.immi.gov.au/visitors/tourist/evisitor/eligibility.htm<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
<br>
<br>
DURACION<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Cuatro (4) semanas aproximadamente.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aplicaciones de visa si el
itinerario de viaje implica varias de ellas, el número de hojas libres puede
cambiar.<br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimiento sin previo aviso.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La Embajada de Australia tiene la
    facultad de solicitar documentación adicional para su evaluación en caso
    que lo requiera<br>
    Documentación que avale situación económica en Australia (en caso de que
    cubran los gastos del viaje y estadía).<br>
    Las personas con familiares en Australia y sean patrocinados por ellos,
    deben adjuntar diligenciado el formulario 1149 en original y firmado por el
    patrocinador del solicitante en Australia, junto con la solicitud de visa.
    También deberá presentar evidencia de relación familiar como: Certificado
    de nacimiento, libreta de familia, etc., y los documentos de evidencia de
    ciudadanía Australiana o de residencia permanente del patrocinador.<br>
    Para las visas de estudiantes deben consultar la página del consulado
    www.chile.embassy.gov.au<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'><br>
    EXÁMENES MÉDICOS:<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Dependiendo de varios factores, el
    solicitante podría someterse a una evaluación médica según sea el caso.
    Esta evaluación sólo puede ser realizada a través de médicos y radiólogos
    autorizados por la Embajada.</span><span style='font-size:10.0pt;
    mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
    "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
    mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    Si el pasajero es mayor de 75 años, deberá practicarse exámenes médicos y
    radiografía del tórax por los médicos autorizados por la embajada, además
    de la evaluación médica, deberá proporcionar copia del seguro medico
    internacional para el viaje que cubra el período de su estadía en
    Australia.<br>
    Si la evaluación médica presenta alguna anotación especial por parte del
    médico tratante, deberá ser remitida a la sección pertinente en Australia y
    el trámite se demorará 3 semanas en la evaluación de su postulación.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Para el Nuevo Procedimiento de toma de
    exámenes médicos en Colombia, por favor ingrese a la siguiente página:
    http://www.chile.embassy.gov.au/sclecastellano/ehealthcolombiaspa.html<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Para ver la lista de médicos
    autorizados en Colombia para residentes de Bogotá, Cali y Medellín ingresar
    a la siguiente página
    http://www.immi.gov.au/Help/Locations/Pages/our-offices.aspx. Cuando asista
    a la cita debe llevar copia del pasaporte.<br>
    <br>
    Para presentación personal:<br>
    <br>
    A partir del 21 de Enero de 2013 el servicio de atención al público será
    mediante citas, las cuales deben ser programadas por teléfono al 1 613 216
    7603<br>
    Las personas que deseen presentar postulaciones en nuestra oficina podrán
    utilizar nuestro servicio de &quot;buzón&quot; que estará disponible de
    Lunes a Viernes entre las 9:00 am y 11:00 am hora local de Santiago de
    Chile. Durante este horario no se atenderá consultas a través de las
    ventanillas<br>
    <br>
    La información debe <span class=SpellE>reconfirmarse</span>
    permanentemente, debido a que las embajadas y consulados se reservan el
    derecho de modificar sin previo aviso la documentación y requisitos
    establecidos.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>