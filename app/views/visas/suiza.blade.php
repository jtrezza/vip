<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La Embajada de Suiza tramita la solicitud
de visa para Hungría y Austria.<br>
<br>
La solicitud de visa Suiza de corta duración se debe tramitar ante el VFS
Global Suiza en Bogotá.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VFS Global:<br>
Dirección: Calle 81 No. 19 A - 18<br>
Teléfono: (+57 1) 382 6793</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
E-mail: Info.swissvaccol@vfshelpline.com<br>
Página Web: https://www.visaservices.org.in/Switzerland/Colombia</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Las solicitudes de visa de larga duración se deben tramitar por medio de la
Embajada de Suiza en Bogotá<br>
<br>
TURISMO Y/O NEGOCIOS:<br>
El trámite es personal y la documentación debe presentarse en original y copia<br>
&#9679; Pasaporte nacional y fotocopia de las páginas 1-2 -3-4 y última. Además
fotocopias de las visas vigentes. El pasaporte debe tener una validez de por lo
menos 3 meses después de la salida al territorio <span class=SpellE>Schengen</span>.
Presentar pasaportes vencidos.<br>
&#9679; Formulario de solicitud de visa completamente diligenciado y legible.<br>
&#9679; Dos fotografías tamaño pasaporte (3.5 x 4.5 cm) fondo azul, una pegada
en la solicitud y una adjunta con un gancho.<br>
&#9679; Reserva aérea ida y regreso.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Reserva de hotel confirmada para viajes individuales.<br>
&#9679; Certificación bancaria y extractos, estado de cuenta bancaria con
movimientos de los últimos 3 meses, último/s extracto/s cuenta corriente y
ahorros con el saldo a la fecha.<br>
&#9679; Seguro de viaje por el tiempo de permanencia en Suiza con una cobertura
mínima de 30.000 Euros o 50.000 SFR (francos Suizos).o 50.000 Dólares.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Para personas a cargo: Carta de responsabilidad de gastos y la
documentación completa de la persona responsable, sea en Suiza o donde viva la
persona.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
&#9679; Para menores de edad: Autorización notarial de salida del país de los
padres (formato Migración Colombia), fotocopia del registro civil de nacimiento
y la presencia de al menos un padre junto con el menor. El padre responsable
presenta la documentación personal completa requerida junto al menor.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Pasaporte con vigencia no menor a tres (3) meses desde la salida de
Suiza y pasaportes anteriores<br>
&#9679; Fotocopia de las visas vigentes y de las páginas 1,2,3,4 y última del
pasaporte<br>
&#9679; Dos fotografías tamaño pasaporte<br>
&#9679; Fotocopia de la cédula de ciudadanía<br>
&#9679; Reserva aérea de entrada y salida confirmada<br>
&#9679; Extractos bancarios de los últimos tres (3) meses, copia de la tarjeta
y certificación bancaria<br>
&#9679; Seguro de viaje por el tiempo de permanencia en Suiza, (<span
class=SpellE>Assist</span> <span class=SpellE>Card</span>, MIC, <span
class=SpellE>Aig</span> <span class=SpellE>Assist</span>, Universal <span
class=SpellE>Assistanse</span> u otra póliza aprobada por la embajada). Si el
seguro se tramita en Suiza se debe enviar por fax a la embajada<br>
&#9679; Para menores se requiere la autorización de salida del país autenticada
en notaría y la presencia de la menos uno de ellos en la embajada, fotocopia
del Registro Civil de nacimiento. El padre es quien presenta los documentos
personales y completos<br>
&#9679; Reserva de hotel confirmada<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Anexar además los siguientes documentos
según el caso:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Estudiantes: Constancia de
estudios especificados, con fecha de ingreso, fecha de vacaciones y fecha de
termino de estudios como también el último comprobante de pago de <span
class=SpellE>matricula</span>.<br>
&#9679; Independientes: Certificado de ingresos por contador con copia de la
tarjeta profesional del contador, original de la inscripción en la Cámara de
Comercio y los tres últimos recibos de aportes sociales, salud/pensión.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Empleados: Carta del empleador con fecha reciente indicando antigüedad,
cargo, salario, fecha de Vacaciones/licencia no remunerada y que el empleado se
va a reincorporar a sus actividades después de sus vacaciones, los 3 últimos
comprobantes de salario con deducciones de aportes sociales. Cámara de Comercio
de la empresa empleadora, especificación de cubrimiento de gastos por parte de
la empresa que los cubre.<br>
&#9679; Jubilados: Resolución de pensión y los tres (3) últimos comprobantes de
pago de la pensión (copias).<br>
&#9679; Tours programados: Confirmación de la agencia de viajes acerca del pago
de su viaje. (Carta de la agencia), deben incluir copia del <span class=SpellE>voucher</span>
de hoteles para todos los hoteles del viaje.<br>
&#9679; Viaje de visita a familiares o amigos residentes en Suiza: Invitación
de la persona desde Suiza, enviada directamente por e-mail (invitación modelo
se puede bajar de la página web) a la persona que desea viajar, acompañada de
una copia del pasaporte Suizo o tarjeta de identidad/ permiso de estadía
vigente de la persona que invita. El solicitante de la visa debe presentar su
solicitud y todos los documentos sin excepción 6 semanas antes del viaje.
Aunque la persona en Suiza invite y responda por los gastos del viaje hay que
presentar la documentación completa de parte de la persona que viaja. La
persona que invita debe ser el familiar en primer grado de consanguinidad.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Viaje de negocios: Carta de invitación de la empresa en Suiza, registro
de la Cámara de Comercio de la sociedad o compañía en Suiza, confirmando que el
solicitante es esperado en Suiza por dicha empresa, aclarando su tiempo de
estadía. <span class=SpellE>Reseva</span> hotelera y explicación escrita de la <span
class=SpellE>responsibilidad</span> de los gastos. Carta de presentación y de
responsabilidad de los gastos del viaje de la compañía a la que representa en
Colombia o para la que trabaja.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Además, la embajada puede solicitar los
siguientes documentos:<br>
<br>
&#9679; Antecedentes judiciales.<br>
&#9679; Declaración de garantía (para visita).Este documento es de competencia
y potestad de la embajada por la cual solamente será entregada por la Embajada
en los pocos casos que considere.<br>
&#9679; Declaración de renta y patrimonio (formato DIAN)de los últimos 3 años.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Otros documentos que la embajada estime necesarios para otorgar la
visa.<br>
&#9679; Registro de entradas y salidas del país (emitido por el Migración
Colombia).<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: Tres (3) semanas aproximadamente<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VALOR: Tarifas sujetas a cambio que se
deben confirmar con la agencia al iniciar el trámite<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aplicaciones de visa si el
itinerario de viaje implica varias de ellas, el número de hojas libres puede
cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; A partir del Enero de 2009, Suiza
integra el territorio <span class=SpellE>Schengen</span>. Por lo tanto viajeros
con visa <span class=SpellE>Schengen</span> no están obligados a solicitar vías
para Suiza.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir del 5 de septiembre de 2013 los
trámites para Visa <span class=SpellE>Schengen</span> deben realizarse
personalmente en las embajadas de las naciones miembros con sede en Bogotá.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Cada persona debe dirigirse a la embajada para solicitar la visa y registrar
sus huellas dactilares y una foto digital.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Este registro tendrá vigencia de cinco años y la información estará disponible
cada vez que se solicite una visa en los consulados de países que hacen parte
de la zona <span class=SpellE>Schengen</span>.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Después de hacer el registro y durante los 5 años de vigencia, se podrá tramitar
el documento por medio de una agencia de viajes.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Cualquier declaración falsa en
    la solicitud será motivo de rechazo<br>
    &#9679; El estudio de la solicitud tiene costo, sea aprobada o negada la
    visa<br>
    &#9679; Los documentos entregados con la solicitud no se devuelven<br>
    &#9679; Si el viaje de avión no se realiza directamente a Suiza, es
    necesario presentar primero las visas de los demás países que se visitan<br>
    &#9679; Si el destino final es otro país europeo, debe solicitarse primero
    la visa correspondiente<br>
    &#9679; Los titulares de pasaportes diplomáticos, de servicio o especiales
    colombianos no requieren vías si van en misión diplomática<br>
    &#9679; Las personas que viajan por motivos no especificados anteriormente
    o que desean permanecer en Suiza más de tres (3) meses, deberán presentar
    su solicitud utilizando formulario especial. La duración del trámite para
    este tipo de visa es de dos (2) a tres (3) meses y el formulario deberá ser
    diligenciado en uno de los siguientes idiomas: alemán, francés, italiano,
    retorrománico o inglés.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>CONSULADO DE SUIZA EN BOGOTÁ<br>
    Dirección: Carrera 9 No. 74 - 08. Piso 11.<br>
    Teléfonos: 349 7230.</span><span style='font-size:10.0pt;mso-bidi-font-size:
    11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'><br>
    Fax: 349 7195.<br>
    Horario de atención telefónica para asuntos de visa 382 6793: Lunes a
    viernes de 9:00 am a 12 m y 3:00 pm a 4:00 pm.<br>
    Horario de entrega de pasaportes: Lunes a jueves de 1:30 a 3:00 pm<br>
    Entrega de pasaportes/visas: Lunes a Jueves 2:00 m a 3:00 pm.<br>
    </span><span lang=EN-US style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-ansi-language:EN-US;mso-fareast-language:ES'>E-mail:
    bog.vertretung@eda.admin.ch<br>
    <span class=SpellE>Página</span> web: www.eda.admin.ch/bogota<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN-US style='mso-ansi-language:EN-US'><o:p>&nbsp;</o:p></span></p>

</div>