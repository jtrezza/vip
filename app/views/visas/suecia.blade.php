<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La Embajada de Suecia tramita las
solicitudes de visas para Noruega, Dinamarca, Finlandia e Islandia con los
mismos requisitos y formularios de Suecia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para tramites de visados <span
class=SpellE>Schengen</span> de corta duración, los documentos se deben radicar
en:<br>
<br>
VFS Global:<br>
Dirección: Calle 81 No. 19A - 18, Piso 4<br>
Teléfono: (+57 2) 620 5191</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
E-mail: info.esco@vfshelpline.com</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Página Web: http://www.vfsglobal.com/sweden/colombia/<br>
Horario de entrega de solicitudes:</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
8:00 a.m. – 2:00 p.m. (Lunes, Miércoles y Jueves)<br>
8:00 a.m. – 3:00 p.m. (Martes y Viernes)<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>Call</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'> center:<br>
+57 2 620 51 91<br>
+57 2 620 51 92<br>
+57 2 620 51 93<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO:<br>
&#9679; Formulario de solicitud de visa <span class=SpellE>Schengen</span><br>
&#9679; Formulario anexo “D”<br>
&#9679; Pasaporte con vigencia mínima de seis (6) meses y copia de la
contraportada y datos biográficos<br>
&#9679; Original y copia de la cédula<br>
&#9679; Original y copia del tiquete o <span class=SpellE>printer</span> de
reserva confirmada<br>
&#9679; Itinerario completo del viaje<br>
&#9679; Registro Civil de matrimonio y/o de nacimiento para verificar lazos
familiares<br>
&#9679; Original y copia de las dos (2) últimas Declaraciones de la Renta o
Certificado de Ingresos y Retenciones<br>
&#9679; Seguro médico internacional con cobertura mínima de 30.000 euros o US
$40.000<br>
&#9679; Original y copia de los extractos bancarios de los tres (3) últimos
meses<br>
&#9679; Reservaciones hoteleras en cada país que planea visitar incluyendo los
que no son <span class=SpellE>Schengen</span><br>
&#9679; Certificado laboral indicando cargo, sueldo, tiempo de servicio y
período de vacaciones<br>
&#9679; Dos (2) fotos de 5 x 5 <span class=SpellE>cms.</span> en fondo azul
claro (formato de visa americana)<br>
&#9679; Para independientes: Registro de Cámara de Comercio y/o certificado del
contador que incluya dirección, teléfono e ingresos mensuales<br>
&#9679; Para menores, Registro Civil de nacimiento y autorización de los padres
para salir del país autenticado<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Según motivo del viaje, el interesado debe
anexar los siguientes documentos:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para las visas para asistencia a cursos,
conferencias o para empresarios, anexar:<br>
&#9679; Formulario anexo “A” (para negocios, conferencias, cursos de
entrenamiento)<br>
&#9679; Carta de invitación del anfitrión indicando motivo del viaje y estadía<br>
&#9679; Carta de presentación de la empresa, indicando cargo, sueldo, motivo y
período de ausencia<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para visitar amigos y/o familiares anexar:<br>
&#9679; Formulario anexo “E” diligenciado por el anfitrión<br>
&#9679; Registro Civil (<span class=SpellE>personbevis</span>) del anfitrión<br>
&#9679; Registro Civil de nacimiento si visita padres/hijos/hermanos de
familiares y viajero<br>
&#9679; Si el anfitrión asume los gastos de viaje, anexar declaración de
impuestos del año anterior y constancia de salario<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>PROCEDIMIENTO:<br>
Consignar en el Banco de Occidente los derechos consulares, sucursal Avenida de
Chile, <span class=SpellE>cra</span> 7ª, 7148 cuenta No.219800083 – Ahorros<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El día de la entrevista, la embajada le
informará una fecha en la cual puede obtener la respuesta a su solicitud de
visa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Presentarse personalmente en el embajada
de lunes a jueves a las 09:00 a.m. con los formularios diligenciados y la
documentación completa, incluyendo menores de edad.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: La duración del trámite es 30
días aproximadamente. Para temporada alto mayo-agosto, el trámite puede demorar
hasta 2 meses cuando la aplicación deba enviarse a la Dirección Nacional de
Migración.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Este país pertenece al Grupo <span
class=SpellE>Schengen</span> y el trámite es personal<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aplicaciones de visa si el
itinerario de viaje implica varias de ellas, el número de hojas libres puede
cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; La embajada de Suecia atiende las
solicitudes de visas para Noruega, Dinamarca, Finlandia e Islandia con los
mismos requisitos y formularios de Suecia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir del 5 de septiembre de 2013 los
trámites para Visa <span class=SpellE>Schengen</span> deben realizarse
personalmente en las embajadas de las naciones miembros con sede en Bogotá.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Cada persona debe dirigirse a la embajada para solicitar la visa y registrar
sus huellas dactilares y una foto digital.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Este registro tendrá vigencia de cinco años y la información estará disponible
cada vez que se solicite una visa en los consulados de países que hacen parte
de la zona <span class=SpellE>Schengen</span>.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Después de hacer el registro y durante los 5 años de vigencia, se podrá
tramitar el documento por medio de una agencia de viajes.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Descargue el formulario de solicitud
    de visado <span class=SpellE>Schenguen</span>.<br>
    Verifique que su formulario esté debida y totalmente diligenciado.<br>
    No debe haber espacios en blanco; debe escribir N/A en cada espacio en el
    que las preguntas no apliquen a su caso.<br>
    Debe firmar en todos los espacios en los que la firma es requerida.<br>
    Todos los formularios deben estar firmados. Cuando se trate de menores de
    edad, la persona responsable por él/ella deberá firmar.<br>
    Donde se solicita su dirección de domicilio, debe escribir también la
    ciudad en la cual se ubica la dirección mencionada.<br>
    <br>
    CONSULADO DE SUECIA, DINAMARCA Y FINLANDIA EN BOGOTÁ<br>
    Dirección: Calle 72 Bis No. 5 - 83 Piso 8. Edificio Avenida Chile<br>
    Teléfono: 325 6200 - 325 6180.<br>
    Fax: 3256181.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'><br>
    E-mail: embsueca@cable.net.co - ambassaden.bogota@foreign.ministry.se<br>
    Página web: www.swedenabroad.com/bogota<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>VALORES:<br>
    Estancia entre 1 y 90 días SEK 315: $168.000<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Cónyuges e hijos menores de edad de
    ciudadanos de la Unión Europea residentes en el exterior no pagan derechos
    consulares<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Las personas exentas de pago pueden
    presentarse en la embajada con sus documentos en orden para entrevista, de
    lunes a jueves a las 09:00 a.m.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Estos valores pueden cambiar por lo
    que es necesario confirmarlos al iniciar el trámite.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>EXENTOS DE PAGO:<br>
    &#9679; Niños menores de 6 años<br>
    &#9679; Colegiales, estudiantes universitarios, de postgrado y profesores
    acompañantes, siempre y cuando su viaje sea con fines de estudio o
    especialización<br>
    &#9679; Científicos, siempre que el viaje sea con fines científicos<br>
    &#9679; Los siguientes familiares de ciudadanos de un país de la comunidad <span
    class=SpellE>Schengen</span> (UE, Noruega, Islandia y Suiza):<br>
    - Cónyuges y compañeros permanentes que hayan formalizado su relación
    mediante un contrato<br>
    - Hijos menores de 21 años o hijos que dependan económicamente de los
    padres<br>
    - Padres que dependan económicamente de sus hijos<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>