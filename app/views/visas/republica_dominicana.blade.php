<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Cónsul: <span class=SpellE>Crismely</span>
Lora Abreu.<br>
Dirección: <span class=SpellE>Av</span> 19 No. 118-95 Oficina 3012<br>
Teléfonos: 3003765 - 3003766 - 3003767 - 3003768.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
E-mail: consuldo_repdom@hotmail.com<br>
Página Web: www.consuladordencolombia.org</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Horario de atención:<br>
Presentación solicitudes de visas: Lunes a Viernes de 9:00 a.m. a 1:00 p.m.<br>
Entrega de pasaportes: Lunes a Viernes de 2:30 p.m. a 3:30 p.m.<br>
<br>
TURISMO<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Formulario debidamente
diligenciado<br>
&#9679; Dos (2) fotos de frente 3 x 4 cm., a color y fondo blanco<br>
&#9679; Pasaporte con vigencia mínima de seis (6) meses<br>
&#9679; Fotocopia de las visas vigentes y vencidas, del pasaporte vigente y
anterior si aplica.<br>
&#9679; <span class=SpellE>Orignal</span> y copia del Pasado Judicial vigente<br>
&#9679; Carta de solicitud dirigida al cónsul Licenciado JUAN L. MARTINEZ,
Cónsul General de la República Dominicana, que contenga nombre del solicitante,
fecha de entrada y salida, nacionalidad, propósito del viaje, número de
teléfono y dirección.<br>
&#9679; Certificado de trabajo en papel membrete indicando cargo, salario y <span
class=SpellE>antiguedad</span>.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
&#9679; Certificación bancaria indicando: fecha apertura cuenta, tipo de
cuenta- ahorros o corriente- promedio anual, saldo actual y fecha y valor del
último depósito</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
&#9679; <span class=SpellE>Printer</span> de la reserva aérea y hotelera<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para menores de edad<br>
&#9679; Registro Civil de nacimiento y permiso notarial de ambos padres además
del certificado de estudios.<br>
&#9679; Carta de responsabilidad de gastos, si aplica<br>
&#9679; Registro Civil de matrimonio o declaración notarial <span class=SpellE>extrajuicio</span>
en caso de unión libre<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Luna de Miel, copia del curso
prematrimonial, si no hay vínculo directo, carta mencionando parentesco
autenticada en notaría.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si es invitado a <span class=SpellE>convenciónes</span>
o <span class=SpellE>reuniónes</span> de <span class=SpellE>asociaciónes</span>
profesionales, deberán presentar carta en papel membrete del anfitrión.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si es invitado por un dominicano:<br>
&#9679; Carta de invitación o carta de garantía que el viajero no tomará <span
class=SpellE>mas</span> del tiempo de la visa. La carta debe indicar destino,
dirección y teléfono, relación con el anfitrión, solvencia económica con carta
laboral y tres (3) últimos extractos bancarios, copia de la residencia legal y
vigente del anfitrión<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para independientes:<br>
&#9679; Certificado de ingresos del contador y copia de su tarjeta profesional <span
class=SpellE>asi</span> como el Certificado de Cámara de Comercio de la
empresa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NEGOCIOS- simple o múltiple:<br>
&#9679; Pasaporte con vigencia mínima de seis (6) meses.<br>
&#9679; Formulario original 509 debidamente diligenciado.<br>
&#9679; Dos (2) fotografías 3 x 4 <span class=SpellE>cms.</span> de frente, a
color y fondo blanco.<br>
&#9679; Carta de solicitud dirigida al Secretario de Estado de Relaciones
Exteriores de la República Dominicana Ing. Carlos Morales Troncoso y/o al
Cónsul General Licenciado JUAN L MARTINEZ NUÑEZ, que contenga el nombre del
solicitante, propósito del viaje y aclarando el/los motivos de los viajes, la
responsabilidad de la empresa por los gastos y regreso antes del vencimiento de
la visa y tiempo de la visita suscrita por el beneficiario o persona moral
interesada.<br>
&#9679; Carta de invitación de la empresa en República Dominicana con sello y
copia del RNC (Registro Comercial Dominicano) y todos los teléfonos legibles de
dicha empresa donde se puedan contactar, en caso de no tener contactos con
alguna empresa en República Dominicana, deberá elaborar una carta explicando el
motivo de su viaje y presentación de su empresa, proyecto o algún tipo de
negocios a realizar en dicho país, especificando la actividad a que se dedica y
lo que va a ofrecer, y por cuánto tiempo permanecerá en territorio dominicano.<br>
&#9679; Comunicación en papel timbrado de la empresa que representan en
Colombia y enviarlo a <span class=SpellE>Republica</span> Dominicana anexando
los documentos que demuestren legalidad de la empresa, motivo del viaje y referencias
comerciales. La documentación se debe presentar en papel oficial de la entidad
que invita.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si la persona va a dictar conferencias o
cursos:<br>
&#9679; Original de la carta de solicitud de visa, carta de invitación de la
empresa que auspicia el evento.<br>
&#9679; Fotocopia de la visa USA si la posee. Si no la posee debe presentar el
Pasado Judicial vigente (original y copia)<br>
&#9679; Carta laboral donde especifique sueldo, cargo y <span class=SpellE>antiguedad</span>
en papel membrete de la empresa y debidamente firmado y sellado por la persona
autorizada.<br>
&#9679; Los últimos tres (3) extractos bancarios.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Trabajadores independientes:<br>
&#9679; Certificado del contador con copia de la tarjeta profesional, donde
especifique ingreso mensual y la actividad a la cual se dedique<br>
&#9679; Certificado de Cámara de Comercio si la posee.<br>
&#9679; Extractos bancarios de los tres (3) últimos meses<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Pensionados:<br>
&#9679; Copia de la resolución de pensión, donde especifique ingreso mensual y
últimos tres (3) extractos bancarios.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>En caso de que la empresa para la que
trabaje se haga responsable deberá de anexar carta de responsabilidad por el
titular autorizado, Cámara de Comercio de la misma y los extractos bancarios,
una fotocopia de su carné que lo vincule con la empresa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION:<br>
Visa turismo : 4 días hábiles<br>
Visa negocios simple ó múltiple : 5 a 8 días hábiles<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los colombianos no necesitan visa para
República Dominicana siempre que posean una visa vigente a Estados Unidos o <span
class=SpellE>Schengen</span>, Canadá o del Reino Unido, deben comprar tarjeta
de turismo e ingresar con pasaporte vigente, y tiquete de entrada y salida de
República Dominicana.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Esto incluye asistentes a congresos,
convenciones y seminarios, no expositores, ponentes o conferencistas.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La Tarjeta de Turismo tiene hoy un precio
de USD 15.00 (sujeto a cambio)<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los colombianos que tengan visa para los
Estados Unidos, Unión Europea, canadiense o de Inglaterra que viajen en
cruceros por territorio dominicano, deben adquirir la tarjeta de turismo por
cada entrada que realiza a República Dominicana.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Los documentos se deben
    presentar en original y copia</span><span style='font-size:10.0pt;
    mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
    "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
    mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    &#9679; Para todas las visas se debe comprar el formulario USD $5.00
    (sujeto a cambio). Los <span class=SpellE>demas</span> costos de los
    visados son variables por lo que es necesario confirmar con la agencia al
    inicio del trámite.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>HORARIO:<br>
    Presentación solicitudes : Lunes a viernes de 09:00 a 1:00 <span
    class=SpellE>p.m</span><br>
    Entrega de pasaporte : Lunes a viernes de 3:00 a 4:00 <span class=SpellE>p.m</span><o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Las visas tienen una vigencia de dos
    (2) meses a partir de la fecha de expedición<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>DERECHOS CONSULARES</span><span
    style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
    10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    Valor visa de turismo simple $83.500<br>
    Valor visa de negocios simple $104.000<br>
    Valor visa de turismo múltiples entradas $124.800<br>
    Valor pago extra (sólo para visa de turismo) $40.000<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Consulado en Cali:<br>
    Cónsul - Ana Lucia Paz Tenor<br>
    Dirección: Calle 70 Norte 3N-45<br>
    Teléfono: 6541146<br>
    Fax: 665 0393<br>
    Horario de atención: Lunes a viernes de 9:00 a.m. a 12:00 m y de 2:00 p.m.
    a 5:00 p.m.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>