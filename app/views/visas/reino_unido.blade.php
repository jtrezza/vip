<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite de la visa es personal.<br>
<br>
La solicitud de visa Reino Unido se debe tramitar ante el VFS Global Reino
Unido en Bogotá.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VFS Global:<br>
Dirección: Carrera 14 No 85 - 86, Oficina 407 <span class=SpellE>Torrre</span>
5<br>
Horario de atención: Presentación de solicitudes 9:00 am a 4:00 pm</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Página Web: http://www.vfsglobal.co.uk/colombia<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Embajada Británica:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Carrera 9 No. 76 - 49 Piso 8.<br>
Teléfono: 326 8300.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Horario de atención:</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Presentación solicitudes de visas: Lunes a viernes de 8:00 a.m. a 10:00 a.m.<br>
Entrega de pasaportes: Lunes a viernes de 11:30 a.m. a 12:00 m.<br>
Página web: www.ukincolombia.fco.gov.uk/es - www.visa4uk.fco.gov.uk -
www.visainfoservices.com</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES $ 231.000 aprox. pagos
con tarjeta de <span class=SpellE>credita</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La sección de visas se La Embajada de
Reino Unido implemento, para el continente de América un <span class=SpellE>Call</span>
Center para dar información y contestar preguntas acerca de la solicitud de
visas para Reino Unido y reglas de inmigración. Los solicitantes podrán hablar
con agentes de información de visas en el teléfono: 442080995462 de lunes a
viernes de 9:00 a.m. a 5:00 <span class=SpellE>p.m</span> Cada llamada tendrá
un costo de USD 12 que se pueden pagar con tarjeta débito o crédito usando un
sistema automatizado, además del costo de la llamada internacional al Reino
Unido.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los documentos se pueden presentar al
consulado 90 días antes del viajes.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VISA MULTIPLE:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Pasaporte vigente mínimo seis (6)
meses y pasaporte anterior<br>
&#9679; Diligenciar en MAYUSCULAS el formulario<br>
&#9679; Dos (2) fotografías recientes tamaño 3.5 x 4.5 <span class=SpellE>cms.</span>
El tamaño de la cara debe corresponder a 3 <span class=SpellE>cms</span> del
mentón a la coronilla y 2.5 <span class=SpellE>cms</span> de ancho, en fondo
blanco con el rostro descubierto<br>
&#9679; Certificación laboral indicando cargo, sueldo, tiempo de servicio y
periodo de vacaciones<br>
&#9679; Si es independiente anexar Registro de Cámara de Comercio y
certificación de ingresos expedida por un contador público con copia de la
cédula y tarjeta profesional autenticadas<br>
&#9679; Tres (3) últimos extractos bancarios de cuenta corriente, de ahorros,
tarjetas de crédito, cuentas en el exterior y certificación bancaria con saldo
final<br>
&#9679; Para invitados, copia del documento o visa del anfitrión. Si tiene
familiares o amigos en el Reino Unido debe anexar copia del pasaporte y de la
visa que les permite permanecer en el país (sean colombianos o británicos),
junto con carta de invitación, la que puede llegar <span class=SpellE>via</span>
fax - no se reciben documentos enviados directamente al fax del consulado-. Si
el patrocinador se encuentra en el Reino Unido debe traer evidencia financiera
de esa persona, ejemplo: extractos bancarios de los tres (3) <span
class=SpellE>ultimos</span> meses, y carta de empleo.<br>
&#9679; <span class=SpellE>Printer</span> de reserva aérea e itinerario<br>
&#9679; Carta de responsabilidad de gastos si el viaje es patrocinado por otra
persona<br>
&#9679; Certificación de estudios si aplica, indicando el tiempo disponible
para el viaje al Reino Unido<br>
&#9679; Menores de edad deben anexar Registro Civil de nacimiento y permiso de
salida del país firmado por ambos padres autenticado, para menores de 16 años.
El formulario debe estar firmado por alguno de los padres. Los menores que
ingresan al Reino Unido llevan en la visa la información de la persona con
quien viaja, por esta razón debe diligenciar el formulario con los datos
exactos de la persona que lo acompaña.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TRANSITO:<br>
&#9679; Pasaporte vigente mínimo seis (6) meses y pasaportes anteriores<br>
&#9679; Diligenciar formulario<br>
&#9679; Dos fotografías recientes tamaño 3.5 x 4.5 <span class=SpellE>cms.</span>
El tamaño de la cara debe corresponder a 3 <span class=SpellE>cms.</span> del
mentón a la coronilla y 2.5 <span class=SpellE>cms.</span> de ancho, en fondo
blanco con el rostro descubierto<br>
&#9679; Certificación laboral indicando cargo, sueldo, tiempo de servicio y
periodo de vacaciones o licencia<br>
&#9679; Visas para el próximo destino<br>
&#9679; Tres (3) últimos extractos bancarios de cuenta corriente, ahorros, tarjetas
de crédito o cuentas en el exterior, <span class=SpellE>CDTs</span> y
certificación bancaria a la fecha con saldo final<br>
&#9679; Invitados deben anexar copia del documento o visa del anfitrión<br>
&#9679; <span class=SpellE>Printer</span> de reserva aérea o itinerario<br>
&#9679; Carta de responsabilidad de gastos, certificado laboral, extractos
bancarios, etc.<br>
&#9679; Certificación de estudios o diplomas en original o copia autenticada<br>
&#9679; Menores de edad deben anexar Registro Civil de nacimiento y permiso de
salida del país firmado por ambos padres<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>ESTUDIANTE:<br>
&#9679; Pasaporte vigente mínimo seis (6) meses y pasaportes anteriores<br>
&#9679; Diligenciar formulario<br>
&#9679; Dos (2) fotografías recientes tamaño 3.5 x 4.5 <span class=SpellE>cms.</span>
El tamaño de la cara debe tener 3 <span class=SpellE>cms.</span> del mentón a
la coronilla y 2.5 <span class=SpellE>cms.</span> de ancho, en fondo blanco con
el rostro descubierto<br>
&#9679; Certificación laboral indicando cargo, sueldo, tiempo de servicio y
periodo de vacaciones o licencia<br>
&#9679; Carta de aceptación que compruebe que ha sido admitido al curso de
estudios, costos del mismo y comprobante de alojamiento<br>
&#9679; Tres (3) últimos extractos bancarios de cuenta corriente, ahorros,
tarjetas de crédito, cuentas en el exterior, <span class=SpellE>CDTs</span> y
certificación bancaria a la fecha con saldo final<br>
&#9679; Si va invitado, anexar copia del documento de identidad o visa del
anfitrión<br>
&#9679; <span class=SpellE>Printer</span> de reserva aérea o itinerario<br>
&#9679; Carta de responsabilidad de gastos además de certificado laboral,
extractos bancarios, etc.<br>
&#9679; Si es estudiante, anexar constancia de estudios o diplomas en original
o copia autenticada<br>
&#9679; Menores de edad deben anexar Registro Civil de nacimiento y permiso de
salida del país <span class=SpellE>auténticado</span> por ambos padres<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: La duración promedio para todas
las visas es de 8 días<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TERRITORIOS INDEPENDIENTES QUE REPRESENTAN
REINO UNIDO EN COLOMBIA<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los territorios que representan el Reino
Unido son:<br>
Bahamas, Bermuda, <span class=SpellE>Bostwana</span>, <span class=SpellE>Monserrat</span>,
Anguilla, Nigeria, Sierra <span class=SpellE>Leone</span>, Gibraltar, Kiribati,
<span class=SpellE>Lesotho</span>, <span class=SpellE>Swaziland</span>, Belice,
Malawi, <span class=SpellE>Srilanka</span>, <span class=SpellE>Malasya</span>,
Tonga, <span class=SpellE>Mauritius</span>, Gambia, <span class=SpellE>Kenya</span>,
<span class=SpellE>Cayman</span> <span class=SpellE>Islands</span>, British <span
class=SpellE>Virgin</span> Island (Anegada, <span class=SpellE>Jost</span> van <span
class=SpellE>Dyke</span>, <span class=SpellE>Tortola</span> y <span
class=SpellE>Virgin</span> <span class=SpellE>Gordan</span>), <span
class=SpellE>Falkland</span> <span class=SpellE>Islands</span>, Dominica,
Barbados, Granada, Antigua y Barbuda, St. <span class=SpellE>Vincent</span>
&amp; <span class=SpellE>the</span> <span class=SpellE>Grenadines</span>,
Trinidad y Tobago, St. <span class=SpellE>Kitts</span> &amp; <span
class=SpellE>Nevis</span>, Zambia y Santa Lucia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Tramites referidos: Antigua, <span
class=SpellE>Belize</span>, Bermuda, <span class=SpellE>Bostwana</span>,
Gambia, Ghana, Gibraltar British <span class=SpellE>Virgin</span> Island (Anegada,
<span class=SpellE>Jost</span> van <span class=SpellE>Dyke</span>, <span
class=SpellE>Tortola</span> y <span class=SpellE>Virgin</span> Gorda), <span
class=SpellE>Mauritius</span>, y Zambia. El consulado recibe la solicitud con
los documentos, los cuales son consultados con el país correspondiente, el
trámite puede demorar de 4 a 8 semanas.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para estos <span class=SpellE>tramites</span>
referidos la Embajada cobra un valor adicional de $ 251.000 por concepto de
envío de documentos al respectivo territorio.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El costo de la visa se debe cancelar
directamente por <span class=SpellE>via</span> <span class=SpellE>electronica</span>
con tarjeta de crédito. VISA, MASTER CARD y/o DINERS $ 160.000 aprox.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los Colombianos no necesitan visa de
turismo para ingresar a:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; St. <span class=SpellE>Vincent</span>
&amp; <span class=SpellE>Grenadines</span>.<br>
&#9679; Dominica hasta por 21 días.<br>
&#9679; Bahamas, Barbados, St. <span class=SpellE>Kitts-Nevis</span>, Trinidad
y Tobago hasta por 90 días.<br>
&#9679;<span class=SpellE>Turk</span> and Caicos hasta por 30 días si tienen en
su pasaporte una visa vigente a Estados Unidos, Canadá o Reino Unido.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NOTAS: La duración del trámite es de 10
días hábiles, aprox.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; La visa a Kenia, se tramita a la
llegada de este país.<br>
&#9679; Al ingresar a Bahamas debe presentar el Certificado Internacional de la
vacuna contra la fiebre amarilla (debe aplicarla 10 días antes del viaje).<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>REQUISITOS PARA VISA DE TURISMO O DE
NEGOCIOS:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Pasaporte vigente mínimo seis
meses y pasaportes anteriores, si alguno de los pasaportes anteriores esta
extraviado, adjuntar movimientos migratorios expedido por el DAS.<br>
&#9679; Formulario debidamente diligenciado en letras mayúsculas, con los dos
apellidos completos, número del pasaporte debe ir unido letras y números,
ejemplo. AG203040 no puede ser el número de la cedula.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Se debe tramitar a través de la página
web: www.visa4uk.fco.gov.uk<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>INSTRUCCIONES PARA DILIGENCIAR EL
FORMULARIO:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Aplicación en línea<br>
&#9679; Señalar &quot;I <span class=SpellE>have</span> <span class=SpellE>read</span>.
<span class=SpellE>the</span> <span class=SpellE>above</span> <span
class=SpellE>information</span> and <span class=SpellE>the</span> <span
class=SpellE>relevant</span> <span class=SpellE>guidance</span> notes&quot;<br>
&#9679; Continuar<br>
&#9679; Diligenciar los datos correspondientes.<br>
&#9679; País de residencia<br>
&#9679; Nacionalidad<br>
&#9679; Propósito del viaje: <span class=SpellE>Overseas</span> <span
class=SpellE>territory</span> &amp; <span class=SpellE>commonwealth</span><br>
&#9679; Tipo de visas: <span class=SpellE>Overseas</span> <span class=SpellE>territory</span>
<span class=SpellE>application</span><br>
&#9679; Declaración juramentada y formato adicional según el caso <span
class=SpellE>Belize</span>, <span class=SpellE>Mauritius</span>, Nigeria,
British <span class=SpellE>Virgin</span> Island.<br>
&#9679; Una foto reciente 3.5 x 4.5 cm, fondo blanco, cara completa, sin gafas
de sol, orejas destapadas, con <span class=SpellE>exepción</span> de las
personas que tienen que hacerlo debido a sus creencias religiosas o costumbres
étnicas.<br>
&#9679; Carta laborar indicando cargo, sueldo tiempo de servicio y tiempo
disponible para viajar. Si es estudiante anexar la constancia de estudios.<br>
&#9679; Tres últimos extractos bancarios originales<br>
&#9679; Para personas a cargo se debe anexar carta de responsabilidad de
gastos.<br>
&#9679; Impresión de la reserva aérea y hotelera.<br>
&#9679; Carta explicando el motivo del viaje.<br>
&#9679; Si tiene familiares o amigos en el país de visita, debe anexar copia
del pasaporte y visa que le permite estar allá, junto con la carta de
invitación (vía fax). Si no tiene familiares o amigos debe especificar el
nombre, dirección y teléfono del hotel donde se hospedará.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los documentos deben de ser presentados en
original y copia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite es netamente personal.
Consultar <span class=SpellE>mas</span> adelante Servicio de Biométricos en
Cali y Medellín.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Todos los documentos deben
    presentarse en original y copia autentica<br>
    &#9679; Los documentos se pueden presentar al consulado 90 días antes del
    viaje<br>
    &#9679; Después impreso el formulario NO se pueden hacer modificaciones<br>
    &#9679; Viajeros de <span class=SpellE>Bogota</span> deben solicitar cita
    previa<br>
    &#9679; La embajada del Reino Unido NO presta pasaportes<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>SERVICIO DE BIOMETRICOS EN CALI Y
    MEDELLIN<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>1. El viajero diligencia su formulario
    en línea<br>
    2. Cancelará el costo del trámite en línea, en dólares, con tarjeta de
    crédito o débito<br>
    3. Deberá hacer su cita en la ciudad donde desee que se le tomen sus datos
    biométricos, sea Cali o Medellín<br>
    4. Se presenta a la cita con todos sus documentos, en el sitio donde se
    tomarán sus huellas:<br>
    -Pasaporte<br>
    -Formulario<br>
    -Evidencia del pago en línea<br>
    -Documentos relevantes a su solicitud<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Una vez tomados los datos biométricos,
    se procederá a pagar el servicio de correo (DOMESA) para el envío<br>
    de sus documentos hacia Bogotá y el regreso de los mismos por la misma vía.
    DOMESA dispondrá de un<br>
    funcionario recogiendo los documentos y el pago en el mismo sitio de la
    toma de huellas. El costo aproximado<br>
    es de $86.000 colombianos, que se pagarán en efectivo el día de la cita.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Las fechas de visita para toma de
    huellas mes a mes pueden ser consultadas en las siguientes direcciones:
    www.visainfoservices.com ; www.ukincolombia.fco.gov.uk/es ;
    www.visa4uk.fco.gov.uk<br>
    El lugar indicado dice ser cada consulado, pero se aclara que en Cali es el
    hotel <span class=SpellE>Dann</span> <span class=SpellE>Carlton</span> y en
    Medellín, el hotel Belfort.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>