<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite de la visa debe realizarlo
personalmente ante el consulado.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Las citas para la solicitud de visa se
otorgarán exclusivamente en el correo electrónico visascostarica@gmail.com, en
horario de 6:00 a.m. a 6:00 p.m. durante días hábiles, los correos que se
reciban en un horario diferente serán borrados automáticamente por el sistema
(debe solicitar la cita con un mínimo de 30 días antes del viaje). En el correo
<span class=SpellE>electónico</span> debe indicar:<br>
<br>
Nombres y apellidos de la persona que solicitará la visa.<br>
Número de cédula de ciudadanía.<br>
Número de pasaporte de los solicitantes.<br>
Fecha de nacimiento del solicitante.<br>
Nacionalidad de los solicitantes.<br>
Números de teléfonos (fijo y celular).<br>
Correo electrónico en los que se puede localizar al solicitante.<br>
Tipo de visa para el que requiere cita (Turismo, estudiante, menor de edad o
residente provisional por vínculo).<br>
<br>
Los colombianos que posean visa de Estados Unidos, países de la Unión Europea
y/o de Canadá estampada en su pasaporte con una vigencia mínima de tres (3)
meses, podrán ingresar sin visa a Costa Rica.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO Y/O NEGOCIOS:<br>
<br>
Se debe pedir cita al teléfono 1-691 0204 o por correo electrónico
consuladocrcol@yahoo.com y el trámite es personal.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>- Pasaporte con vigencia mínima de seis
(6) meses<br>
- Original y copia del certificado judicial vigente para mayores de 18 años</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Certificado laboral indicando cargo, sueldo, tiempo de servicio y motivo del
viaje.<br>
- Una (1) foto de 3 x 4 <span class=SpellE>cms</span> a color<br>
- Formulario diligenciado<br>
- <span class=SpellE>Printer</span> de la reserva o fotocopia del tiquete<br>
- Original y copia del Certificado Internacional de Vacunación contra la Fiebre
Amarilla<br>
- Reserva de hotel o carta de invitación desde Costa Rica si aplica.<br>
- Para menores permiso de salida del país autenticado por ambos padres,
certificado de estudios y Registro Civil de nacimiento.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Carta de responsabilidad de gastos autenticada en notaría si aplica.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Solvencia económica, títulos de propiedad de bienes muebles o inmuebles,
estados financieros, tarjetas de crédito y fotocopia de los tres (3) últimos
extractos bancarios.<br>
- Al ingresar se deberá presentar tiquete de regreso o continuación de viaje y
se le podrá solicitar que</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
demuestre su solvencia (mínimo USD $500.00).</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Certificado de movimientos migratorios. Las personas con visa americana
vigente están exentas de este requisito.<br>
- Se debe anexar registro migratorio expedido por el D.A.S. (Calle 100-11B,
Bogotá) para personas que no</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
posean visa americana. En otras ciudades, confirmar dirección.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NEGOCIOS:<br>
- Adjuntar invitación de la empresa en Costa Rica especificando el motivo de la
visita, contactos y fechas.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Para la visa de negocios se exigirá los tres (3) últimos extractos bancarios
del solicitante adicional o los</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
extractos bancarios de la empresa.<br>
- Registro Mercantil de Cámara de Comercio si el viaje es de negocios y carta
de la empresa indicando motivo del viaje y responsabilidad en gastos.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: Un (1) día a partir de la fecha
de radicación.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>CONSULADO DE COSTA RICA EN BOGOTA</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Dirección : Cr 12 No. 114-37<br>
Horario de atención al público: Lunes a viernes de 08:30 a 12:00 m para radicar
documentos.<br>
Para entrega de solicitudes : 1:00 a 2:00 <span class=SpellE>p.m</span><br>
Las citas solamente se debe solicitar en el correo: visascostarica@gmail.com<br>
Página web: www.embajadadecostarica.org<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES: USD $32 los cuales se
consignan en el Banco de Occidente, cuenta corriente 228020921 a nombre de la
Embajada de Costa Rica. Valor sujeto a cambio.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>- Al ingresar a Costa Rica se debe
    presentar el Pasado Judicial y el <span class=SpellE>Carnét</span>
    Internacional de Vacunación contra</span><span style='font-size:10.0pt;
    mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
    "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
    mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    la fiebre amarilla, aplicada al menos 10 antes de ingresar a Costa Rica.<br>
    - La visa es autorizada por 30 días a partir de la fecha de expedición y la
    permanencia la dan a la entrada en</span><span style='font-size:10.0pt;
    mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
    "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
    mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    Costa Rica.<br>
    - En Bogotá sólo expiden visas por una entrada, las visas de entradas
    múltiples se solicitan en Colombia y las</span><span style='font-size:10.0pt;
    mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
    "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
    mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    estampan en Costa Rica<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Si la visa es DENEGADA, la persona
    puede reclamar la devolución del dinero, deberá solicitar en el Consulado
    una nota por concepto de devolución que podrá presentar al Banco y deberá
    esperar seis meses para poder optar por una visa de ingreso a Costa Rica.
    Es necesario tomar en consideración que la Sección Consular lleva registros
    de a quienes se ha denegado la visa, por lo que se solicita respetar la
    normatividad para evitar complicaciones posteriores.<br>
    A partir de enero de 2012, por motivos de orden fiscal, no será posible
    bajo ninguna circunstancia tramitar cartas de devolución de pagos por
    derechos consulares, efectuados con más de 30 días de anterioridad al día
    de la cita o de la solicitud de devolución. Toda solicitud de devolución
    deberá plantearla el interesado por escrito.<br>
    Si la solicitud de visa es denegada debe tomar en consideración las
    disposiciones del artículo 17 del Reglamento para el otorgamiento de visas
    de ingreso a Costa Rica, vigente:<br>
    &quot;Artículo 17: Contra la denegatoria de una visa no cabrá recurso
    ordinario ni extraordinario en virtud de lo establecido en los artículos 59
    y 222 de la Ley. No podrá plantearse una nueva solicitud, sino hasta
    pasados seis meses a partir de la denegatoria.&quot;</span><span
    style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
    10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    En Bogotá se expiden visas por una entrada, las visas de entradas múltiples
    se solicitan en Colombia y son estampadas en Costa Rica.<br>
    Una vez otorgada la visa, el usuario cuenta con 60 días para ingresar a
    territorio costarricense, por lo que es necesario que el solicitante tenga
    presente en el momento de solicitar la cita para la visa.<br>
    Ninguno de los documentos que se entregan <span class=SpellE>el</span> en
    consulado para trámite de visa, son devueltos.</span><span
    style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
    10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'><br>
    A partir del 15 de Diciembre de 2011, los documentos podrán ser
    apostillados en el Ministerio de Relaciones Exteriores de Colombia, no se
    requerirá legalización en la Sección Consular de esta Embajada.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>