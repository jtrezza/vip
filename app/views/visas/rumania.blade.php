<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite de la visa es personal.<br>
<br>
Embajada de Rumania en Colombia<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>Direccion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>: Carrera 7a No 92-58, Chico, Bogotá.<br>
Teléfono: 256 6438 Extensión 103.<br>
Fax: 256 6158.<br>
Página Web: http://bogota.mae.ro<br>
E-mail: seconsbog@etb.net.co</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Horario de trabajo de la Sección Consular: Lunes, <span class=SpellE>miercoles</span>
y viernes de 9:00 a.m. a 12:00 m. Martes y jueves de 1:00 p.m. a 4:00 p.m.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Antes de acercase a la embajada debe pedir
una cita vía telefónica (256 6438 - 256 6719) o enviar un correo electrónico
(seconsbog@etb.net.co).<br>
<br>
La duración del trámite es de 20 días, aproximadamente.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Requisitos generales para visa:<br>
<br>
• Pasaporte en original, que no venza en los tres meses después de vencerse la
visa.<br>
• Fotocopia de la hoja del pasaporte con la foto y los datos de identificación.<br>
• Formulario debidamente diligenciado y firmado por el solicitante. Para los
menores de edad los <span class=SpellE>formualrios</span> pueden ser llenados
por los padres, pero ambos padres tienen que firmar el formulario, aunque el menor
viaje acompañado solamente por uno de sus padres u otra persona.<br>
• Formulario de <span class=SpellE>declaracion</span>, debidamente
diligenciado.<br>
• Dos fotografías a color tamaño 3x4 cm, fondo blanco, no pegadas al formulario
de solicitud de visa.<br>
• Reserva de hotel, incluyendo la confirmación de parte de Rumania.<br>
• Reserva aérea de ida y vuelta, el solicitante debe presentar el pasaje aéreo
comprado en el momento de la devolución del pasaporte.<br>
• Si viene el caso, certificado de estudiante (expedido por la Secretaria de
Facultad no antes de 10 días, con la firma del Rector y el sello oficial).<br>
<br>
Otros requisitos generales para visas de corta estancia:<br>
<br>
Misión:<br>
<br>
• Nota verbal o carta oficial dirigida a la Embajada o Consulado de Rumanía,
que confirme la calidad oficial del solicitante y el propósito del viaje.<br>
• Seguro médico internacional para el período de estancia en Rumanía. (En
nuestras oficinas puede comprar el seguro de asistencia médica
correspondiente).</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
Turismo:<br>
<br>
• Fotocopia del <span class=SpellE>voucher</span> y / o programa / itinerario
turístico.<br>
• Fotocopia de la reserva aérea de ida y vuelta.<br>
• Seguro médico internacional. (En nuestras oficinas puede comprar el seguro de
asistencia médica correspondiente).</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Visita:<br>
<br>
• Carta de invitación autenticada de parte de un ciudadano rumano.<br>
• Fotocopia de la reserva aérea de ida y vuelta.<br>
• Seguro médico internacional para el período de estancia en Rumanía. (En
nuestras oficinas puede comprar el seguro de asistencia médica
correspondiente).</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
Negocios:<br>
<br>
• Carta de parte de la empresa/compañía rumana visitada, mencionando el
propósito del viaje y que ya está reservado/pagado el alojamiento, o;<br>
• Carta de la empresa que envía al solicitante, confirmando que se encarga de
los gastos que implica el viaje de su representante.<br>
• Fotocopia de la reserva aérea de ida y vuelta.<br>
• Seguro médico internacional para el período de estancia en Rumanía. (En
nuestras oficinas puede comprar el seguro de asistencia médica
correspondiente).</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
Actividades deportivas:<br>
<br>
• Invitación de los organizadores del evento deportivo, mencionando los nombres
de la delegación invitada, el período del certamen deportivo, si aseguran el
alojamiento, etc., con la autorización de las autoridades rumanas.<br>
• Seguro médico internacional para el período de estancia. (En nuestras
oficinas puede comprar el seguro de asistencia médica correspondiente).</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Actividades culturales, científicas, humanitarias u otras:<br>
<br>
• Carta de <span class=SpellE>presentacion</span> del propósito del viaje, de
parte de la persona física o moral rumana, indicando su dirección en Rumania,
la razón social y confirmando que asegura el alojamiento para el período de
estancia.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
• Fotocopia de la reserva aérea de ida y vuelta.<br>
• Seguro médico internacional para el período de estancia en Rumanía. (En
nuestras oficinas puede comprar el seguro de asistencia médica
correspondiente).</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
REQUISITOS ADICIONALES:<br>
<br>
Para visas de tránsito:<br>
<br>
• Fotocopia de la reserva aérea hasta el destino final.<br>
• Visa de ingreso en el país de destinación final y/o para otros países, hasta
el destino final.<br>
<br>
Para visas de corta estancia:<br>
<br>
• Si un solicitante de visa no está en la categoría de excepciones en el
momento cuando aplica para la visa rumana tipo turismo o negocios o visita a
familiares se necesita una autorización/permiso especial de parte de Rumania
(se consigue a través de una de agencia de viaje de allá, una empresa de allá o
un rumano de allá).</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Para obtener este permiso especial ante la OFICINA RUMANA PARA INMIGRACIONES la
agencia/empresa/persona pueda esperar hasta 60 días en Rumania.<br>
• Una vez que en Rumania se consigue el permiso especial, este documento debe
ser enviado en Colombia, en original, al solicitante de visa. El solicitante
puede aplicar para la visa dentro de 30 días a partir de la fecha cuando fue
otorgada la autorización en Rumania.<br>
• Cada solicitante necesita una invitación/permiso especial de OFICINA RUMANA
PARA INMIGRACIONES.<br>
• Los solicitantes deben cumplir también con todos los requisitos generales.<br>
<br>
Excepciones:<br>
<br>
Las siguientes categorías de ciudadanos colombianos no deben tener un
autorización/permiso en original de parte de OFICINA RUMANA PARA INMIGRACIONES,</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
sólo tienen que cumplir con los requisitos generales:<br>
<br>
• Los que están casados con ciudadanos rumanos. Hijos/hijas de ciudadanos
rumanos (en el caso que no sacaron el pasaporte rumano).<br>
• Los que tengan visa vigentes tipo <span class=SpellE>Schengen</span>,
americana, canadiense, de Reino Unido etc. o un permiso de residencia vigente
en cualquier de estos países.<br>
• Los invitados con fines de negocios de parte de autoridades de Rumania o de
parte de grandes empresas de Rumania. En este caso la empresa/autoridad de allá
debe enviar una carta de garantía, en original, al Centro Nacional de Visas en
Bucarest, asumiendo la responsabilidad con respecto de la presencia en Rumania
de los Colombianos.<br>
• Los invitados de parte de una embajada extranjera acreditada ante el Gobierno
de Rumania (deben presentar una carta oficial).<br>
• Los que tengan cartas de invitación de parte de las cámaras de comercio
extranjeras.<br>
• Los que están invitados de parte de las autoridades centrales o locales de
Rumania (Presidencia, Gobierno, ministerios, alcaldías, etc.). En este caso la
autoridad de allá debe enviar una carta de garantía, en original, al Centro
Nacional de Visas en Bucarest, asumiendo la responsabilidad con respecto de la
presencia en Rumania de los Colombianos.<br>
• Los padres, esposa/esposo, hijos/hijas del ciudadano de Colombia que tiene un
permiso de residencia vigente en Rumania.<br>
• Los padres de un ciudadano rumano (en el caso que no sacaron el pasaporte
rumano).<br>
• Hijos/hijas, mayores de edad, de ciudadanos rumanos (en el caso que no
sacaron el pasaporte rumano).<br>
• Personalidades de la diáspora rumana de Colombia (en el caso que no sacaron
el pasaporte rumano).<br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Derechos consulares:<br>
    <br>
    • Valor visa de corta o larga estadía una entrada $118.000.<br>
    • Valor visa de corta o larga estadía múltiples entradas $223.000.<br>
    • Valor visa de tránsito una entrada $88.000.<br>
    • Valor visa de tránsito dos entradas $118.000.<br>
    <br>
    * Valor sujeto a cambio sin previo aviso.<br>
    <br>
    En algunos casos existen costos de trámite, los cuales varían dependiendo
    del país donde se efectúe.</span><span style='font-size:12.0pt;font-family:
    "Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
    mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>