<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Por instrucción de la Embajada de Japón el
interesado debe comunicarse telefónicamente al 317 5001 Ext. 135 o vía correo
electrónico visa@ba.mofa.go.jp para obtener la información referente al trámite
de visa según el caso.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO:<br>
&#9679; Pasaporte vigente y anterior si lo hay<br>
&#9679; Formulario debidamente diligenciado<br>
&#9679; Foto color de 4,5 x 4,5 <span class=SpellE>cms</span> en fondo blanco<br>
&#9679; Fotocopia del Registro Civil de nacimiento<br>
&#9679; Fotocopia de la cédula de ciudadanía o equivalente<br>
&#9679; Copia del tiquete confirmado de ida y vuelta o <span class=SpellE>printer</span>
de reserva, con fecha de salida<br>
&#9679; Carta de empleo o certificación de actividad económica y/o fotocopia de
documentos que demuestren solvencia económica: Declaración de la Renta,
Registro de Cámara de Comercio, colillas de nómina, extractos bancarios y/o
Certificado de Libertad<br>
&#9679; Confirmación de hotel en Jaspón<br>
&#9679; Itinerario o plan turístico indicando los lugares a visitar<br>
&#9679; Carta de responsabilidad de gastos si aplica<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Visitas familiares, amigos o para contraer
matrimonio:<br>
A los seis (6) primeros documentos mencionados, anexar:<br>
&#9679; Carta de invitación expedida por el anfitrión o garante (<span
class=SpellE>Shohel</span> <span class=SpellE>Riyusho</span>)<br>
&#9679; Certificado de trabajo negocios del garante (<span class=SpellE>Zaishoku</span>
<span class=SpellE>Shoeismo</span>, <span class=SpellE>Tokibo</span> <span
class=SpellE>Tohon</span> o <span class=SpellE>Elgyo</span> <span class=SpellE>Kyokasho</span>)<br>
&#9679; Colillas de nóminas o copia de Declaración de Renta o certificado de
pago de contribución (impuestos) del garante (<span class=SpellE>Kyuyo</span> <span
class=SpellE>Shomeissho</span> <span class=SpellE>Gensen</span> <span
class=SpellE>Choshunhyo</span>, <span class=SpellE>Shoeismo</span>)<br>
&#9679; Certificado de registro de residencia (<span class=SpellE>Juminhyo</span>)
y <span class=SpellE>Koseki</span> (registro oficial de familia) del garante.
Si el anfitrión es colombiano o extranjero residente en Japón, adjuntar
certificado de registro de extranjero (<span class=SpellE>Toroku</span> <span
class=SpellE>Genpyokisanjiko</span> <span class=SpellE>Shomeisho</span>) y
pasaporte con visa o permiso de estancia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NEGOCIOS – Conferencias, ferias,
seminarios o capacitación de corta duración:<br>
&#9679; Pasaporte vigente y anterior si lo hay<br>
&#9679; Formulario totalmente diligenciado<br>
&#9679; Foto a color de frente de 4,5 x 4,5 <span class=SpellE>cms</span> en
fondo blanco<br>
&#9679; Fotocopia de la cédula de ciudadanía o documento de identidad<br>
&#9679; Fotocopia del Registro Civil de nacimiento<br>
&#9679; Copia del tiquete de ida y regreso o <span class=SpellE>printer</span>
de la reserva confirmados, incluyendo el destino siguiente<br>
&#9679; Carta de empleo o certificado de actividad económica y/o fotocopia de
documentos que demuestren solvencia económica.<br>
&#9679; Copia de la Declaración de Renta, Registro de Cámara de Comercio,
colillas de nómina, extractos bancarios y certificado de libertad<br>
&#9679; Confirmación del hotel en Japón o equivalente<br>
&#9679; Carta de invitación y programa del evento o comprobantes de la
actividad a realizar<br>
&#9679; Certificado de asistencia de la compañía (<span class=SpellE>Tokibo</span>
<span class=SpellE>Tahon</span>) a visitar o copia del permiso del negocio (<span
class=SpellE>Eigo</span> <span class=SpellE>Hyokasho</span>)<br>
&#9679; Certificado de contribución (<span class=SpellE>Nozel</span> <span
class=SpellE>Shoeismo</span>)<br>
&#9679; Documentos que comprueben negocios realizados como L/C o B/L<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: 10 días hábiles aproximadamente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VALOR: Los costos son variables por lo que
deben verificarse con la agencia de viajes al inicio del trámite.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aplicaciones de visa si el
itinerario de viaje implica varias de ellas, el número de hojas libres puede
cambiar.<br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimiento sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; La carta de responsabilidad de
    gastos, permiso de salida del país, Registro Civil nacimiento y Registro
    Civil de matrimonio deben legalizarse ante la Superintendencia de Notario y
    Registro y apostillarse ante el Ministerio de Relaciones Exteriores<br>
    &#9679; La partida de bautismo deberá legalizarse en la Nunciatura
    Apostólica y apostillarse ante el Ministerio de Relaciones Exteriores<br>
    &#9679; Para menores que viajen solos, a los documentos ya mencionados se
    debe sumar el permiso de los padres autenticado y apostillado<br>
    &#9679; Para visas de 30 y 90 días de múltiples entradas, anexar soporte de
    entrada a tercer país<br>
    &#9679; En ningún caso el consulado admite certificados de contadores<br>
    &#9679; En caso de ser invitado por un familiar, además de la invitación
    original, anexar los Registros Civiles de nacimiento del anfitrión e
    invitado, autentificados y apostillados con el fin de demostrar parentesco<br>
    &#9679; El trámite para extranjeros se realiza solo si presentan copia de
    la cédula de extranjería y de la visa colombiana vigente.</span><span
    style='font-size:12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:
    "Times New Roman";mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>