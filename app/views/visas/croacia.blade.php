<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite de la visa es personal.<br>
<br>
Consulado de Croacia en Brasil:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: <span class=SpellE>Shis</span>,
Qi09, <span class=SpellE>Conj</span>. 11 Casa 03 CEP 71625110 Brasilia D.F.<br>
Teléfonos: 55 613 248 0610<br>
Fax: 55 613 248 1708<br>
Página Web: www.mfa.hr<br>
E-mail: consuladocroacia@cantv.net<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La Embajada de la República de Croacia
informa que no requieren visa las personas con pasaporte colombiano que tengan
visa <span class=SpellE>Schengen</span> vigente tipo C o D. La visa debe estar
vigente durante todo el viaje.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
La visa debe presentarse con 3 meses de antelación a la fecha de viaje.
aproximadamente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>- Pasaporte original con vigencia mínima
de 9 meses, debe tener mínimo 3 páginas libres.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Formulario debidamente diligenciado.<br>
- Copia de la visa de Estados Unidos, si aplica.<br>
- Dos fotografías tamaño 3.5 x 4.5 a color, fondo blanco, el rostro debe ocupar
el 80 % de la foto, (no escaneada)<br>
- Comprobante de Agencia de Viajes que indica que el viaje ha sido pagado en su
totalidad.<br>
- <span class=SpellE>Voucher</span> de hotel, con número de confirmación de la
reserva.<br>
- Seguro de asistencia médica con cobertura mínima de 30.000 Euros o USD
40.000.<br>
- Carta de compromiso del seguro médico para las visitas siguientes.<br>
- Tarjeta de crédito.<br>
- Fotocopia del itinerario<br>
- Copia del extractos bancario de los últimos tres meses.<br>
- Si es invitado: Dos formatos de invitación originales autenticados desde
Croacia ( uno para la visa y otro para el viaje), - Carta laboral, recibos de
pago y extractos bancarios de los últimos tres meses.<br>
- Si va de negocios: Carta del empleador indicando el motivo del viaje a Bosnia
y Herzegovina, dos cartas de garantía con sello oficial y firma del
representante legal en Croacia (uno para la visa y otro para el viaje).<br>
<br>
DURACION: La duración del trámite es de un mes<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES:<br>
<br>
- Valor visa USD 79.<br>
- Valor transferencia USD 25 .<br>
- Valor transferencia Banco intermediario USD30<br>
- Valor correo Bogotá-<span class=SpellE>Brasíl</span>-Bogotá $239.300<br>
- No pagan derechos consulares: Menores de 12 años, familiares (hijos,
cónyuges) de ciudadano croata, titulares de pasaporte diplomático y oficiales
en viaje oficial.<br>
<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La visa debe tramitarse en Brasil<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>El costo de las visas varía por lo que
    debe confirmarse con la agencia de viajes al inicio del trámite.<br>
    Los menores de 14 años pagan el 50% de los derechos consulares.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>En caso de omisión de algunos de los
    requisitos anteriormente nombrados, la solicitud para la visa no será
    aceptada.<br>
    Las autoridades de inmigración pueden solicitar medios de manutención
    equivalentes a 100 Euros por día de permanencia.<br>
    Para presentación personal: solicitar cita vía telefónica al 202 337 1500 <span
    class=SpellE>ext</span> 235 o vía e-mail al correo m.arnaut@bhembassy.org<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>