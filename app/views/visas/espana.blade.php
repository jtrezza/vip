
<div class=Section1>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>General<o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:18.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:silver;mso-fareast-language:
  ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>CONSULADO DE ESPAÑA EN BOGOTÁ:<br>
  Dirección: Calle 94A No 11A - 70<br>
  Teléfono: 6283910 ext. 340<br>
  Teléfono desde España: 005716283910<br>
  Fax: 6283938 / 39<br>
  Página web: www.mae.es/consulados/bogota<br>
  E-mail: cogbogota@maec.es<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Este país pertenece al Grupo <span
  class=SpellE>Schengen</span><o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>El trámite de la visa de corta estadía
  es personal ante el VFS Global.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Todas las personas que vayan a realizar
  el trámite de visa personalmente, deben solicitar cita a través del siguiente
  link: http://www.vfsglobal.com/spain/colombia/ , es indispensable presentar
  la cita impresa que le arroja el sistema del VFS Global en el momento de
  presentarse.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Los solicitantes de visados nacionales o
  de larga duración deberán solicitar previamente la correspondiente cita en la
  página web https://www.citasconsuladoespanabogota.co.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>DERECHOS CONSULARES:<br>
  Menores de 6 años no pagan derechos consulares.<br>
  Valor visa pasajeros de 6 a 12 años $92.800<br>
  Valor visa pasajeros mayores de 13 años $159.100<br>
  Valor del VFS Global $53.000 (IVA incluido) los menores de 6 años están
  exentos del pago.<br>
  Las visas con vigencia superior a 90 días de estudiantes no tienen costo.</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  <br>
  Nota: A partir del 1 de Abril del 2014 los derechos consulares deben ser
  consignados máximo 1 mes antes de la presentación de los documentos, en caso
  de que la consignación supere el mes el pasajero debe solicitar el reembolso
  al VFS Global y cancelar nuevamente los derechos consulares.</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  <br>
  El Consulado de España devolverá los pasaportes de todos los solicitantes de
  visado que realizan el tramite personalmente a través de la empresa <span
  class=SpellE>Domesa</span> de Colombia S.A, los entregará en la dirección
  señalada por el interesado. El costo de este servicio deberá ser cancelado en
  la oficina del Centro de Solicitud de Visados.</span><span style='font-size:
  10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  <br>
  Valor correo <span class=SpellE>Domesa</span> individual $26.000 y grupo
  familiar en primer grado de consanguinidad (hasta 5 pasaportes) para Bogotá
  $21.500.<br>
  Valor correo <span class=SpellE>Domesa</span> individual $44.000 y grupo
  familiar en primer grado de consanguinidad (hasta 5 pasaportes) para fuera de
  Bogotá $31.000.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>NOTA IMPORTANTE PARA VISADOS SCHENGEN:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>A partir del 5 de septiembre de 2013 los
  trámites para Visa <span class=SpellE>Schengen</span> deben realizarse
  personalmente en las embajadas de las naciones miembros con sede en Bogotá.</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  Cada persona debe dirigirse a la embajada para solicitar la visa y registrar
  sus huellas dactilares y una foto digital.</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  Este registro tendrá vigencia de cinco años y la información estará
  disponible cada vez que se solicite una visa en los consulados de países que
  hacen parte de la zona <span class=SpellE>Schengen</span>.</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  Después de hacer el registro y durante los 5 años de vigencia, se podrá
  tramitar el documento por medio de una agencia de viajes.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Es muy importante informar a los
  pasajeros que van a realizar cambio de pasaporte por vencimiento o cualquier
  otra razón, que posean una Visa <span class=SpellE>Schengen</span> Vigente, y
  que haya sido expedida por el Consulado Español que deben volver a tramitarla
  con su nuevo pasaporte. El cambio de pasaporte ANULA dicha visa, según el
  criterio a aplicar por este Consulado.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>A partir del 23 de enero de 2012 las
  solicitudes de visados de corta duración (Visados <span class=SpellE>Schengen</span>)
  se presentarán en el Centro de Solicitudes de Visados VFS Global.</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  Dirección: Calle 81 No. 19A - 18, Piso 4<br>
  E-mail: info.esco@vfshelpline.com</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  Página Web: http://www.vfsglobal.com/spain/colombia/<br>
  Horario de atención: 7:30 am a 4:00 pm<br>
  <span class=SpellE>Call</span> center: 018005182498 - (571) 2941512<br>
  Horario de atención <span class=SpellE>Call</span> center: 8:00 am a 5:00 pm<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>REQUISITOS:<br>
  - Pasaporte vigente mínimo seis (6) meses, los pasaportes anteriores no se
  deben adjuntar. Los pasaportes sin modificación cuando el solicitante es
  mayor de edad usualmente son devueltos por el Consulado.<br>
  - Fotocopia de la hoja de datos biográficos del pasaporte, hojas donde
  pudiera haber cualquier rectificación de datos, visas <span class=SpellE>Schengen</span>
  anteriores expedidas en los últimos tres años y visa americana vigente (si
  aplica).<br>
  - Formato de compromiso de seguro médico diligenciado.<br>
  - Formato Comunicación de lugar a efectos de notificaciones en procedimiento,
  debidamente diligenciado.</span><span style='font-size:10.0pt;mso-bidi-font-size:
  11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  - Formulario totalmente diligenciado con las respectivas firmas y formato de
  compromiso de seguro diligenciado<br>
  - Una (1) fotografías a color en fondo blanco de 3 x 4 <span class=SpellE>cms</span>
  preferiblemente que no sean digitales, pegada al formulario.<br>
  - Reservas aéreas indicando las fechas del itinerario del viaje.<br>
  - Reserva hotelera o acta de invitación original desde España, con copia del
  DNI vigente de quien invita<br>
  - Seguro de viaje valido en todo el territorio <span class=SpellE>Schengen</span>
  y cobertura mínima de 30.000 euros o US$ 40.000<br>
  - Si quien invita es familiar, anexar original los registros civiles que
  acrediten parentesco o equivalentes<br>
  -Los tres (3) últimos extractos bancarios (original y copia) con saldo y
  cupos disponibles, se puede anexar los tres últimos extractos de las tarjetas
  de crédito.<br>
  - Copia autentica de inversiones -CDT, depósitos fiduciarios, ahorros etc.,
  si los hay<br>
  - Fotocopia Declaración de Renta.<br>
  - Menores: permiso de salida firmado por los padres, carta de responsabilidad
  de gastos y Registro Civil de nacimiento y certificado de estudio,
  apostillados.<br>
  Carta de responsabilidad en gastos indicando parentesco.<br>
  - El consulado acepta el avale económico únicamente de primer grado de consanguinidad,
  con sus respectivos registros civiles.<br>
  - Declaración de seguro medico firmada con numero de documento y numero de
  pasaporte.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  - Fotocopia de la cédula de ciudadanía.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- Para menores de edad registro de
  nacimiento, con permiso de salida firmado por los padres y carta de
  responsabilidad de gastos, (estos documentos deben estar autenticados) los
  menores requieren certificado de estudios.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- Para las personas a cargo demostrar
  parentesco, carta de responsabilidad de gastos autenticada y copia de pagos
  de salud <span class=SpellE>prepagada</span> o E.P.S.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- El consulado acepta el avale económico
  únicamente de primer grado de consanguinidad, con sus respectivos registros
  civiles.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Además de los requisitos anteriores
  deben anexar los siguientes documentos según el caso:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>PARA TURISMO EN TRANSITO.<br>
  Empleados: Certificación laboral indicando cargo, sueldo, tiempo de servicio
  periodo de vacaciones o licencia,</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  tres (3) últimas planillas de pago de aportes parafiscales (original y
  copia), tres (3) últimos desprendibles de la nómina (original y copia).<br>
  Pensionados: Resolución de pensión y tres (3) últimos desprendibles de pago
  (original y copia).<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Para las personas que solicitan visa de
  30 y 90 días de múltiples entradas, deben anexar soporte de la entrada a un
  tercer país y regreso al grupo <span class=SpellE>Schengen</span>.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Independientes: Si es comerciante
  original reciente del Registro de Cámara de Comercio y certificación del
  contador autenticado, original de la Declaración de Renta del último año,
  certificado de ingresos, tres (3) últimos comprobantes de pago de los aportes
  parafiscales y extractos bancarios.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>NEGOCIOS:<br>
  Carta de invitación de las empresas legalmente establecidas en España, con
  las que poseen vínculos comerciales indicando la actividad comercial y el
  tiempo de visita.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Carta del representante legal de la
  empresa en Colombia que mantiene vínculos comerciales con la empresa española
  especificando el cargo del solicitante, el objetivo y el motivo del viaje e
  indicando si se hacen cargo de los gastos del viaje.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Certificado actualizado de la Cámara de
  Comercio de la empresa en Colombia y medios económicos, Declaración de Renta
  y extractos bancarios de los últimos tres meses.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Si el interesado no viaja abalado por la
  empresa colombiana o española deben aportar la justificación de los medios
  económicos propios.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- Menores viajando solos: el anfitrión
  debe solicitar informe favorable ante la Subdelegación de Gobierno de la
  ciudad donde se encuentre.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>NOTAS:<br>
  - Los pasaportes anteriores de portada verde no se deben adjuntar<br>
  - Los anteriores documentos deben ser presentados en fotocopias, salvo
  certificaciones dirigidas al consulado</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  como las laborales o bancarias.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>VISA DE ESTUDIANTE:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Estudiantes inferior a 90 días:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Para este tipo de visa se deben anexar
  los documentos para visa de turismo, adicionando los siguiente:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- Admisión definitiva o matricula
  (original y fotocopia), para cursar estudios, indicar fecha de inicio, en
  donde se especifique la duración del curso en el centro educativo español
  público o privado. En caso de institución privada se debe adjuntar aprobación
  oficial del centro educativo; si es oficial</span><span style='font-size:
  10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  la respectiva resolución ó decreto. Las admisiones o prescripciones que <span
  class=SpellE>esten</span> condicionadas al cumplimiento de otros trámites o
  requisitos, no son válidas sin la acreditación de haber cumplido los mismos. No
  se admiten correos electrónicos o fax.<br>
  - Titulo oficial o asignaturas convalidados de estudios no reglamentados
  (cine, fotografía, bellas artes) deben</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  acreditar formación previa sobre la materia.</span><span style='font-size:
  10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  - Los becados deberán aportar original y fotocopia del documento expedido por
  la institución que concede la</span><span style='font-size:10.0pt;mso-bidi-font-size:
  11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  beca, indicando el cómputo total de la misma.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Estudiantes larga estadía:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- Pasaporte vigente mínimo seis (6)
  meses.<br>
  - Formulario totalmente diligenciado en dos (2) ejemplares<br>
  - Dos (2) fotografías 3x4 fondo blanco.<br>
  - Seguro de viaje valido en todo el territorio <span class=SpellE>Schengen</span>
  con una cobertura mínima de 30.000 euros o USD 40.000<br>
  - Certificado médico y resultados de análisis (original y fotocopia)
  expedidos por uno de los centros médicos autorizados por el consulado<br>
  - Certificado de alojamiento<br>
  - Admisión definitiva o matricula (original y copia) para cursar estudios
  superiores indicando fecha de inicio computo total de horas lectivas del
  curso. En caso de institución privada se debe adjuntar aprobación oficial del
  centro educativo y si es oficial la respectiva resolución ó decreto<br>
  - Titulo oficial o asignaturas convalidadas de los estudios realizados
  previamente autenticados y apostillados<br>
  - Para estudios no reglamentados -cine, fotografía, bellas artes- deben
  presentarse los recursos económicos,</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  becas, créditos educativos, ingresos periódicos que sirvan como sustento
  económico del estudiante durante</span><span style='font-size:10.0pt;
  mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  su permanencia en España. Si el sostenimiento <span class=SpellE>esta</span>
  a cargo de un familiar se debe acreditar parentesco por medio de Registro
  Civil de nacimiento apostillado; en caso de personas jurídicas se debe
  presentar Registro de <span class=SpellE>Camara</span> de Comercio. Si el
  aval económico proviene de Colombia debe ser suscrito ante notario y apostillado<br>
  - Pasado judicial expedido por el DAS original y copia legalizado.<br>
  - Declaración de renta del <span class=SpellE>ultimo</span> año fiscal del
  solicitante, familiar o empresa, comprobantes de pago de</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  nomina, pensiones con los correspondientes Certificados de Ingresos y Retenciones
  del <span class=SpellE>ultimo</span> año fiscal, tres</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  (3) últimos extractos bancarios, <span class=SpellE>CDTs</span>, depósitos
  fiduciarios o ahorros a la vista<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Para esta visa no se cobran derechos
  consulares pero esta</span><span style='font-size:10.0pt;mso-bidi-font-size:
  11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  exención no incluye el valor del trámite<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>NOTAS:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>La carta de responsabilidad de gastos,
  permisos de salida del país, registros de nacimiento y registros de
  matrimonio deberán estar legalizados en la superintendencia de notaria y
  registro y apostillados en el ministerio de relaciones exteriores. La partida
  de bautismo deberá ser legalizada en la nunciatura apostólica en el
  ministerio de relaciones exteriores.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Las personas que tengan pasaporte en
  blanco sin visas y sin sellos se salida y a quien les haya sido negada alguna
  visa deben presentarse ante el consulado así mismo quienes tengan la notación
  en el pasaporte &quot;este pasaporte reemplaza el pasaporte anterior por
  perdida&quot;.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Para la solicitud de la visa de menores
  que viajan solos, las agencias realizan el trámite con todos los documentos
  ya mencionados y con los permisos de los padres autenticados.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>No necesitan visa de transito
  aeroportuaria, los pasajeros que viajan vía Madrid y sean residentes o tengan
  un visado de alguno de los siguientes países: Reino Unido, Irlanda, Suiza,
  Andorra, Liechtenstein, San Marino, Canadá, Estados Unidos y/o visado <span
  class=SpellE>Schangen</span> de múltiples.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>El trámite de la visa a los pasajeros
  extranjeros se realiza solo si este presenta copia de la cédula de
  extranjería de la visa colombiana <span class=SpellE>vigemte</span>. Para las
  personas que deben hacer el trámite de la visa personalmente deben
  presentarse con los documentos para la solicitud de la visa y el consulado le
  asigna una ficha para solicitar la visa. Una vez presentados los documentos
  el trámite se demora aproximadamente 30 días.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>En caso de negación de la visa, el
  solicitante puede apelar ante el Tribunal Superior de la Justicia de Madrid,</span><span
  style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
  10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
  en un plazo de dos meses a partir de la notificación. Adjunto a la carta de
  apelación, se debe presentar la negación, documentos de soporte económicos
  adicionales a los presentados, si los hay.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Visados de estudio para licenciados en
  medicina, <span class=SpellE>cirugia</span> farmacia, psicología, ciencias
  químicas y ciencias biológicas:<br>
  <span class=SpellE>Ademas</span> de lo anterior:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>- Certificación de la Subdirección
  General de Ordenación Profesional del Ministerio de Sanidad y Consumo<br>
  - Titulo Oficial Español o Extranjero Homologado<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Sin excepción, la firma del solicitante
  debe estar en la casilla 37 tanto como al final del formulario.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>PROCEDIMIENTOS PARA ASIGNACION DE CITAS:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Visados de turismo, negocios, visita
  familiar:<br>
  Para este tipo de visado se debe de realizar personalmente o con agencia de
  viajes en la embajada de España en Bogotá en el horario de lunes a jueves a
  las 07:00 <span class=SpellE>a.m</span><o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>DOCUMENTACIÓN REQUERIDA:<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Original y copia de la consignación en
  el banco BBVA. Cuenta Corriente 001301710100011026 consulado de España, y la
  documentación solicitada para cada visa.<o:p></o:p></span></p>
  <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
  line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>No necesitan VISADO PARA ESPAÑA, los
  pasajeros que tienen tarjeta de residencia expedida y están siendo citados
  para la renovación de la misma, siempre que presenten el original de la
  Resolución de la Autoridad Española competente.<br>
  Los pasajeros que presenten original de reguardo para recoger la Tarjeta de
  Residencia o D.N.I<br>
  <br>
  LAS FOTOCOPIAS DEBEN SER TODAS DE TAMAÑO OFICIO.<br>
  <br>
  </span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>Requisitos</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:10.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
  pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
  Si el itinerario de viaje implica varias de ellas, el número de hojas libres
  puede cambiar.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
  style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
  mso-fareast-language:ES'><br>
  <span class=SpellE>Apartir</span> del día 6 de Junio de 2013, se suprime para
  la tramitación de visados <span class=SpellE>Schengen</span>, la APOSTILLA de
  los siguientes documentos:<br>
  - Registros civiles de nacimiento<br>
  - Registros civiles de matrimonio<br>
  - Cartas de responsabilidad económica<br>
  - Permisos de salida del país<br>
  <br>
  </span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
  style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
  mso-fareast-language:ES'><o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:18.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:silver;mso-fareast-language:
  ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta
  como informativo de tipo general y en todos los casos debe confirmarse
  previamente a la iniciación de cualquier trámite por cuanto los consulados se
  reservan el derecho de modificar procedimientos y requerimientos sin previo
  aviso.<br>
  <br>
  </span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
  mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
  color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>
  <div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:
  .0001pt;text-align:center;line-height:normal'><span style='font-size:18.0pt;
  font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";color:silver;mso-fareast-language:
  ES'>
  <hr size=1 width="100%" noshade style='color:silver' align=center>
  </span></div>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
   1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
    <td style='padding:0cm 0cm 0cm 0cm'></td>
    <td style='padding:0cm 0cm 0cm 0cm'></td>
   </tr>
   <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
    <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
     width="100%" style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:
     1184;mso-padding-alt:7.5pt 7.5pt 7.5pt 7.5pt'>
     <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
      <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
      <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
      line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
      mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
      color:black;mso-fareast-language:ES'>- El consulado no presta los
      pasaportes pero admite iniciar trámite de visa adjuntando copia de TODAS
      las hojas del pasaporte.<br>
      - No acepta tampoco certificaciones bancarias sin estar soportadas por
      los correspondientes extractos, o sin saldos disponibles. Tampoco son
      válidos documentos de posesión de bienes patrimoniales (inmuebles,
      hipotecas, vehículos, etc., ni certificaciones de bienes públicos ni
      constancias por Internet.<br>
      - Los créditos, comisiones de estudios y licencias remuneradas deberán
      acreditarse mediante presentación del Contrato respectivo en original y
      copia firmado por ambas partes, indicando el total del crédito, comisión
      o licencia.<br>
      - No se conceden visados para presentar Pruebas de Selectividad.<br>
      - Las Prácticas Estudiantiles solo son aceptadas si existe un convenio
      entre la universidad de Colombia y la institución española
      correspondiente. Es este caso deberá aportarse fotocopia del convenio
      sellada por la universidad. Las prácticas en empresas españolas están
      sometidas a Contrato de Trabajo en Prácticas y no es posible expedir
      visado de estudios.<br>
      - El trámite de la visa para viajeros extranjeros se realiza solo si presentan
      copia de la cédula de extranjería y de la visa colombiana vigente.<br>
      - En caso de negación de la visa, el solicitante puede presentar el
      recurso de apelación ante el tribunal de justicia de Madrid, en un plazo
      de dos meses contados a partir de la fecha de la notificación. Adjunto
      carta de apelación, debe presentar documento de negación, de soporte
      económico adicionales a los presentados -si los hay-y pasaporte vigente.<br>
      - Para solicitar visas de múltiples entradas se deben adjuntar a la
      solicitud las reservas aéreas, reservas de hotel y visas aprobadas de los
      países a visitar.<br>
      - Si el solicitante cambio de pasaporte, la visa que tenga vigente queda
      cancelada y debe volver a presentarse y pagar derechos consulares.<br>
      - Las personas que ingresen a España de visita, deben llevar la carta de
      invitación original expedida por una estación de policía de España,
      excepto para nacionales de Andorra, Suiza y países miembros de la
      Comunidad Económica Europea. Para las personas que se vayan a hospedar en
      hotel, se recomienda llevar constancia de reservación.</span><span
      style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
      mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
      color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:
      10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
      mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
      ES'><br>
      - Las personas que tienen Residencia Temporal en España y permanecen
      fuera de este país por más de 6 meses en un período de un año, pierden la
      residencia.<o:p></o:p></span></p>
      <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
      auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
      mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
      color:black;mso-fareast-language:ES'>IMPORTANTE: Los pasajeros
      colombianos pueden realizar tránsito sin visa por Madrid teniendo en
      cuenta lo siguiente:<br>
      <br>
      - El vuelo de conexión debe estar confirmado con la otra aerolínea.<br>
      - El equipaje debe estar chequeado hasta el destino final.<br>
      - Los pasajeros que realicen conexión con aerolíneas de bajo costo deben
      contar con <span class=SpellE>pasabordo</span>, de lo contrario deben
      tener visa vigente para recoger el equipaje.<br>
      - Los pasajeros que realicen tránsito sin visa de la T4 hay un bus que
      lleva al T1 siempre y cuando cumplan con las condiciones anteriormente
      mencionadas.<o:p></o:p></span></p>
      </td>
     </tr>
    </table>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>