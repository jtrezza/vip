<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>CONSULADO CANADA EN BOGOTÁ:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Carrera 7 No. 114 - 33 Piso 13<br>
Teléfonos: 657 9914 - 657 9951.<br>
Página web: www.canadainternational.gc.ca/colombia-colombie<br>
Para solicitar cita: bgota.im-citas@international.gc.ca<br>
<br>
Centro de solicitud de visas VFS :<br>
Dirección: carrera 11 a No. 93-52 oficina 303<br>
Página web: http://vfsglobal.ca/canada/colombia/</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
E-mail: info.cancol@vfshelpline.com<br>
Horario de atención al público: 09:00 Hrs. <span class=SpellE>to</span> 18:00
Hrs.<br>
Recepción de solicitudes: 09:00 Hrs. <span class=SpellE>to</span> 16:00 Hrs.<br>
Entrega de pasaportes: 09:00 Hrs. <span class=SpellE>to</span> 16:00 Hrs.<br>
Horario para agencias para presentar y reclamar: Lunes, Miércoles y Viernes de
4:00 a 5:00 pm<br>
Asistencia telefónica y dirección de correo electrónico:<br>
Asistencia telefónica: +5712941824<br>
Correo electrónico: info.cancol@vfshelpline.com<br>
<br>
Ciudadanía e Inmigración de Canadá ha firmado un acuerdo para abrir un Centro
de Solicitud de Visa (VAC) en <span class=SpellE>Bogota</span> con el objetivo
de hacer que el proceso de visa sea más fácil para los solicitantes en
Colombia.<br>
<br>
El VAC es sólo responsable de:</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
• Distribuir información relevante acerca de los requisitos para la visa
publicados por la Embajada de Canadá.<br>
• Recibir solicitudes y documentos de apoyo.<br>
• Revisar las solicitudes para asegurarse de que estén completas.<br>
• Asegurar el envío de las solicitudes a la oficina de visas en <span
class=SpellE>Bogota</span>.<br>
• Devolver el pasaporte con el resultado al solicitante de manera segura.<br>
<br>
El VAC está autorizado por Ciudadanía e Inmigración de Canadá para recibir su
solicitud. Sin embargo, si usted decide no presentarla en el, VAC usted puede
presentarla directamente en la Embajada. Consulte el sitio de internet de la
Embajada para mayores instrucciones.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Los solicitantes que presenten sus solicitudes a través del VAC deberán pagar
la cuota correspondiente por el servicio ya sea en el VAC o en Banco de
Occidente. La cuota por el servicio del VAC es de $58,000 pesos y se podrá
pagar en las oficinas del VAC con tarjetas de crédito Visa o <span
class=SpellE>MasterCard</span>. En caso de que el pago se realice en cualquier
sucursal de Banco de Occidente se deberán consignar a la cuenta No 219-04172-0
y cancelar $4.100 por valor de transferencia bancaria.<br>
<br>
Cada solicitante debe de pagar esta cuota, sin importar su edad o la razón de
su viaje.<br>
<br>
A partir del 4 de septiembre de 2013 entrará en vigencia el sistema biométrico
para este consulado y todos los aspirantes a visado para Canadá tendrán que
desplazarse a la ciudad de Bogotá para toma de huellas y foto, de manera
personal.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El consulado de Canadá manejará la
información directamente con los postulantes, sin admisión de intermediarios;
razón por la cual, ni las agencias de viajes, ni ANATO podrán interceder en
este proceso.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir de dicha fecha, no aceptarán
ningún tipo de intermediación en el diligenciamiento de los formularios y por
lo tanto no consentirán el cobro de tarifas o remuneración alguna por
representar o aconsejar a un solicitante en relación con su trámite de
inmigración canadiense; Exceptuando:</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
- Abogados y asistente legales que sean miembros acreditados de una asociación
legal canadiense<br>
- Notarios acreditado por la Chambre des <span class=SpellE>notaires</span> du <span
class=SpellE>Québec</span><br>
- Consultores de Inmigración acreditados<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Valor visa una y múltiples entradas
$183.500</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Valor visa múltiples entradas grupo familiar $903.500<br>
Valor VFS Global únicamente para menores de 14 años y mayores de 80 años
$52.600</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Valor toma de huellas y fotografía en el VFS Global $ 155.000, grupo familiar $
310.000<br>
La visa de tránsito no tiene costo.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO Y/O NEGOCIOS:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Formulario “Solicitud de visa”,
“Información adicional familiar” y “Autorización para entregar información a
representantes o personas designadas” según el caso, totalmente diligenciado
con firma y fecha. Si falta información su solicitud no se tramitará. Los
espacios en blanco se deben marcar N/A.<br>
&#9679; Cada solicitante, <span class=SpellE>asi</span> sea menor de edad se le
debe anexar solicitudes diligenciadas completamente por separado.<br>
&#9679; Esposos viajando juntos o padres con hijos menores de 18 años deben
entregar una (1) sola solicitud adjuntado hoja de información adicional
familiar para cada padre.<br>
&#9679; Dos fotos de frente, tamaño pasaporte y en fondo blanco.<br>
&#9679; Pasaporte válido mínimo un (1) año y pasaportes anteriores.<br>
&#9679; Carta dirigida al consulado solicitando la visa, motivo del viaje,
fecha de entrada y salida de Canadá, nombres y direcciones de las personas e
instituciones que visitará. Si va a estudiar, informe de los planes a su
regreso.<br>
&#9679; Copia de los extractos bancarios suyos de los 3 últimos meses, cuentas
de ahorro o <span class=SpellE>CDTs</span>, los cuales demuestren solvencia
económica suficiente para respaldar su viaje. No se aceptan descargados por
internet.<br>
&#9679; Copias de las declaraciones de renta de los 2 últimos años,
comprobantes de pensión.<br>
&#9679; Originales de Certificados de libertad y tradición de los bienes
inmuebles, máximo con un mes de expedición.<br>
&#9679; Las personas que tengan o hayan tenido (retirados) algún rango militar
(Fuerza Aérea, Armada, Ejército o Policía) deben anexar la hoja de vida oficial
expedida por las Fuerzas Armadas.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si va a visitar amigos o familiares:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Copia por ambos lados de la
tarjeta de residencia permanente o visa canadiense<br>
&#9679; Carta de invitación de hijos, no de yerno/nuera, además carta del
empleador y <span class=SpellE>Notice</span> of <span class=SpellE>Assesment</span>
(Avis de <span class=SpellE>Cotisation</span>) del <span class=SpellE>Revenue</span>
Canadá reciente de su hijo o <span class=SpellE>conyuge</span>.<br>
&#9679; Original y copia de Declaración de Renta o certificados de Ingresos y
Retenciones de los dos (2) últimos años, y comprobantes de nómina o pensión.<br>
&#9679; Carta de empleador indicando cargo, tiempo de servicio, salario y la
fecha exacta de salida y regreso de las vacaciones, certificado de Ingresos y
Retenciones (DIAN); esta documentación debe ser tanto de la persona que aplica
como del cónyuge.<br>
&#9679; Si los gastos son sufragados por la empresa, presentar además
documentación financiera propia.<br>
&#9679; Itinerario de viaje y reservaciones de hoteles si no se alojará donde
amigos o familiares. En hoja aparte suministre descripción clara y detallada
del propósito de la visita a Canadá incluyendo itinerario de viaje, nombre y
direcciones de las personas e instituciones que visitará. Si viaja acompañado ,
indique nombres y direcciones actualizadas de los acompañantes.<br>
&#9679; Si el empleador en Colombia o la empresa anfitriona en Canadá paga los
gastos del viaje se debe indicar de manera explícita en la carta,
adicionalmente debe agregar:<br>
- Original de la Cámara de Comercio de la compañía máximo con un mes de
expedición.<br>
- Copias de las Declaraciones de Renta de la empresa de los últimos dos años.<br>
Si va a visitar a una compañía en Canadá o va a asistir a un seminario o a un
curso debe adjuntar una carta de la entidad canadiense que le invita, carta de
registro en el congreso o seminario.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para personas de la tercera edad sin
recursos económicos propios:<br>
&#9679; Pruebas económicas de los familiares responsables y de quienes pagarán
los gastos.<br>
&#9679;Presentar seguro o asistencia médica internacional.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si es estudiante:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Certificado de estudios, tiempo
para la obtención del diploma, período de vacaciones y aceptación al próximo
semestre.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si no trabaja:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Documentación financiera de las
personas que cubran los gastos del viaje y carta de responsabilidad en gastos.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para menores de 18 años que viajen sin uno
de sus padres:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Carta de quien se hará responsable
durante su viaje.<br>
&#9679; Registro Civil de nacimiento o inscripción de nacimiento para los
ecuatorianos.<br>
&#9679; Carta notariada donde los padres ceden la custodia del menor a la
persona o entidad en Canadá.<br>
&#9679; Carta de esta persona o entidad aceptando la custodia del menor durante
el viaje.<br>
&#9679; Autorización de salida de los padres autenticada en notaría,
mencionando el destino y tiempo de estadía en Canadá.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si es divorciado:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Adjuntar datos completos del ex
esposo en la hoja de información familiar adicional.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: 30 días hábiles aproximadamente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VALOR: El costo es variable así que debe
verificarse con la agencia al inicio del trámite.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Lo que se paga incluye el estudio de la visa y no es reembolsable en ningún
caso.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TRANSITO: Adjuntar los mismos requisitos
de turismo y adicionar:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Tiquete aéreo y visas del
siguiente país.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>ESTUDIANTE:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Solicitud de visa de estudiante
debidamente diligenciada y firmada en la última página, con una (1) fotografía
tamaño pasaporte.<br>
&#9679; Pasaporte con vigencia mínima de un (1) año y que supere el tiempo de
estudio en Canadá.<br>
&#9679; Original o fax de la carta de aceptación como estudiante de la
institución donde desea llevar a cabo sus estudios, informando el curso en que
está registrado, duración y fechas del programa académico y fecha máxima para
registrarse.<br>
&#9679; Si piensa asistir a una escuela primaria o secundaria en Canadá, debe
presentar una carta de la junta escolar (<span class=SpellE>school</span> <span
class=SpellE>board</span>) de la escuela. Si es una institución privada deben
indicar nombre completo de la escuela, el nivel del curso y la duración del
mismo.<br>
&#9679; Si trabaja carta del empleador indicando cargo ocupado, tiempo de
servicio, sueldo y especificando vacaciones o licencia para estudiar en Canadá
y el tiempo exacto de la misma.<br>
&#9679; Si termino recientemente sus estudios debe suministrar copia
autenticada de Diploma o Certificación del centro de estudios donde termino. Si
va a continuar sus estudios a su regreso de Canadá debe presentar constancia de
aceptación de la entidad.<br>
&#9679; Pruebas de solvencia económica: La mínima cantidad aceptable para el
sostenimiento del estudiante es de CAN $830,00 mensual y CAN $333,00 por cada
miembro de su familia que lo acompañe sin incluir los costos de la matrícula.
Si hay patrocinio, presentar declaración notariada de quien financiará los
gastos en Canadá expresando claramente su compromiso.<br>
&#9679; Original y copia de Declaraciones de Renta o certificados de Ingresos y
Retenciones de los dos (2) últimos años.<br>
&#9679; Original y copia de extractos bancarios de los tres (3) últimos meses,
cuentas de ahorros o <span class=SpellE>CDTs</span> que demuestren solvencia
económica suficiente para respaldar la solicitud.<br>
&#9679; Prueba de una cuenta bancaria en Canadá si el dinero será enviado al
país.<br>
&#9679; Pruebas de soporte financiero si usted posee una beca o pertenece a un
programa educacional canadiense.<br>
&#9679; Si usted es menor de 18 años. Registro Civil de nacimiento (inscripción
de nacimiento para ecuatorianos) y autorización de sus padres debidamente
autenticada mencionando el lugar de destino y tiempo de estadía en Canadá,
además una carta de sus padres cediendo la custodia a la familia en el Canadá y
una carta de la familia en el Canadá aceptando dicha custodia</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Para provincia de British Columbia la mayoría de edad es a los 19 años.<br>
&#9679; Si desea tomar un curso de idiomas no mayor a seis (6) meses se le
otorgará vías de turismo siempre y cuando cumpla con los requisitos mencionados
arriba. Sin embargo, si tienen intenciones e continuar sus estudios después de
los seis (6) meses deberá solicitar una visa de estudiantes ya que la visa de
turista no se puede cambiar por una visa de estudiante dentro de Canadá.<br>
&#9679; Si va a permanecer más de 6 meses en el Canadá debe hacerse exámenes
médicos antes de solicitar su visa; las instrucciones para dichos exámenes
deberá solicitarlas junto con su formulario y los resultados deberá anexarlos a
su documentación. Ver médicos autorizados en Colombia ingrese a
http://geo.international.gc.ca/latin-america/colombia/visa/list-medical-practitioner.es.aspx<br>
&#9679; Si va a solicitar un permiso de estudios, así no lo requiera debe
escribir una carta explicando las razones de su solicitud. Los permisos de
estudios pueden ser renovados dentro de Canadá.<br>
&#9679; Los estudiantes destinados a la provincia de Quebec necesitan la
aprobación del gobierno de Quebec.<br>
Favor verificar con la institución educativa en Quebec sobre como solicitar el
“<span class=SpellE>Certificat</span> D” <span class=SpellE>Aceptation</span>
Du Quebec (CAQ)”, la Embajada no podrá dar información sobre el proceso de su
CAQ para una mayor información sobre <span class=SpellE>como</span> obtener el
CAQ, podrá comunicarse con el ”<span class=SpellE>Ministere</span> Des <span
class=SpellE>Relations</span> <span class=SpellE>avec</span> les <span
class=SpellE>citovns</span> et de I <span class=SpellE>Inmigration</span> Du
Quebec” División Des <span class=SpellE>Etudiantes</span>, 2376 <span
class=SpellE>Rue</span> <span class=SpellE>Sain</span>-Jacques 4eme <span
class=SpellE>étage</span> , Montreal, (Quebec), H2 y IN3 Canadá número de
teléfono (514) 8642544 Fax: (514) 8739931 correo: oriana.leon@mra.gouv.ca o
ingrese a la página www.inmigration-quebec.gouv.gc.ca/es/index.jtml<br>
&#9679; En hoja aparte suministre una descripción clara y detallada las razones
que tiene para estudiar en el Canadá. Debe incluir informe del itinerario de
viaje, los nombres y las direcciones de las personas e instituciones que usted
visitará. Si usted viaja acompañado, indique nombre y direcciones actualizadas
de las personas que lo acompañarán.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Ingreso de menores a Canadá:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para ingreso de menores de cualquier
nacionalidad se debe tener en cuenta:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>-Viajando con los padres, portar
documentos que demuestran parentesco.<br>
-Si viaja con uno de los padres y están separados, deben portar documentos que
acrediten la custodia del menor.<br>
-Si viajan solos o con adultos sin custodia legal, deben portar autorización de
los padres o de quien tenga la custodia, informando el destino, duración de la
estadía y teléfono en Canadá.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VALOR: Los costos son variables por lo
tanto se deben confirmar con la agencia al <span class=SpellE>inciar</span> el
trámite y NO SON REEMBOLSABLES.<br>
Debe consignarse en efectivo en el Banco de Occidente, cuenta No.291005932 a
nombre de Embajada de Canadá.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La solicitud y recibo de pago se admitirán
solo durante los 30 días siguientes a la consignación.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para presentación Personal<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para visas de residencia temporal
(turista, negocios, estudiantes y trabajadores temporales), la Embajada realiza
el procedimiento por correo certificado.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Quienes deseen tramitar la visa
personalmente pueden dirigirse a las instalaciones de <span class=SpellE>Servientrega</span>
Torre Samsung, <span class=SpellE>Cra</span> 7 113-43 Local 5 y comprar una
guía de retorno <span class=SpellE>prepagado</span> o guía de compañía de
correos de preferencia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Procedimiento:</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
En la urna ubicada en el primer piso de la Embajada, depositar en sobre sellado
los siguientes documentos:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>- Guía de retorno <span class=SpellE>prepagada</span><br>
- Formulario debidamente diligenciado<br>
- Formulario de información de familia<br>
- Consignación bancaria<br>
- Pasaportes vigentes y anteriores<br>
- Requisitos acordes con la visa requerida<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Nota: La presentación de estos documentos
no garantiza la obtención de la visa pues la <span class=SpellE>posteriormenta</span>
a la revisión de los documentos, la Embajada puede exigir entrevista personal.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Las solicitudes de visa de residencia
temporal (VRT) deberán ser diligenciados en línea antes de ser impresas. Tenga
en cuenta que el diligenciamiento en línea y con éxito generara una página de
códigos de barras. Favor asegurarse que la página de códigos de barras sea
impresa en una impresora laser de alta calidad y adjúntela a su solicitud de
visa. La falta de esta página con los códigos de barras retrasara el
procesamiento de su solicitud. Al momento de diligenciar el formulario no utilice
letras mayúsculas.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TENGA EN CUENTA:<br>
- No se devuelven pasaportes antes del tiempo límite. Antes de depositar su
solicitud verifique los tiempos de procesamiento.<br>
- Todos los documentos originales y copias deben ser claros y legibles.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
- No envíe ningún documento por fax o por correo electrónico.<br>
- No comprar tiquetes <span class=SpellE>aereos</span> antes de recibir la
respuesta.<br>
- No hay consultas telefónicas.<br>
- No hay entrevistas personales a menos que el oficial decida llamarlo a dicha
entrevista.<br>
- Utilice el número de la guía de correo para rastrear su envío en internet (<span
class=SpellE>sito</span> web de la compañía de correo).<br>
- La entrega de los documentos adicionales solicitados por la selección de <span
class=SpellE>imigración</span> se deben depositar en la recepción de la
embajada en sobre sellado e identificado con nombre del solicitante, fecha de
nacimiento y número de archivo. Lunes a jueves de 8:00am a 8:30am.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir del 20 de noviembre debe ir
diligenciado en INGLES o FRANCES y se debe bajar de la página:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>http://www.cic.g.ca/<br>
http://www.bogota.gc.ca<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimiento sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Si los hijos residentes en Canadá
    invitan a sus padres, deben adjuntar documentos que demuestren solvencia
    económica.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Todos los documentos deben de ser
    presentados en original y copia.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Es indispensable que el formulario de <span
    class=SpellE>Canada</span> se diligencie con todos los datos requeridos,
    las palabras completas y sin abreviaturas. En los espacios que no aplique
    la información, colocar las letras N/A para así no dejar espacios en
    blancos. Los formularios que no lleguen completos y bien diligenciados se
    devolverán lo cual retrasará el trámite del cliente.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La duración promedio de los trámites
    en temporada alta es de 30 días hábiles, aunque en ocasiones pueda tardar
    menos tiempo; en temporada baja pueden tardar entre 10 y 20 días hábiles.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>