<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El trámite de la visa es personal.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO:<br>
Los colombianos que ingresen a Venezuela por avión no requieren visa y deben
presentar los siguientes documentos:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Pasaporte válido mínimo seis (6)
meses<br>
&#9679; Tiquete aéreo de ida y vuelta<br>
&#9679; Reserva de hotel u original de la carta de invitación notariada,
enviada desde Venezuela y que contenga dirección y teléfono del anfitrión.<br>
&#9679; Si viaja con Avianca la reservación del hotel debe estar confirmada y
presentarse en papel <span class=SpellE>membreteado</span> del hotel.<br>
&#9679; Certificado Internacional de vacuna contra la fiebre amarilla, rubéola
y sarampión<br>
&#9679; Plan de cobertura de gastos médicos para viajeros internacionales y
contra perdida, hurto o robo de equipaje. Los gastos médicos no deberá ser
menor a US $ 40.000 mientras que en caso de pérdida o robo de equipaje, el
monto máximo será equivalente a US $ 1.000.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO – Ingreso terrestre:<br>
&#9679; Formulario debidamente diligenciado<br>
&#9679; Pasaporte con vigencia mínima de seis (6) meses<br>
&#9679; Referencia bancaria original dirigida al consulado<br>
&#9679; Fotocopias de los tres (3) últimos extractos bancarios, originales o
copias autenticadas, con corte de cuenta hasta la fecha<br>
&#9679; Carta laboral indicando cargo, sueldo, tiempo de servicio y motivo del
viaje<br>
&#9679; Carta al consulado indicando forma de ingreso al país y tiempo de
estadía<br>
&#9679; Dos (2) fotos a color de 3 x 4 <span class=SpellE>cms</span><br>
&#9679; Para menores Registro Civil nacimiento, permiso de salida firmado por
ambos padres y certificado de estudios<br>
&#9679; Para independientes, certificado de ingresos del contador con copia de
la tarjeta profesional notarial<br>
&#9679; Carta de responsabilidad de gastos si hay patrocinio<br>
&#9679; Copia de la resolución de pensión si aplica<br>
&#9679; Certificado de estudios<br>
&#9679; Plan de cobertura de gastos médicos para viajeros internacionales y
contra perdida, hurto o robo de equipaje. Los gastos médicos no deberá ser
menor a US $ 40.000 mientras que en caso de pérdida o robo de equipaje, el
monto máximo será equivalente a US $ 1.000.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NEGOCIOS:<br>
&#9679; Formulario de solicitud debidamente diligenciado<br>
&#9679; Dos (2) fotos de 3 x 4 <span class=SpellE>cms</span><br>
&#9679; Pasaporte con vigencia mínima de seis (6) meses<br>
&#9679; Carta suscrita por el interesado o empresa señalando motivo del viaje y
responsabilidad en gastos<br>
&#9679; Copia del tiquete o reserva aérea<br>
&#9679; Original del Registro Mercantil de Cámara de Comercio<br>
&#9679; Carta Laboral indicando cargo, sueldo y tiempo de servicio<br>
&#9679; Últimos tres (3) extractos bancarios personales y certificación
bancaria<br>
&#9679; Últimos dos (2) extractos bancarios de la empresa que lo envía a
Venezuela, en original</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
&#9679; Plan de cobertura de gastos médicos para viajeros internacionales y
contra perdida, hurto o robo de equipaje. Los gastos médicos no deberá ser menor
a US $ 40.000 mientras que en caso de pérdida o robo de equipaje, el monto
máximo será equivalente a US $ 1.000.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>VISA EMPRESARIAL:<br>
&#9679; Pasaporte con vigencia mínima de un (1) año<br>
&#9679; Carta original al embajador en papel membrete de la empresa, indicando
cargo del viajero<br>
&#9679; Certificado de la asociación empresarial a la cual este vinculada la
empres (ANDI- <span class=SpellE>Fenalco</span>, etc..)<br>
&#9679; Registro de la Cámara de Comercio donde aparezca inscrito el nombre del
solicitante<br>
&#9679; Formulario debidamente diligenciado<br>
Para extranjeros es necesario adjuntar fotocopia de la visa de trabajo o
residencia colombiana y de la cedula de extranjería.<br>
&#9679; Plan de cobertura de gastos médicos para viajeros internacionales y
contra perdida, hurto o robo de equipaje. Los gastos médicos no deberá ser menor
a US $ 40.000 mientras que en caso de pérdida o robo de equipaje, el monto
máximo será equivalente a US $ 1.000.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La VISA EMPRESARIAL sólo se otorga a
ciudadanos colombianos, presientes, vicepresidentes y/o funcionarios de alto
rango. Este trámite es personal.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES:<br>
Valor visa de turismo USD 30<br>
Valor visa de negocios USD 60<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: Hasta 5 semanas aproximadamente.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Todos los ciudadanos que deseen ingresar
en calidad de turistas a la República Bolivariana de Venezuela a partir del 31
de enero del 2014 deben de adquirir con carácter obligatorio un plan de
cobertura de gastos médicos para viajeros internacionales y contra perdida,
hurto o robo de equipaje.<br>
(Gacetas oficiales de la República Bolivariana de Venezuela Nos. 40.308. y
40.317 del 04 y 17 de diciembre de 2013, respectivamente).<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimiento sin previo aviso<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Para viajar a Venezuela desde
    Colombia se debe portar Certificado Internacional de Vacuna contra la
    fiebre amarilla aplicada 10 días antes del viaje, sarampión y rubéola.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Las visas se deben tramitar en
    la ciudad de residencia o trabajo.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; OBSERVACIÓN IMPORTANTE:<br>
    Todos los viajeros que se trasladen a Venezuela con motivo de reuniones,
    congresos, entrenamientos, charlas, etc.., deben tener visa de trabajo, de
    lo contrario NO SERAN ADMITIDOS.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>CONSULADOS EN COLOMBIA:<br>
    Bogotá<br>
    Dirección: Avenida 13 No 103-16<br>
    Horario de atención: Lunes a viernes 08:00 a 12:00 m para recepción de
    documentos<br>
    3:00 a 4:00 p.m. para entrega de documentos<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Barranquilla<br>
    Dirección: Carrera 52 No 69-96 Piso 3 Edificio <span class=SpellE>Bancafé</span><br>
    Horario de atención Lunes a miércoles a partir de 10:00 a.m. Requiere cita
    previa.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Medellín<br>
    Dirección: Calle 32B No 69-59 barrio Belén<br>
    Horario de atención: Lunes a viernes de 08:00 a 11:00 a.m.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Bucaramanga<br>
    Dirección: Carrera 38 No 52-10<br>
    Horario de atención: Lunes a viernes de 08:00 a 11:00 a.m.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Cúcuta<br>
    Dirección: Avenida Aeropuerto No 17N-73 Zona Industrial<br>
    Horario de atención: Lunes a viernes de 08:00 a 11:00 a.m.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>