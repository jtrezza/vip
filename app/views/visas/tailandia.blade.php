<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Calle 100 No. 17 - 66 Oficina
102.<br>
Teléfono: 257 6004<br>
Fax: 257 7541.<br>
Correo <span class=SpellE>electronico</span>: consulado100@etb.net.co<br>
Horario de atención al público: Para radicar documentación lunes, martes y
miércoles de 8 a.m. a 12:30 m y entrega de visados martes, jueves y viernes de
8:30 a.m. a 12:30 m.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La visa tiene una vigencia de 3 meses para
ingresar al país a partir de la fecha de expedición. Puede ser otorgada por una
o dos entradas según sea demostrado por el itinerario de viaje.<br>
<br>
TURISMO:<br>
&#9679; Pasaporte vigente mínimo seis (6) meses y copia de los datos
biográficos del pasaporte<br>
&#9679; Dos (2) solicitudes de visa tamaño carta debidamente diligenciados con
firma y fecha<br>
&#9679; Dos (2) fotografías de 3 x 4 <span class=SpellE>cms</span> a color en
fondo blanco o azul indicando <span class=SpellE>raza:blanca</span>-mestiza-<span
class=SpellE>afrodescendiente</span><br>
&#9679; Carta laboral que exprese cargo, sueldo y tiempo de servicio<br>
&#9679; Tres (3) últimos extractos de cada cuenta bancaria del responsable de
los gastos (no se reciben extractos de tarjeta de crédito)<br>
&#9679; Vacuna internacional contra la fiebre amarilla (original y copia donde
figuran fecha y datos personales<br>
&#9679; Fotocopia del tiquete aéreo de ida y regreso o <span class=SpellE>printer</span>
de la reserva<br>
&#9679; Itinerario de viaje detallado<br>
&#9679; Fotocopia de la visa americana, si la tiene<br>
&#9679; Carta de responsabilidad de gastos (de la empresa con el empleado o a
título personal con la familia) esta última con sello de presentación personal
ante notario<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Cuando existe responsabilidad de gastos de
familiares:<br>
&#9679; Documento que certifique afinidad familiar, anexar Registro Civil de
nacimiento o de matrimonio<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para menores de edad:<br>
&#9679; Folio del Registro Civil<br>
&#9679; Certificado de estudios<br>
&#9679; Permiso de los padres que no viajen con el menor, o de quien ostente la
patria potestad, dirigida al consulado<br>
&#9679; Permiso de salida autenticado por ambos padres, dirigido al
Departamento Administrativo de Seguridad DAS<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para independientes:<br>
&#9679; Registro de la Cámara de Comercio o RUT<br>
&#9679; Carta de ingresos mensuales expedida por contador en papelería <span
class=SpellE>membreteada</span><br>
&#9679;Copia legible de la Tarjeta Profesional del Contado<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para pensionado:<br>
&#9679; Copia de la resolución de pensión de de los tres (3) últimos comprobantes
de pago<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>NEGOCIOS:<br>
&#9679; Dos (2) solicitudes de visa tamaño oficio debidamente diligenciados con
firma y fecha<br>
&#9679; Vacuna internacional contra la fiebre amarilla (original y copia datos
personales y fecha de vacuna)<br>
&#9679; Pasaporte vigente mínimo seis (6) meses y copia de los datos
biográficos del pasaporte<br>
&#9679; Dos (2) fotografías de 3 x 4 <span class=SpellE>cms</span> a color en
fondo blanco o azul indicando <span class=SpellE>raza:blanca</span>-mestiza-<span
class=SpellE>afrodescendiente</span>, etc..<br>
&#9679; Fotocopia de la visa americana, si la hubiese<br>
&#9679; Carta detallando relaciones comerciales<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Para independientes se debe adjuntar:<br>
&#9679; Registro de la Cámara de Comercio o RUT<br>
&#9679; Carta de ingresos mensuales expedida por contador en papel membrete<br>
&#9679; Copia legible de la Tarjeta Profesional del Contado<br>
&#9679; Carta laboral indicando cargo, sueldo y tiempo de servicio<br>
&#9679; Carta de responsabilidad de gastos (de la empresa o familiar), esta
última con reconocimiento ante notario<br>
&#9679; Copia de los tres (3) últimos extractos de cada cuenta bancaria de
quien asume los gastos (no se reciben extractos de tarjeta de crédito)<br>
&#9679; Fotocopia del tiquete aéreo de ida y regreso o <span class=SpellE>printer</span>
de la reserva<br>
&#9679; Itinerario detallado<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES:</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Valor visa de turismo una entrada USD 40<br>
Valor visa de turismo dos entradas USD 80<br>
Valor visa de turismo tres entradas USD 120<br>
Valor visa de no inmigrante una entrada/3 meses USD 80<br>
Valor visa de no inmigrante entradas <span class=SpellE>multiples</span>/1 año
USD 200<br>
Valor visa de tránsito USD 35<br>
Valor pago extra USD 20<br>
El valor de la visa se debe cancelar en <span class=SpellE>Dolares</span>.<br>
<br>
DURACION DEL TRÁMITE: 4 días hábiles aproximadamente.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aplicaciones de visa si el
itinerario de viaje implica varias de ellas, el número de hojas libres puede
cambiar.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:18.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:silver;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimiento sin previo aviso.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>El consulado no devuelve
    documentación.</span><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
    mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>