<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Consulado de Francia en Bogotá:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Carrera 11 No. 93-12<br>
Teléfonos: 638 1490 - 638 1400 - 618 1444.<br>
Fax: 638 1491<br>
Horario de atención al público: Lunes a viernes de 8:00 a.m. a 12:00 p.m.<br>
Horario de atención agencias de viajes: Lunes a jueves 8:00 a.m.<br>
Para reclamar visa de turismo: Lunes a jueves 2:00 p.m.<br>
Página web: www.diplomatie.gouv.fr<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Requiere cita previa que se debe solicitar
en http://www.ambafrance-co.org/solicitar-una-cita<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES $ 166.650<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>REQUISITOS VISA TURISMO / NEGOCIOS:<br>
- Pasaporte vigente superior a nueve (9) meses y pasaportes anteriores<br>
- Formulario de solicitud de visa debidamente diligenciado y firmado. Tinta
negra con letra despegada.<br>
- Sin excepción, la firma del solicitante debe estar en la casilla 37 tanto
como al final del formulario.<br>
- Dos (2) fotos recientes de tamaño 3.5 x 4.5 <span class=SpellE>cms</span>, en
fondo blanco con el rostro descubierto, y una de ellas pegada a la solicitud<br>
- Fotocopia de la cédula<br>
- Copia de todas las visas anteriores y de los datos biográficos del pasaporte<br>
- Seguro médicos válido para Europa, con cobertura mínima de 30.000 Euros o USD
40.000 con cobertura de accidente, enfermedad y/o repatriación<br>
- Copia del <span class=SpellE>printer</span> o del tiquete aéreo<br>
- Copia de la reserva hotelera confirmada de todo el tiempo de viaje.<br>
- Certificación laboral indicando cargo, sueldo, tiempo de servicio y motivo
del viaje<br>
- Si es independiente, original del Registro Mercantil de Cámara de Comercio y
del certificado de contador público con fotocopias autenticadas de cédula y
tarjeta profesional. Este último debe mencionar dirección, teléfono e ingresos
mensuales del solicitante, anexando copia de la Declaración de Renta y pago de
impuestos de los últimos tres (3) años<br>
- Para viajes de negocios carta de invitación de la empresa en Francia con la
cual tiene contactos comerciales indicando teléfono y dirección<br>
Resolución de pensión y tres (3) últimos desprendibles de pago, si aplica<br>
- Copia de los últimos extractos bancarios, tres (3) últimos meses en cuentas
de ahorros y seis (6) últimos meses para cuentas corrientes. No se aceptan
extractos de Internet, deberá exigir sello y firma de la entidad bancaria para
que sean aceptados. Si el trámite se presenta después del día 10 del mes se
debe presentar el movimiento de la cuenta con saldo a la fecha.<br>
- Copia de la Declaración de Renta autenticada.<br>
- Carta de responsabilidad de gastos autenticada si el viaje es patrocinado. Si
el patrocinio del viaje es otorgado por familiares diferentes a los padres, se
debe demostrar el parentesco entre quien patrocina y la persona que viaja. En
este caso se presentan folio de los Registros civiles de los familiares
autenticados.<br>
- Para menores, folio del Registro Civil de matrimonio de los padres, folio del
Registro Civil de nacimiento, permiso de salida del país firmado por ambos
padres, los documentos anteriores deben ser autenticados no mayor a 30 días.<br>
- Si la persona es estudiante, debe anexar la certificación de estudios<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: Una (1) semana después de la
entrevista<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La autorización de visas de cinco (5),
tres (3) ó un (1) año quedan a criterio del consulado y requieren visas previas
al territorio <span class=SpellE>Schengen</span><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TRANSITO:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir del 1 de Junio de 2012 permiten
el ingreso sin visa a Colombianos que estén en tránsito aeroportuario, siempre
y cuando el viajero proceda o continúe hacia un país del cual ya tiene visa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>ESTUDIANTE:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El estudiante debe inscribirse en el link
www.colombia.campusfrance.org y presentar la documentación en copias, en <span
class=SpellE>campusfrance</span> en la Alianza Colombo Francesa, sede centro o <span
class=SpellE>envairse</span> por correo<br>
Dirección : Carrera 3 No. 18-45 -Bogotá<br>
Horario : Lunes a viernes de 9:00 a 12:30 m<br>
En otras ciudades, consultar dirección<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>LARGA ESTADIA -seis (6) meses a un año
para mayores de edad<br>
- Dos (2) formularios de solicitud de visados de larga estadía debidamente
diligenciados<br>
- Tres (3) fotografías recientes, dos pegadas en los formularios y una suelta,
en fondo blanco, de 3.5 x 4.5 <span class=SpellE>cms</span>, y cara de 2 <span
class=SpellE>cms</span> x 2.5 <span class=SpellE>cms.</span>)<br>
- Pasaporte con validez superior a tres (3) meses a la del visado solicitado, y
copia de las dos (2) primeras páginas dobles del pasaporte<br>
- Carta de presentación en francés y en español con identificación del
pasajero, motivos, estudios que pretende<br>
- Hoja de vida, máximo dos páginas<br>
- Pruebas de la condición del estudiante tales como copia apostillada del
último diploma, certificado de la universidad o colegio, constancia de
escolaridad, bachillerato o título equivalente o último <span class=SpellE>carnét</span>
de estudiante<br>
- Certificación laboral, si aplica<br>
- Certificado de estudios de francés en la Alianza Francesa o test de
conocimiento No. 2 de la Alianza Francesa si ha estudiado en institución o
profesor particular. Para estudios de francés en Francia se requiere un mínimo
de 150 horas de estudio previo en Colombia<br>
- Constancia de inscripción en un establecimiento en Francia, carta de
aceptación, certificado de pre-inscripción de un establecimiento público o
privado de enseñanza superior o de formación profesional superior inicial o
continua<br>
- Pruebas de solvencia económica<br>
- Certificación de cuenta bancaria con ocho (8) millones de pesos para
solicitar la visa de seis (6) meses o de 16 millones para solicitar la visa de
un (1) año.<br>
- Constancia manuscrita de una persona en Francia que se compromete a asumir
gastos, con copia de cédula o pasaporte ó titulo de residencia en Francia<br>
- Prueba de domicilio por ejemplo cuenta de servicios, de arriendo o escrituras
notariales si es propietario<br>
- Tres (3) últimos certificados de salario y último dictamen de imposición
sobre la renta (avis d´ <span class=SpellE>imposition</span> sur le <span
class=SpellE>revenu</span>)<br>
- Los tres (3) últimos extractos bancarios<br>
- Certificado de alojamiento en Francia, contrato de arrendamiento o reserva
confirmada en una residencia estudiantil o apartamento particular<br>
- Constancia manuscrita de una persona en Francia que se comprometa a alojarlo
y copia de la cédula o pasaporte francés del anfitrión, prueba de residencia,
constancia de domicilio y cuentas de servicios, pago de arriendo, o escrituras
notariales si es propietario<br>
- Para pasantías, convenio firmado por la universidad colombiana, la empresa en
Francia y el estudiante (convenio de <span class=SpellE>Stage</span> <span
class=SpellE>Type</span> en la página (http://www.ciam.org.co/) y duración del
programa<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>En caso de que la inscripción esté
subordinada a la aprobación de un examen o cualquier otra prueba, se aplicará
el procedimiento de visa-concurso<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>ESTUDIOS CORTA DURACION<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Los mismos documentos para visa de turismo
y adicional:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>- Pruebas de la formación obtenida:
constancia de preinscripción en un centro privado o público de enseñanza, o en
un establecimiento de formación profesional continua, constancia de beca,
justificación del beneficio de un programa de formación en la Unión Europea,
carta tipo de la Dirección Departamental de Trabajo, Empleo y Formación
(DDTEFP) en caso de prácticas de información o convenio de prácticas entre la
empresa y el establecimiento educativo<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Es indispensable que el estudiante viaje a
Francia con todos los documentos originales presentados para la visa ya que
estos pueden ser solicitados por la aduana francesa, razón por la cual no se
deben entregar los documentos originales al consulado<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACIÓN<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Hasta tres (3) meses de duración se debe
solicitar visa <span class=SpellE>Schengen</span>.<br>
De 4 a 6 meses de duración no se puede renovar en Francia<br>
Para estadías a partir de los siete (7) meses se tramita visa de un (1)año la
cual se expide por tres (3) meses, plazo máximo para tramitar la &quot;<span
class=SpellE>carte</span> de <span class=SpellE>sejour</span>&quot; (cédula de
extranjería) al llegar a Francia<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir del 5 de septiembre de 2013 los
trámites para Visa <span class=SpellE>Schengen</span> deben realizarse
personalmente en las embajadas de las naciones miembros con sede en Bogotá.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Cada persona debe dirigirse a la embajada para solicitar la visa y registrar
sus huellas dactilares y una foto digital.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Este registro tendrá vigencia de cinco años y la información estará disponible
cada vez que se solicite una visa en los consulados de países que hacen parte
de la zona <span class=SpellE>Schengen</span>.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Después de hacer el registro y durante los 5 años de vigencia, se podrá
tramitar el documento por medio de una agencia de viajes.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La agencia puede intermediar
    solicitudes de visa a Francia para aquellas personas que tengan o hayan
    tenido visa al Reino Unido, <span class=SpellE>Canada</span>, Suiza,
    Comunidad Europea o dispongan de visa americana vigente<br>
    La agencia podrá tramitar visas a menores sin visa USA o Europea cuando
    ambos padres tengan visas vigentes para el territorio <span class=SpellE>Schengen</span>
    o EEUU.<br>
    Las visas para menores de edad las autorizan máximo para 90 días de estadía
    y seis (6) meses para utilizarlas. Los menores de 6 años no cancelan
    derechos consulares.<br>
    La autorización de visas de cinco (5), tres (3) ó un (1) año quedan a
    criterio del consulado y requieren visas previas al territorio <span
    class=SpellE>Schengen</span>.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Es necesario tomar cita a través de la
    web http://www.ambafrance-co.org/spip.php?article1095. Para particulares la
    respuesta se recibe el mismo día.<br>
    Los pasajeros en tránsito por París con destino a Ginebra o Basilea
    necesitan visa <span class=SpellE>Schengen</span> de corta estadía y visa
    de Suiza.<br>
    La vigencia de la visa de tránsito es de 3 meses a partir de la fecha de
    expedición<br>
    Los ciudadanos colombianos necesitan visa de tránsito para circular por
    cualquier aeropuerto <span class=SpellE>frances</span> excepto los
    titulares de visas válidas, titulo de estadía o residencia para los países
    del espacio <span class=SpellE>Schengen</span>, Canadá, Estados Unidos o
    Suiza.<br>
    Las personas que no tengan visa utilizada a EEUU, Suiza, Canadá, Reino
    Unido o la Comunidad Europea, deben realizar la gestión de manera personal
    pues las agencias no están autorizadas a gestionarlas.<br>
    El trámite de esta visa tiene una duración de 5 días hábiles<br>
    Los <span class=SpellE>conyuges</span> ascendientes y descendientes de
    franceses y de <span class=SpellE>mimembros</span> de la <span
    class=SpellE>Union</span> Europea, deben solicitar cita en
    https://pastel.diplomatie.gouv.fr<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>IMPORTANTE:<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Para los siguientes territorios
    franceses la embajada exige los mismos documentos para la visa de turismo y
    la <span class=SpellE>duracion</span> del trámite es de diez (10) días
    hábiles:<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span class=SpellE><span style='font-size:10.0pt;
    font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:
    ES'>Polynesia</span></span><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'> Francesa: <span class=SpellE>Moorea</span>
    y <span class=SpellE>Tahiti</span>, Bora <span class=SpellE>Bora</span>, <span
    class=SpellE>Huahine</span>, <span class=SpellE>Maupiti</span>, <span
    class=SpellE>Raiatea</span>, <span class=SpellE>Anaa</span>, <span
    class=SpellE>Hao</span>, <span class=SpellE>Manihi</span>, <span
    class=SpellE>Rengirao</span>, <span class=SpellE>Rikitea</span>, <span
    class=SpellE>hiva</span> <span class=SpellE>Oa</span>, <span class=SpellE>Nuku</span>
    <span class=SpellE>Hiva</span>, <span class=SpellE>Ua</span> <span
    class=SpellE>Huka</span>, <span class=SpellE>Ua</span> <span class=SpellE>Pou</span>,
    <span class=SpellE>Rurutu</span>, <span class=SpellE>Tubuai</span><br>
    Departamentos de ultramar: Guadalupe, Guyana, <span class=SpellE>Martinique</span>,
    <span class=SpellE>Reunion</span>, La <span class=SpellE>Desirade</span>, <span
    class=SpellE>St</span> <span class=SpellE>Barthelemy</span>, <span
    class=SpellE>St</span> Martín, Nueva Caledonia.<br>
    Senegal- Este trámite tiene una duración aproximada de 17 días.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La embajada de Francia en Colombia
    informa que debido a la disminución del número de solicitudes observadas
    desde principio de año, el servicio de visas se vio obligado a cambiar su
    organización con respecto a las citas otorgadas al público.<br>
    Por lo tanto a partir del 3 de julio de 2013 el servicio de visas recibirá
    a los clientes de ANATO los miércoles a partir de las 3pm.<br>
    Agradecemos enviar sus solicitudes de cita con máximo 8 días de
    anticipación a: documentacinmde@anato.org A vuelta de correo recibirá la
    correspondiente información de su cita e indicaciones para radicar sus <span
    class=SpellE>tramites</span>.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>