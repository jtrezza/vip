
<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Calle 22 D Bis No. 47-51
Bogotá, Colombia.<br>
Teléfono General: 275 2000<br>
Teléfono Sección Consular: 275 4900.<br>
Fax General: 275 4600<br>
Fax Sección Consular: 275 4655.<br>
<span class=SpellE>Paginá</span> web: http://spanish.bogota.usembassy.gov<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Nuevo centro de recepción de visas en
Medellín está ubicado en el centro comercial Punto de la Oriental, <span
class=SpellE>Cra</span> 46 #47-66, centro de la ciudad.<br>
<br>
La Embajada de Estados Unidos implemento un nuevo sistema de citas en línea diseñado
para que el proceso de solicitud de visa sea más rápido, simple y económico.
Este cambio representa otra gran alianza con Colombia y facilita el proceso de
solicitud de visa para los colombianos.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Los solicitantes pueden programar sus propias citas en línea a través de la
nueva página web http://colombia.usvisa-info.com o comunicarse con una
operadora en el nuevo centro de llamadas (5713259851). Los solicitantes de visa
pueden economizar el costo de la misma, ya que el nuevo sistema elimina la necesidad
de adquirir el Número de Identificación Personal (PIN) para programar su
entrevista en el consulado y el pasaporte es entregado en forma gratuita en los
puntos designados para su recolección.<br>
<br>
Además, el sistema que entro a regir está disponible en inglés y en español, y
permite que los usuarios de la página puedan:<br>
<br>
• Elegir la fecha y hora que más les convenga para su cita de no inmigrante, a
través de un calendario fácil de lectura.<br>
• Reprogramar o cancelar sus citas para visa de no inmigrante.<br>
• Coordinar la entrega gratuita de su pasaporte y visa en cualquiera de las 20
oficinas de DHL autorizadas para tal efecto, ubicadas en nueve ciudades del
país (en algunas ciudades los pasaportes podrán ser entregados a domicilio por
una tarifa a cargo del solicitante)<br>
• Pagar la solicitud de visa de no inmigrante con tarjeta de crédito o débito a
través de la página (la opción de hacer el pago en efectivo seguirá disponible
para quienes lo prefieran)<br>
<br>
Gracias a que el procedimiento para la entrega del documento se hace con
anticipación, el tiempo de permanencia en la Embajada el día de la entrevista
es menor.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>REQUISITOS PARA SOLICITAR VISAS B1/B2:<br>
<br>
• El primer paso para solicitar la visa es diligenciar el formulario DS-160 en
el link https://ceac.state.gov/genniv; el número de código de barra tiene que
estar disponible para cada solicitud a fin de programar una cita a través de la
página web asignada por la <span class=SpellE>embajda</span>.<br>
• Debe realizar el pago del cargo de solicitud por cada solicitante y programar
la cita en la página web http://colombia.usvisa-info.com.<br>
• Asista a la cita programada para presentar los documentos de solicitud.<br>
• Si se aprueba la visa, el pasaporte, la visa y cualquier documento
correspondiente de los solicitantes se enviarán por mensajería en función de la
selección de la ubicación de las solicitudes. <span class=SpellE>VerInformación</span>
sobre el servicio de mensajería.<br>
• Si la visa fue negada: Se informará al solicitante al final de la entrevista
y recibirá su pasaporte y documentación de inmediato conjuntamente con una
carta de explicación del rechazo.<br>
• Si la visa se aprueba: El pasaporte con la visa aprobada estará disponible
para ser recogida en la ubicación del servicio de mensajería de su elección 7 a
10 días hábiles (3 semanas para los niños menores de edad) después de la
entrevista.<br>
• Cuando reciba su pasaporte revise la visa emitida con detenimiento. La
Sección Consular reemplazará la visa sin cargo si no coincide con la
información biográfica que presentó. En este caso, retorne con su pasaporte a
la ventanilla VIP de lunes a viernes de 1:00 a 2:00 p.m. De lo contrario tendrá
que presentar una nueva solicitud por cambios a realizar en su información
biográfica.<br>
<br>
Pagos por realizar:<br>
<br>
• Derechos Consulares USD 160 (equivalentes a $299.200). <span class=SpellE>Veropciones</span>
de pago de cargo<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Notas:<br>
<br>
• Para ingresar a EE.UU el pasaporte debe tener una vigencia mínima de 6 meses.<br>
• Únicamente el padre o la madre pueden solicitar la visa para sus hijos.<br>
<br>
<br>
Documentos a presentar:<br>
<br>
• Pasaporte actual con vigencia mínima de un año. Fotocopia de la portada
interior del pasaporte (donde aparece el número del pasaporte) y fotocopia de
la página biográfica del pasaporte.<br>
• Pasaportes anteriores. En caso de pérdida del pasaporte debe haber informado
a la Embajada y tramitar ante el Departamento Administrativo de Seguridad DAS,
un Certificado de Movimientos Migratorios.<br>
• Cédula de ciudadanía para mayores de edad y registro de nacimiento para
menores de edad.<br>
• Una fotografía de 5X5 cm, vigente, a color y fondo blanco, grapada al
formulario más no pegada, sin aretes, collares y lentes. De ninguna manera se
aceptan fotos de blanco y negro.<br>
• <span class=SpellE>Confirmacion</span> del formulario DS-160.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
• Los documentos de soporte, tales como certificados laborales, escrituras,
extractos y certificados de impuestos son presentados a menudo por el
solicitante para demostrar sus vínculos con Colombia; sin embargo, la
información proporcionada directamente por el solicitante en el momento de la
entrevista es de igual o mayor importancia.<br>
• Los documentos de Estados Unidos, como cartas de invitación, prueba de
contactos comerciales, etc., pueden ser útiles para clarificar las razones de
viaje, pero no son tan importantes como aquellos que indiquen la situación
económica, social, familiar y profesional del solicitante.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Nota: Para los jóvenes de 14,15,16 o 17 años de edad, es requisito presentar
una entrevista personal con todos los documentos necesarios para solicitar la
visa, y a la entrevista pueden venir en compañía de sus padres. A partir de los
18 años de edad debe presentar una entrevista personal.<br>
<br>
Los siguientes son los documentos sugeridos que puede presentar para solicitar
su visa. Sin embargo, se debe tener en cuenta que la presentación de estos
documentos no garantiza la aprobación de la visa. Si es posible, presentar
originales los cuales serán devueltos al solicitante.<br>
<br>
• Verificación de empleo:<br>
Carta del empleador en donde se especifique: Salario mensual, cargo, tiempo de
servicio y días otorgados para su viaje.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Desprendibles recientes de pago.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Tarjeta del seguro social (ISS o EPS).<br>
Tarjeta de caja de compensación familiar.<br>
• Solvencia económica:</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Certificado de Cámara de Comercio.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Formulario de registro mercantil (si se requiere).</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Certificación de ingresos y/o retención en la fuente de los últimos tres años.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Extractos bancarios de los últimos seis meses.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Escrituras de las propiedades que posee.<br>
• Vínculos familiares/sociales:</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Copia del Certificado de matrimonio autenticado (expedido en los últimos tres
meses).</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Para menores de edad, copia del registro de nacimiento autenticado (expedido en
los últimos tres meses).</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Si depende de los padres, presentar documentos de ellos.<br>
Tarjeta profesional o carné universitario.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
VISAS DE TRANSITO C-1<br>
<br>
• A todos los colombianos con intención de estar en tránsito en un aeropuerto
de Estados Unidos antes de viajar a un tercer país, se le exige presentar una
visa estadounidense válida al abordar el vuelo con destino hacia Estados
Unidos. Quienes soliciten la Visa de tránsito (C-1) deben solicitar cita para
la entrevista en la Sección Consular en el Servicio de Información de visas. En
el momento de la entrevista los solicitantes deben mostrar su pasaje o
itinerario, indicando el destino final, diferente de Estados Unidos o Colombia,
así como la visa para un tercer país de destino y los demás documentos que
normalmente se exigen para probar que se reúnen las condiciones para obtener
una visa. Los requisitos son los mismos de la visa de turismo B1/B2.<br>
<br>
VISA DE ESTUDIANTE<br>
<br>
Quien solicite visa de estudiante debe pedir su cita al Servicio de Información
sobre visas. El solicitante debe mostrar su Formulario I-20 en la portería y
presentar los siguientes documentos durante la entrevista:<br>
<br>
• Todos los requisitos enumerados bajo la visa de turismo (B1/B2).<br>
• Formulario I-20A (diligenciar sólo las páginas necesarias) o I-20M, recibido
de una universidad o colegio en Estados Unidos.<br>
• Confirmación del formulario DS-160 puede diligenciarse en el siguiente link
https://ceac.state.gov/genniv</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
• Información personal económica (o información de los padres, si depende de
ellos) que muestre suficiente liquidez para cubrir todos los gastos de los
estudios, o carta acerca de una beca.<br>
• Prueba de poseer un seguro médico utilizable en Estados Unidos.<br>
• Documento de identificación (cédula de ciudadanía) para los ciudadanos
colombianos mayores de 18 años, visa de residencia para extranjeros mayores de
18 años que residan en Colombia o tarjeta de identidad para ciudadanos
colombianos menores de 18 años. Si el solicitante acaba de cumplir la mayoría
de edad y no ha recibido la cédula de ciudadanía, deberá presentar el
comprobante expedido por la <span class=SpellE>Registarduría</span> junto con
la correspondiente anotación en su pasaporte.<br>
• Para los solicitantes menores de 14 años de edad, copia autenticada del
registro de nacimiento (folio del Registro Civil de Nacimiento).</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
Las personas que soliciten la visa de estudiante por primera vez deben cancelar
el valor no reembolsable correspondiente al &quot;Sistema de Información de
Visitantes de Intercambio y Estudiantes&quot; (SEVIS).<br>
<br>
El valor correspondiente al SEVIS para estudiantes con visa F1 y M1 es de USD
200, se puede cancelar a través de internet, por Western <span class=SpellE>Union</span>
o por correo. Los solicitantes de visas F2 y M2 no requieren cancelar este
valor.<br>
<br>
Si al solicitante se le ha negado la visa de estudiante, debe esperar cerca de
seis meses para programar una nueva cita.<br>
<br>
CASOS ESPECIALES<br>
<br>
La Sección Consular de la Embajada de Estados Unidos acepta los siguientes
casos sin presentar entrevista personal ni toma de huellas dactilares:<br>
<br>
VISAS MENORES DE EDAD DE 0 A 13 AÑOS<br>
<br>
• Pasaporte actual con mínimo un (1) año de vigencia y pasaportes anteriores
(no deben tener visas a Estados Unidos rechazadas anteriormente).<br>
• Confirmación del formulario DS-160 puede diligenciarse en el siguiente link
https://ceac.state.gov/genniv. Debe ser impreso en impresora laser.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
• Debe realizar el pago del cargo de solicitud por cada solicitante. Ingresar a
http://colombia.usvisa-info.com.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
El valor de la visa lo puede cancelar a través de la página asignada por la
Embajada con tarjeta crédito o débito.<br>
• Carta autenticada dirigida a DHL autorizando a un funcionario de la agencia
para reclamar pasaporte, firmada por uno de los padres con copia de la cédula
de ciudadanía.<br>
• Una fotografía de 5X5 cm, vigente, a color y fondo blanco, grapada al
formulario más no pegada, sin aretes, collares y lentes. De ninguna manera se
aceptan fotos de blanco y negro.<br>
• Copias a color de la hoja de datos biográficos del pasaporte y visa americana
vigente de los padres, sin anotaciones, expedidas en Colombia por un año ó más
y con múltiples entradas. (Las visas de los padres deben tener como mínimo 6
meses de vigencia al momento de la presentación de los documentos).<br>
• Si uno de los padres tiene pasaportes diferente al colombiano, debe tener
visa de residente Colombiana impresa en el pasaporte y traer fotocopia de la
cédula de extranjería, (la visa de Estados Unidos debió ser expedida en
Colombia).<br>
• Fotocopia de la tarjeta de identidad, cuando el solicitante presente
pasaporte de lectura mecánica.<br>
• Dos copias recientes del folio del registro civil de nacimiento, con sellos
originales expedido en Colombia y mínimo tres meses de vigencia.<br>
Los menores registrados a partir de Febrero del año 2000 deben tener el NUIP en
el registro de nacimiento.<br>
Los registros que tengan este número (NUIP) corregido deben tener sello y firma
del notario.<br>
• Debe ingresar a la página del Servicio de Visas de la Embajada de Estados
Unidos https://colombia.usvisa-info.com:<br>
Crear una cuenta y completar la información personal.<br>
Escoger la oficina de DHL donde desea que le envíen el pasaporte.<br>
Imprimir la hoja de resumen del solicitante y adjuntar a los documentos.<br>
Realizar el pago de los derechos de visa (USD 160) e imprimir la copia del comprobante
de pago.</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
• Hoja de resumen del solicitante.<br>
<br>
VISAS PARA MAYORES DE 80 (Plan Canitas)<br>
<br>
Aplica para personas que hayan tenido visa a Estados Unidos en los últimos 5
años.<br>
<br>
• Pasaporte actual con un (1) año de vigencia y pasaportes anteriores.<br>
• Confirmación del formulario DS-160 puede diligenciarse en el siguiente link
https://ceac.state.gov/genniv</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
• Debe realizar el pago del cargo de solicitud por cada solicitante. Ingresar a
http://colombia.usvisa-info.com.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
El valor de la visa lo puede cancelar a través de la página asignada por la
Embajada con tarjeta crédito o debito.<br>
• Carta autenticada dirigida a DHL autorizando a un funcionario de <span
class=SpellE>Aviatur</span> para reclamar pasaporte y copia de cédula del
solicitante.<br>
• Una fotografía de 5X5 cm, vigente, a color y fondo blanco, grapada al
formulario más no pegada, sin aretes, collares y lentes. De ninguna manera se
aceptan fotos de blanco y negro.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
• Debe ingresar a la página del Servicio de Visas de la Embajada de Estados Unidos
https://colombia.usvisa-info.com:<br>
Crear una cuenta y completar la información personal.<br>
Escoger la oficina de DHL donde desea que le envíen el pasaporte.<br>
Imprimir la hoja de resumen del solicitante y adjuntar a los documentos.<br>
Realizar el pago de los derechos de visa (USD 160) e imprimir la copia del
comprobante de pago.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
• Hoja de resumen del solicitante.<br>
• Derechos Consulares USD 160 (equivalentes a $299.200). Ver opciones de pago
de cargo</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
• Si se aprueba la visa, el pasaporte, la visa y cualquier documento
correspondiente de los solicitantes se enviarán por mensajería en función de la
selección de la ubicación de las solicitudes. Ver Información sobre el servicio
de mensajería.<br>
<br>
Para renovación, adición de hojas, solicitantes de 15 años o menos por favor
ingrese a la siguiente página:
http://spanish.bogota.usembassy.gov/pasaportes.html<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>PASAPORTE AMERICANO<br>
<br>
A partir del 1 de junio de 2011 la Embajada Americana en Bogotá solo recibirá
solicitudes de pasaporte y anexo de hojas mediante citas programadas en línea. <span
class=SpellE>Agende</span> su cita en el siguiente
link:https://evisaforms.state.gov/acs/default.asp?postcode=BGT&amp;appcode=1<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Notas:<br>
• Todos los niños/as de 15 años o menores deben presentarse en la Embajada o la
Agencia Consular en el momento de la solicitud.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
• Para solicitantes de 15 años o menores, ambos padres deben dar su
consentimiento para la expedición del pasaporte del menor. Los padres deben
presentar su documento de identidad y firmar la solicitud de pasaporte del
menor en presencia del funcionario consular.<br>
• Si uno o ninguno de los padres acompaña al menor, el padre o padres ausentes
deben presentar una carta firmada y notariada en Inglés, que manifieste
expresamente su consentimiento para la expedición del pasaporte. Esta carta
puede ser notariada por un notario americano o colombiano, o por un oficial
consular en cualquier Embajada o Consulado de Estados Unidos. Los padres pueden
utilizar el formulario DS-3053 para este propósito. El padre ausente debe
también enviar una copia de su licencia de conducción, cédula, pasaporte u otro
documento oficial de identificación.<br>
• Si usted no puede presentar un pasaporte estadounidense anterior, debe llevar
un documento de identidad con foto expedido por el Gobierno, por ejemplo, una
licencia de conducción. Como se mencionó anteriormente, un documento de
identidad sirve únicamente como prueba de identidad y no es prueba de
ciudadanía estadounidense.<br>
• Si su pasaporte actual está vigente, pero no tiene <span class=SpellE>mas</span>
espacio para visas y sellos de inmigración, puede solicitar páginas adicionales
sin costo alguno. Existe un servicio para el mismo día si presenta la solicitud
en la Embajada. Si lo solicita en la Agencia Consular, el proceso toma
aproximadamente dos o tres semanas.<br>
<br>
PÉRDIDA DE PASAPORTE CON VISA AMERICANA VIGENTE<br>
<br>
Si su pasaporte con visa americana ha sido perdido o robado, por favor
repórtelo <span class=SpellE>inmeditamente</span> a la Embajada. Los pasos que
debe tomar son:<br>
<br>
• Haga una constancia juramentada (pérdida) o un denuncio (robo) con la
Policía, en la jurisdicción donde el pasaporte fue perdido o robado.<br>
• <span class=SpellE>Envie</span> una copia del reporte de la policía al correo
electrónicoBogotaVisaPerdida@state.gov con la siguiente información:<br>
- Nombres y apellidos como aparecen en su pasaporte.<br>
- Fecha de nacimiento y número de cédula.<br>
- Si el pasaporte fue perdido o robado y cómo.<br>
- Fecha de la pérdida o robo.<br>
- Su número de teléfono y correo electrónico.<br>
- Diligencie el formato &quot;Informe de <span class=SpellE>Extravio</span>/Robo
de pasaporte con Visa&quot;</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
<br>
La Embajada no reactiva visas. Si recupera su pasaporte extraviado, con su visa
americana que reportó como pérdida ante la Embajada, no debe viajar con esta
visa. Usted deberá solicitar una nueva visa.<br>
<br>
Si solicita una nueva visa, debe traer una certificación de entradas y salidas
de Colombia hasta la fecha, en sobre sellado. Esta certificación es expedida
por el Departamento Administrativo de Seguridad (DAS).<br>
<br>
INFORMACIÓN PERDIDA FORMA I-94<br>
<br>
Con el fin de incrementar la eficiencia, reducir costos operacionales y
modernizar el proceso de admisión, la oficina de Aduanas y Protección de
Fronteras de los Estados Unidos (CBP) ha automatizado la forma I-94 (registro
de admisión) en los puertos de entrada tanto vía aérea como marítima. La forma
en papel ya no se le entregará a los viajeros en el momento de ingresar,
excepto en circunstancias limitadas. A cada viajero se le estampará un sello de
admisión del CBP en su documento de viaje.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Con la desincorporación del formulario
físico, la información sobre los extranjeros que arriban y partan será
recopilada y archivada a través de la página www.cbp.gov/I94. Esto aplica tanto
para los que necesitan visa estadounidense, como para los ciudadanos de países
que no la requieren.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Previo a su viaje, el visitante debe
entrar www.cbp.gov/I94 y completar la información solicitada. Seguidamente el
sistema proveerá un número único que el pasajero puede anotar para su propio
control. Al llegar a inmigración, mostrará al funcionario su pasaporte, quien
lo escaneará y obtendrá la declaración ya cargada<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>El mismo portal permite imprimir una
constancia de llenado del formulario, que le servirá al viajero para tramitar
documentos, extender su estadía o solicitar cambio de visa estando dentro de
los Estados Unidos de Norteamérica.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Las autoridades estiman con esto aumentar
la seguridad, agilizar el tiempo que toma ingresar al país y reducir las largas
filas en los aeropuertos.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Le recomendamos que apenas disponga de la
información sobre su viaje proceda inmediatamente a visitar el enlace y
completar el formulario digital.<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La sección consular de la embajada de
    los EE.UU. se permite recordar a los ciudadanos colombianos que piensan
    viajar a los Estados Unidos durante la temporada de vacaciones, que deben
    realizar los <span class=SpellE>tramites</span> de visa con suficiente
    anticipación, debido a la alta demanda de las mismas, las citas para
    entrevistas se están asignando en un tiempo más largo de lo habitual.<br>
    Actualmente y debido a la alta demanda de solicitudes, el tiempo de espera
    para la asignación de citas puede ser de hasta 3 semanas. Es importante que
    los ciudadanos colombianos tengan esto en cuenta al momento de planear sus
    viajes.</span><span style='font-size:12.0pt;font-family:"Times New Roman","serif";
    mso-fareast-font-family:"Times New Roman";mso-fareast-language:ES'><o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>