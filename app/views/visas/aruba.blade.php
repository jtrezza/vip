<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Embajada Real de los Países Bajos en
Bogotá:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Dirección: Cr 13 No. 93 - 40 Piso 2<br>
Teléfonos: 6384275 - 6384244<br>
Fax: 6112020<br>
Página web: http://colombia.nlembajada.org<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Horario de atención:<br>
Para el público radicar documentación: Lunes a viernes de 8:30 a.m. a 11:00
a.m.<br>
Para agencias de viajes radicar documentación: Lunes a jueves de 8:00 a.m.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Para recoger visados para público y agencias: Lunes a jueves 3:00 <span
class=SpellE>p.m.y</span> viernes 12:30 p.m.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DERECHOS CONSULARES</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Valor visa de 30 días $95.000<br>
Los pasajeros menores de 6 años no pagan derechos consulares.<br>
<br>
NOTA IMPORTANTE:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La Agencia de Viajes solo tramita
solicitudes de visa a Territorios Caribeños cuando el paquete turístico <span
class=SpellE>esta</span> confirmado por la misma, de lo contrario debe realizar
el trámite personalmente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Las solicitudes de visa a Territorios
Caribeños para negocios, visitas a familiares y amigos deben tramitarse
personalmente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La Embajada Real de los Países Bajos se
permite informar a la ciudadanía en general que no requerirán visa de corta
estadía para Territorios Caribeños aquellas personas con pasaporte colombiano que
tengan visa vigente con múltiples entradas o que sean portadoras de tarjetas de
residencia de Estados Unidos (<span class=SpellE>green</span> <span
class=SpellE>card</span>), Canadá, Reino Unido, Territorio <span class=SpellE>Schengen</span>
y/o Irlanda. La visa que se utilice para viajar debe estar vigente durante todo
el período de estadía.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Sin embrago, se debe tener en cuenta lo
siguiente:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Esta regulación NO APLICA para las
personas que desean residir en la Isla.<br>
Esta regulación aplica ÚNICAMENTE cuando la visa este vigente.<br>
Al igual que cuando los pasajeros cuentan con una visa vigente para Territorios
Caribeños, esta exención NO GARANTIZA que a la persona se le permita el ingreso
en la Isla pues depende de las autoridades de inmigración competentes.<br>
Si la visa vigente y utilizada está estampada en pasaporte diferente del vigente,
NO REPRESENTA NINGÚN INCONVENIENTE.<br>
La visa debe estar vigente durante todo el período de estadía en la(s) Isla(s).<br>
El tiempo de permanencia máxima en las Islas es de treinta (30) días,
distribuidas entre todas.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Si la comunicación anterior no aplica en
su caso y va a solicitar la visa personalmente, debe pedir cita previa a través
de la página web http://colombia.nlembajada.org<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La duración del trámite es aproximadamente
de 8 días hábiles, una vez ingresen los documentos a la Embajada.<br>
<br>
Para la presentación de solicitudes por parte de la agencia, la Embajada asigna
citas según disponibilidad (en temporada alta se debe manejar con 20 días de
anticipación).<br>
<br>
NOTA: Lo anterior aplica para Aruba y Antillas Neerlandesas (Curazao, <span
class=SpellE>Bonaire</span>, St. Maarten, <span class=SpellE>Saba</span> y St. <span
class=SpellE>Eustatius</span>).<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>TURISMO Y/O NEGOCIOS<br>
&#9679; Original y fotocopia del pasaporte vigente mínimo por 6 meses.<br>
&#9679; Fotocopia de la página biográfica del pasaporte.<br>
&#9679; Formulario, debidamente diligenciado sin fecha. (Ver recomendaciones
para diligenciar el formulario).<br>
&#9679; Dos fotografías 3x4 cm a color, fondo blanco.<br>
&#9679; Fotocopia de la cédula de ciudadanía o tarjeta de identidad, según el
caso.<br>
&#9679; Carta laboral indicando cargo, sueldo, tiempo de servicio y motivo del
viaje.<br>
&#9679; Extractos bancarios de los últimos tres meses y certificación bancaria.<br>
&#9679; Reserva aérea confirmada (si viaja con acompañantes adjuntar copias de
las visas vigentes y utilizadas que los eximen del tramite).</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
&#9679; Certificado de asistencia médica internacional con cobertura mínima de
10.000 euros o 15.000 dólares y cubrimiento en el Territorio Caribeño, durante
el período de estadía.</span><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
&#9679; Reserva de hotel. El <span class=SpellE>voucher</span> de hotel debe
estar confirmado por la agencia de viajes.<br>
&#9679; Para menores de edad: Registro civil de nacimiento, permiso de salida
del país firmado por los padres (o por el que no viaje con él/ella),
autenticado, anexando fotocopia de sus pasaportes.<br>
&#9679; Para estudiantes: Copia del carné estudiantil y certificación de
estudios en papel membrete indicando dirección y teléfono de la institución y
el cargo de quien firma la carta.<br>
&#9679; Para personas a cargo: Carta de responsabilidad de gastos.<br>
Viajes de negocios:<br>
&#9679; Para negocios: Copia de la carta de garantía legalizada del referente
expedida por el DIMAS, en caso de quedarse en casa de un socio, de lo contrario
deberá anexar reserva de hotel.<br>
Carta de invitación de la compañía que visitará.<br>
Copia del certificado de Cámara de Comercio de la compañía que va a visitar.<br>
En caso que la empresa colombiana lo invite, fotocopia de la Cámara de
Comercio, carta de responsabilidad de gastos y tres últimos extractos
bancarios.<br>
<br>
VISITA FAMILIAR (Curazao)<br>
&#9679; Copia de la carta de garantía legalizada del referente expedida por el
DIMAS, no inferior a tres meses.<br>
&#9679; Carta informando que parentesco tiene con el solicitante<br>
&#9679; Copia del pasaporte y/o permiso de residencia del referente.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>ESTUDIOS<br>
&#9679; En caso de quedarse con alguien copia de la carta de garantía
legalizada del referente expedida por el DIMAS, no inferior a tres meses.<br>
&#9679; Reserva de hotel o apartamento, si no tiene referencia.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
DURACION</span><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'>&nbsp;</span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><br>
Para Curazao, 8 días hábiles aproximadamente, y Aruba 5 días hábiles.<br>
El trámite se puede presentar hasta con tres (3) meses de anticipación a la
fecha del viaje.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aplicaciones de visa si el
itinerario de viaje implica varias de ellas, el número de hojas libres puede
cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Se expide una visa <span class=SpellE>valida</span>
para territorios Caribeños que los componen, ARUBA, CURAZAO, ST. MARTEEN,
BONAIRE ST. EUSTATIUS y SABA.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'><br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos deben confirmarse previamente
a la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho a modificar procedimientos y requerimiento sin previo aviso.<br>
<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Los solicitantes no están
    autorizados para trabajar, buscar empleo o aceptar un trabajo sin el
    consentimiento explicito de autoridades competentes de la Isla<br>
    &#9679; La visa de negocios “será referida”, por lo tanto la duración
    mínima es de ocho (8) días hábiles<br>
    &#9679; Para las personas que viajen a Aruba por turismo o visita familiar
    la permanencia máxima permitida es de 30 días. Para estadías mayores deben
    solicitar otro tipo de visa (estudiante, trabajo, etc..)<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Están exentos de presentar visa ante
    las autoridades de la Isla:<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>&#9679; Pasajeros de embarcaciones y/o
    conexiones de vuelos internacionales que no permanezcan en la Isla por más
    de 48 horas en Aruba o 24 en Curazao.<br>
    &#9679; Personas portadoras de tarjetas de residencia de Estados Unidos
    (Green <span class=SpellE>Card</span>), Canadá o cualquier país del área <span
    class=SpellE>Schengen</span>.<br>
    &#9679; Poseedores de visa vigente para los estados <span class=SpellE>Schengen</span>,
    Estados Unidos o Canadá, para visitas de corta estadía.<br>
    &#9679; Pasajeros a bordo de cruceros de turismo.<br>
    &#9679; La misión diplomática informará oportunamente en caso de negación
    de visas.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>Notas:<br>
    En el caso de visitar varias islas de los Territorios Caribeños, en el
    formulario se debe definir el destino principal de acuerdo con la estadía
    de más larga duración ó, si viaja de turismo a una isla y de visita a otra,
    prima el motivo de visita familiar<br>
    Las visas expedidas hasta la fecha por un año (o más) con entradas
    múltiples, o fueron impresas antes del 9 de Octubre del 2010, con fecha de
    validez posterior al 10 de Octubre, seguirán estando vigentes<br>
    En caso que la Misión Diplomática decida negar su solicitud de visa, se le
    informará oportunamente.<br>
    Los solicitantes no están autorizados para trabajar, buscar trabajo o
    aceptar un trabajo sin el consentimiento explícito de las autoridades
    competentes de Aruba.<br>
    La visa de negocios &quot;será referida&quot;, por lo tanto la duración del
    trámite es mínimo de 8 días hábiles.<br>
    La Embajada Real de los Países Bajos puede solicitar documentación
    adicional.<br>
    Para las personas que viajen a el Territorio Caribeño en calidad de turismo
    o visita familiar la permanencia máxima permitida es de 30 días. Para
    estadías mayores deben solicitar otro tipo de visa (estudiante, trabajo,
    etc.).<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>