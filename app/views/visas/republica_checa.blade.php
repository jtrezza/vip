<div class=Section1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:24.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>General</span><span style='font-size:
12.0pt;font-family:"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:12.0pt;font-family:
"Times New Roman","serif";mso-fareast-font-family:"Times New Roman";mso-fareast-language:
ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>La solicitud de visa a <span class=SpellE>Repúplica</span>
Checa se debe tramitar ante la Embajada de Francia en Bogotá (ver requisitos
Francia)<br>
<br>
TURISMO -hasta 90 días de estadía para ciudadanos colombianos / ecuatorianos:<br>
&#9679; Pasaporte con vigencia mínima de tres (3) meses<br>
&#9679; Formulario debidamente diligenciado<br>
&#9679; Una (1) fotografía 3.5 x 4.5 <span class=SpellE>cms.</span> en fondo
blanco ó azul claro con el rostro despejado y sin lentes oscuro<br>
&#9679; Copia de los datos biográficos del pasaporte<br>
&#9679; Copia de todas las visas vigentes<br>
&#9679; Seguro médico con cobertura internacional mínimo por 30.000 euros
vigente durante la estadía completa en la República Checa<br>
&#9679; Extractos bancarios de los tres (3) últimos meses<br>
&#9679; Tiquete aéreo y reserva hotelera confirmados y pagados<br>
&#9679; Documentos que acrediten que el solicitante dispondrá de medios
suficientes para su estadía<br>
&#9679; Invitación escrita de persona física o jurídica, con sede en la
República Checa haciendo constar responsabilidad en los gastos de estadía. Debe
ser en formulario oficial (ver <span class=SpellE>informacion</span> en checo
en<br>
www.mvcr.cz/2003/cizinci/info police.html#5) certificada por la policía de <span
class=SpellE>extrajería</span> de la República Checa.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Si el viaje es comercial,
cultural, deportivo periodístico o de estudio, invitación de un organismo con
sede en la República Checa. En este caso no se requiere la invitación
certificada por la policía extranjera de la República Checa<br>
&#9679; Certificado o invitación del organismo correspondiente en la República
Checa si se trata de un viaje de carácter humanitario<br>
&#9679; Certificado o invitación de establecimiento médico o balneario en la
República Checa del cual se desprenda claramente que el viaje persigue
objetivos médicos, indicando forma de pago del alojamiento y otros servicios<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Viajes organizados por agencias de
turismo: <span class=SpellE>Voucher</span> y copia del pago total de los
servicios con la siguiente información: nombres y apellidos del viajero, fecha
de nacimiento, nacionalidad, fecha y detalles de los servicios turísticos, en
papelería oficial de la agencia, y fecha y firma del responsable de la emisión
del <span class=SpellE>voucher</span><o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Si el viaje es individual y no se
dispone de los documentos anteriores, se deben presentar pruebas de solvencia
económica suficientes para sufragar la estadía. En efectivo será un mínimo de
1180 coronas checas ó USD 50 por persona/día, <span class=SpellE>bién</span> en
cheques de viajero, tarjeta de crédito internacional o extractos bancarios con
disponible suficiente para atender los gastos de viaje<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&#9679; Para menores de edad, anexar folio
del Registro Civil nacimiento, permiso de salida del país firmado por ambos
padres y certificación de estudios.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Las visas de larga duración se deben
tramitar por medio de la Embajada de República Checa en Lima. Se exige la
presencia personal del solicitante en la Embajada. Los visados <span
class=SpellE>Schengen</span> de corta duración también se pueden <span
class=SpellE>tamitar</span> a través de la Embajada en Lima.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
EMBAJADA REPUBLICA CHECA EN LIMA<br>
Dirección: <span class=SpellE>Baltazar</span> La Torre 398, ( Esquina de Av.
Salaverry con Av. Javier Prado) Lima, <span class=SpellE>Peru</span><br>
Teléfonos: (511) 264 3374 - 2643381</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Fax: (511 ) 264 1708<br>
Página Web: www.mzv.cz/lima<br>
E-mail: lima@embassy.mzv.cz<br>
Horario de atención, sección Consular : Martes y Jueves: 9:00 - 12:00<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>DURACION: Un (1) mes aproximadamente<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Requisitos</span><span style='font-size:
10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Algunos consulados exigen que los
pasaportes dispongan de 3 ó 4 hojas libres para aceptar aplicaciones de visa.
Si el itinerario de viaje implica varias de ellas, el número de hojas libres
puede cambiar.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>A partir del 5 de septiembre de 2013 los
trámites para Visa <span class=SpellE>Schengen</span> deben realizarse
personalmente en las embajadas de las naciones miembros con sede en Bogotá.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
Cada persona debe dirigirse a la embajada para solicitar la visa y registrar
sus huellas dactilares y una foto digital.</span><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Este registro tendrá vigencia de cinco años y la información estará disponible
cada vez que se solicite una visa en los consulados de países que hacen parte
de la zona <span class=SpellE>Schengen</span>.</span><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'><br>
Después de hacer el registro y durante los 5 años de vigencia, se podrá
tramitar el documento por medio de una agencia de viajes.</span><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>&nbsp;</span><span style='font-size:10.0pt;
font-family:"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:black;mso-fareast-language:ES'><br>
<br>
<br>
</span><span class=SpellE><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Actuacion</span></span><span
style='font-size:10.0pt;font-family:"Verdana","sans-serif";mso-fareast-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:black;
mso-fareast-language:ES'><o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:10.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:black;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:black;mso-fareast-language:ES'>Este contenido debe tenerse en cuenta como
informativo de tipo general y en todos los casos debe confirmarse previamente a
la iniciación de cualquier trámite por cuanto los consulados se reservan el
derecho de modificar procedimientos y requerimientos sin previo aviso.<br>
<br>
</span><span style='font-size:18.0pt;font-family:"Verdana","sans-serif";
mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
color:silver;mso-fareast-language:ES'>Observaciones<o:p></o:p></span></p>

<div class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal'><span style='font-size:18.0pt;font-family:
"Verdana","sans-serif";mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:
"Times New Roman";color:silver;mso-fareast-language:ES'>

<hr size=1 width="100%" noshade style='color:silver' align=center>

</span></div>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;mso-cellspacing:0cm;background:#DDEAF3;mso-yfti-tbllook:
 1184;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
  <td style='padding:0cm 0cm 0cm 0cm'></td>
 </tr>
 <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
  <td colspan=2 style='padding:0cm 0cm 0cm 0cm'>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
   style='width:100.0%;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:
   7.5pt 7.5pt 7.5pt 7.5pt'>
   <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
    <td style='padding:7.5pt 7.5pt 7.5pt 7.5pt'>
    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;
    line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>La <span class=SpellE>Republica</span>
    Checa hace parte del espacio <span class=SpellE>Schengen</span>. El trámite
    es personal en Bogotá y requiere cita previa, la cual se solicita en los
    teléfonos (1) 640 0600/1 ó al fax (1) 640 0599.<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>CONSULADO DE FRANCIA EN BOGOTÁ:<br>
    Dirección: Carrera 11 No. 93-12<br>
    Teléfonos: 638 1490 - 638 1400 - 618 1444.<br>
    Fax: 638 1491<br>
    Horario de atención al público: Lunes a viernes de 8:00 a.m. a 12:00 m.<br>
    Horario de atención agencias de viajes: Miércoles 2:00 p.m.<br>
    Página web: www.ambafrance-co.org - www.diplomatie.gouv.fr<o:p></o:p></span></p>
    <p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:
    auto;line-height:normal'><span style='font-size:10.0pt;font-family:"Verdana","sans-serif";
    mso-fareast-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman";
    color:black;mso-fareast-language:ES'>NOTAS:<br>
    &#9679; Los niños que no hayan cumplido 10 años no presentan solicitud de
    visa<br>
    &#9679; Si el itinerario del viajero incluye visitas posteriores a países
    que también exigen visa, será necesario presentarlas al iniciar el trámite
    ante el consulado checo.<br>
    &#9679; En caso de múltiples entradas al país, el solicitante deberá firmar
    una declaración con compromiso de disponer de un seguro médico para cada
    entrada a la República Checa.<o:p></o:p></span></p>
    </td>
   </tr>
  </table>
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>