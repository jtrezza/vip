@extends('layout')
@section('content')
<?php
    $datos = PaginaPrincipal::find(1);
    $promos = Promocion::orderBy('created_at','desc')->take(3)->get();
    $promos2 = Promocion::orderBy('created_at','desc')->skip(3)->take(3)->get();
?>
<section class="no-margin" id="main-slider">
        <div class="carousel slide wet-asphalt">
            <ol class="carousel-indicators">
                <li class="" data-slide-to="0" data-target="#main-slider"></li>
                <li data-slide-to="1" data-target="#main-slider" class=""></li>
                <li data-slide-to="2" data-target="#main-slider" class="active"></li>
            </ol>
            <div class="carousel-inner">
                <div style="background-image: url({{asset('assets/images/bg1.jpg')}})" class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered" style="margin-top: 264px;">
                                    <h2 class="<?php if ($datos->titulo_uno != '') {echo "boxed";} ?> animation animated-item-1">{{$datos->titulo_uno}}</h2>
                                    <p class="<?php if ($datos->descripcion_uno != '') {echo "boxed";} ?> animation animated-item-2">{{$datos->descripcion_uno}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div style="background-image: url({{asset('assets/images/bg2.jpg')}})" class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered" style="margin-top: 207.5px;">
                                    <h2 class="<?php if ($datos->titulo_dos != '') {echo "boxed";} ?> animation animated-item-1">{{$datos->titulo_dos}}</h2>
                                    <p class="<?php if ($datos->detalle_dos != '') {echo "boxed";} ?> animation animated-item-2">{{$datos->detalle_dos}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div style="background-image: url({{asset('assets/images/bg3.jpg')}})" class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content centered" style="margin-top: 218px;">
                                    <h2 class="<?php if ($datos->titulo_tres != '') {echo "boxed";} ?> animation animated-item-1">{{$datos->titulo_tres}}</h2>
                                    <p class="<?php if ($datos->detalle_tres != '') {echo "boxed";} ?> animation animated-item-2">{{$datos->detalle_tres}}</p><br />
                                    <a href="{{url('conoce_mas')}}" class=" boxed btn btn-md animation animated-item-3">¡Conoce más!</a>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="centered" style="margin-top: 129px;">
                                    <div class="embed-container">
                                        <iframe width="560" height="315" src="{{$datos->url_video}}" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a data-slide="prev" href="#main-slider" class="prev hidden-xs">
            <i class="icon-angle-left"></i>
        </a>
        <a data-slide="next" href="#main-slider" class="next hidden-xs">
            <i class="icon-angle-right"></i>
        </a>
    </section>
    <!--<section class="emerald" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-twitter icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Twitter Marketing</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-facebook icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Facebook Marketing</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-google-plus icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Google Plus Marketing</h3>
                            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->

    <section id="recent-works">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3>Útlimas promociones</h3>
                    <p>Echa un vistazo a nuestras últimas promociones.</p>
                    <div class="btn-group">
                        <a class="btn btn-danger" href="#scroller" data-slide="prev"><i class="icon-angle-left"></i></a>
                        <a class="btn btn-danger" href="#scroller" data-slide="next"><i class="icon-angle-right"></i></a>
                    </div>
                    <p class="gap"></p>
                </div>
                <div class="col-md-9">
                    <div id="scroller" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    @foreach($promos as $p)
                                        @include('promociones/item_carrusel', array('p'=>$p))
                                    @endforeach
                                </div><!--/.row-->
                            </div><!--/.item-->
                            <div class="item">
                                <div class="row">
                                    @foreach($promos2 as $p)
                                        @include('promociones/item_carrusel', array('p'=>$p))
                                    @endforeach
                                </div><!--/.row-->
                            </div><!--/.item-->
                        </div>
                    </div>
                </div>
            </div><!--/.row-->
        </div>
    </section><!--/#recent-works-->
    <section id="testimonial" style="background-color:#BCDDE6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="center">
                            <h2>Nuestra empresa</h2>
                            <p></p>
                        </div>
                        <div class="gap"></div>
                        <div class="row">
                            <div>
                                <div class="col-md-6">
                                    <h2>Quienes somos</h2>
                                    <p class="parrafo-empresa">Somos una empresa especializada en la prestación de servicios turísticos comprometidos con hacer realidad las expectativas y sueños de cada uno de nuestros socios ofreciéndoles los mejores sitios turísticos a los mejores precios. Contamos con un personal que cuenta con la mayor preparación y con las herramientas necesarias para responder a las necesidades de nuestros socios de una manera eficiente y eficaz. Nuestros servicios pueden ser desde un simple pasaje aéreo hasta los más elaborados programas vacacionales..</p>
                                    <!--<small>Someone famous in <cite title="Source Title">Source Title</cite></small>-->
                                </div>
                                <div class="col-md-6">
                                <h2>Misión</h2>
                                    <p class="parrafo-empresa">Proporcionar a nuestros socios facilidades que le permita disfrutar de los más grandiosos sitios turísticos tanto nacionales como internacionales, brindando la mejor atención y asesoría con personal altamente calificado, realizando un seguimiento personalizado que garantice la satisfacción de nuestros socios y eliminando los intermediarios para así conceder los mejores precios del mercado.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div>
                                <div class="col-md-6">
                                    <h2>Visión</h2>
                                    <p class="parrafo-empresa">Posicionarnos en el mercado nacional e internacional como una prestigiosa empresa que ofrece el mejor portafolio de planes turísticos destacándonos entre nuestros socios por nuestra excelente atención, asesoría, transparencia, y respeto.</p>

                                </div>
                                <div class="col-md-6">
                                <h2>Valores</h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul>
                                            <li>Respeto</li>
                                            <li>Amabilidad</li>
                                            <li>Responsabilidad</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul>
                                            <li>Confiabilidad</li>
                                            <li>Transaparencia</li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/#testimonial-->
@stop