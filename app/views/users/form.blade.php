@extends('admin.layout')
<?php
    if ($usuario->exists):
        $form_data = array('route' => array('panel.usuarios.update', $usuario->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.usuarios.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>usuario <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/usuarios')}}"><i class="icon-dashboard"></i> Usuarios</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
	@if(!$usuario->exists)
		<div class="alert alert-warning">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    <strong>Acerca de las contraseñas:</strong>
		    <p>La contraseña por defecto es el mismo nombre de usuario. Los usuarios podrán cambiarla
		    	más adelante al ingresar al sistema.</p>
		</div>
	@endif
</div>
<div>
	{{ Form::model($usuario,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('oficina_id', 'Oficina') }}
	     	{{ Form::select('oficina_id', $oficinas_a, null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('name', 'Nombre') }}
	     	{{ Form::text('name', null, array('placeholder' => 'Nombre', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('username', 'Username') }}
	     	{{ Form::text('username', null, array('placeholder' => 'Nombre de usuario', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('email', 'e-mail') }}
	     	{{ Form::text('email', null, array('placeholder' => 'Correo electrónico', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('role_id', 'Rol de usuario') }}
	     	{{ Form::select('role_id', $roles_a, 2, array('class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop