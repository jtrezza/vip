@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Usuarios <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/usuarios')}}"><i class="icon-dashboard"></i> Usuarios</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.usuarios.create') }}" class="btn btn-primary">Crear un nuevo usuario</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Nombre</th>
			<th>Username</th>
			<th>Tipo</th>
			<th>Oficina</th>
			<th>e-mail</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($usuarios as $usuario)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$usuario->name}}</td>
				<td>{{$usuario->username}}</td>
				<td>{{$roles[$usuario->role_id]}}</td>
				<td>{{$usuario->oficina->nombre}}</td>
				<td>{{$usuario->email}}</td>
				<td><a href="{{ route('panel.usuarios.edit', $usuario->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($usuario, array('route' => array('panel.usuarios.destroy', $usuario->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $usuarios->links() }}
</div>
@stop