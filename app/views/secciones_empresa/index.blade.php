@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Empresa <small>Secciones</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/empresa')}}"><i class="icon-dashboard"></i> Empresa</a></li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="#" class="btn btn-primary">Oficinas</a>
	<a href="#" class="btn btn-primary">Closers</a>
	<a href="#" class="btn btn-primary">Clientes</a>
</p>
@stop