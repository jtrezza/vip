@extends('admin.layout')
<?php
    if ($seccion_empresa->exists):
        $form_data = array('route' => array('panel.empresa.update', $seccion_empresa->id), 'method' => 'PATCH');
    else:
        $form_data = array('route' => 'panel.empresa.store', 'method' => 'POST');
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Sección empresa <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/empresa')}}"><i class="icon-dashboard"></i> Empresa</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($seccion_empresa,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('title', 'Título') }}
	     	{{ Form::text('title', null, array('placeholder' => 'Nombre', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('text', 'Texto') }}
        <div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        Para introducir títulos en <b>negrita</b> escriba el texto entre asteriscos (*ejemplo*). Para escribir listas, ingrese los elementos dentro de corchetes, separados por comas (ejemplo: [elemento1, elemento2, elemento3]).</div>
	      {{ Form::textarea('text', null, array('placeholder' => 'Ingrese aquí el texto de la sección.', 'class' => 'form-control')) }}        
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop