<div class="row">
	<div class="col-md-5">
		
	</div>
	<div class="col-md-7">
		<h3>{{$p->title}}</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-5">
		<a href="{{url('uploads',$p->picture)}}" data-toggle="lightbox" data-title="{{$p->title}}" data-footer="{{$p->large}}">
			<img class="thumbnail" title="Click para ampliar" style="max-width:280px; margin:auto;" src="{{url('uploads',$p->picture)}}">
		</a>
	</div>
	<div class="col-md-7">
		<p style="text-align:justify">{{$p->short}}</p>
	</div>
</div>