@extends('admin.layout')
<?php
    if ($plan->exists):
        $form_data = array('route' => array('panel.planes.update', $plan->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.planes.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Plan de viaje <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/planes')}}"><i class="icon-dashboard"></i> Planes</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($plan,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('plan_tipo_id', 'Tipo de plan') }}
	     	{{ Form::select('plan_tipo_id', $planes_tipos_a, null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('title', 'Título') }}
	     	{{ Form::text('title', null, array('placeholder' => 'Nombre del plan de viaje', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('short', 'Primera parte descripción') }}
	      {{ Form::textarea('short', null, array('placeholder' => 'Es la primera parte de la descripción. Se verá en la vista de lista.', 'class' => 'form-control')) }}        
	    </div>
	    <div class="form-group">
	      {{ Form::label('large', 'Segunda parte descripción') }}
	      {{ Form::textarea('large', null, array('placeholder' => 'Texto adicional. Aparecerá en la sección "Ver más".', 'class' => 'form-control')) }}        
	    </div>
	    <div class="form-group">
            <label for="picture">Imagen</label>
            {{Form::file('picture',array('id'=>'picture'));}}
            <p class="help-block">Ésta es la imagen que acompañará la sección del Plan de viaje.</p>
        </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop