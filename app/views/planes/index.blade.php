@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Plan de viaje <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('planes')}}"><i class="icon-dashboard"></i> Planes</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.planes.create') }}" class="btn btn-primary">Crear un nuevo plan</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Nombre</th>
			<th>Tipo</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($planes as $plan)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$plan->title}}</td>
				<td>{{$plan->plan_tipo->descripcion}}</td>
				<td><a href="{{ route('panel.planes.edit', $plan->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($plan, array('route' => array('panel.planes.destroy', $plan->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $planes->links() }}
</div>
@stop