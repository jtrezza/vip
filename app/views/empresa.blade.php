@extends('layout')
@section('content')
<div class="row">
	<?php $modulo = 0; ?>
	@foreach ($secciones_empresa as $e)
		<?php $modulo++; ?>
		<div class="col-md-6">
			<h2>{{$e->title}}</h2>
			<p class="parrafo">{{$e->text}}</p>
		</div>
		<?php if($modulo%2 == 0): ?>
			<div class="row"></div>
		<?php endif; ?>
	@endforeach
</div>
@stop