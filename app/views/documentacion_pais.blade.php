@extends('layout')
@section('content')
<style type="text/css">
p{
    text-align: justify;
}
</style>
<section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Documentación</h1>
                </div>
            </div>
        </div>
    </section><!--/#title-->
<div style="padding:4em 2em 2em 2em;">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="text-center">{{$nombre_pais}}</h1>
			<div class="blog" id="contenido_visa">
				@include('visas/'.$pais)
            </div>
		</div>
		<div class="col-md-4 aside-der" style="padding-left:3em;">
			@include('aside')
		</div>
	</div>
	<div class="row" style="padding-left:4em;">
		<div class="col-md-12"></div>
	</div>
</div>
@stop