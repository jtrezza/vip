@extends('admin.layout')
<?php
    if ($closer->exists):
        $form_data = array('route' => array('panel.closers.update', $closer->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.closers.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Closer <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/closers')}}"><i class="icon-dashboard"></i> Closers</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($closer,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('oficina_id', 'Oficina') }}
	     	{{ Form::select('oficina_id', $oficinas_a, null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('nombre', 'Nombre') }}
	     	{{ Form::text('nombre', null, array('placeholder' => 'Nombre', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('cedula', 'Cédula') }}
	      {{ Form::text('cedula', null, array('placeholder' => 'Cédula', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('porcentaje_100_mayor_seis', 'Porcentaje venta 100% mayor a 6 millones') }}
	      {{ Form::text('porcentaje_100_mayor_seis', null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('porcentaje_100_mayor_cuatro', 'Porcentaje venta 100% mayor a 4 millones') }}
	      {{ Form::text('porcentaje_100_mayor_cuatro', null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('porcentaje_100_mayor_uno_cuatro_cuarenta', 'Porcentaje venta 100% mayor a 1.440.000') }}
	      {{ Form::text('porcentaje_100_mayor_uno_cuatro_cuarenta', null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('porcentaje_50', 'Porcentaje venta 50%') }}
	      {{ Form::text('porcentaje_50', null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
            {{ Form::label('tipo', 'Tipo') }}
            {{ Form::select('tipo', array('CLOSER'=>'CLOSER','OPC'=>'OPC','LINER'=>'LINER'), null, array('class' => 'form-control')) }}
        </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop