@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Closer <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/closers')}}"><i class="icon-dashboard"></i> Closers</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.closers.create') }}" class="btn btn-primary">Crear un nuevo closer</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Nombre</th>
			<th>Cédula</th>
			<th>Oficina</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($closers as $closer)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$closer->nombre}}</td>
				<td>{{$closer->cedula}}</td>
				<td>{{$closer->oficina->nombre}}</td>
				<td><a href="{{ route('panel.closers.edit', $closer->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($closer, array('route' => array('panel.closers.destroy', $closer->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $closers->links() }}
</div>
@stop