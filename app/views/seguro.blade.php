@extends('layout')
<?php
    if ($solicitud_seguro->exists):
        $form_data = array('route' => array('seguro.update', $solicitud_seguro->id), 'method' => 'PATCH', 'class'=>'form-horizontal');
    else:
        $form_data = array('route' => 'seguro.store', 'method' => 'POST', 'class'=>'form-horizontal');
    endif;

?>
@section('content')

<div style="padding:4em 2em 2em 2em;">
<div class="row">
	<div class="col-md-8">
			<h1 class="text-center">Seguro de viaje<br /><small>Ingrese sus datos para solicitar un seguro de viaje</small></h1>
		<br />
		<div>
			@include ('errors', array('errors' => $errors)) 
		</div>
		{{ Form::model($solicitud_seguro, $form_data, array('role' => 'form')) }}
		    <div class="form-group">
		      	{{ Form::label('nombre', 'Nombre *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('nombre', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('cedula', 'Cédula *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('cedula', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('telefono', 'Teléfono *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('telefono', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('email', 'e-mail *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('email', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('ciudad', 'Ciudad *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('ciudad', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('numero_tiquete', 'Número de tiquete *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('numero_tiquete', null, array('class' => 'form-control')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('vuelo', 'Vuelo *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('vuelo', null, array('class' => 'form-control')) }}
		     	</div>
		    </div><div class="form-group">
		      	{{ Form::label('fecha_salida', 'Fecha de salida *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('fecha_salida', null, array('class' => 'form-control datepicker')) }}
		     	</div>
		    </div>
		    <div class="form-group">
		      	{{ Form::label('fecha_regreso', 'Fecha de regreso *', array('class'=>'control-label col-sm-2')) }}
		      	<div class="col-sm-10">
		     		{{ Form::text('fecha_regreso', null, array('class' => 'form-control datepicker')) }}
		     	</div>
		    </div>
		    <div class="col-sm-12">
		    	{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary', 'style'=>'float:right;')) }}    
		    </div>
		  
		  
		{{ Form::close() }}
	</div>
	<div class="col-md-4 aside-der">
		@include('aside')
	</div>
</div>
</div>
@stop