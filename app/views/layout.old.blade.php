<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title', 'Agencia de Viajes VIP Tours Colombia')</title>

	<!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/global.css')}}" rel="stylesheet">

    <!-- jQuery & Bootstrap JS -->
    <script src="{{asset('assets/js/jquery-2.1.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/js/global.js')}}"></script>
    @yield('head')
</head>
<body>
<div class="row">
	<div class="col-lg-8 col-lg-offset-1">
		<img src="{{asset('assets/images/logo_vip.png')}}">   
	</div>
	<div class="col-lg-2 text-center" style="padding-top:2em;">
		<h2 style="color:#092ca5;">(575)3516745</h2>
		<h2 style="color:#092ca5;">Llama ahora</h2>  
	</div>
</div>
<div class="row">
	<div class="col-lg-10 col-lg-offset-1">
		<nav class="navbar navbar-custom" role="navigation">
			  <div class="container-fluid" style="float:center;">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div id="menu-inicio" class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="{{ url('/') }}">INICIO</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav navbar-center">
			        <li id="menu-empresa"><a href="{{ url('empresa') }}">EMPRESA</a></li>
			        <li id="menu-planes" class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">PLANES TURÍSTICOS <b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="{{ url('planes/nacionales') }}">NACIONAL</a></li>
			            <li><a href="{{ url('planes/internacionales') }}">INTERNACIONAL</a></li>
			          </ul>
			        </li>
			        <li id="menu-cruceros"><a href="{{ url('cruceros') }}">CRUCEROS</a></li>
			        <li id="menu-promociones"><a href="{{ url('/promociones') }}">PROMOCIONES</a></li>
			        <li id="menu-seguro"><a href="{{ url('/seguro') }}">SEGURO DE VUAJE</a></li>
			        <li id="menu-documentacion"><a href="{{ url('/documentacion') }}">VISAS</a></li>
			        <li id="menu-contacto"><a href="#">CONTACTO</a></li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>  
	</div>
</div>
<div class="row">
	<div class="col-lg-10 col-lg-offset-1" id="contenedor">
		@yield('content')
	</div>
</div>
<div style="background:#233d96; color:white; margin-top:5em; padding-top:2em;" class="text-center row">
	<div class="col-sm-2 col-sm-offset-1">
		<p><b>Oficina Barranquilla</b> <br />
			CR 54 # 72-147 LC 107 <br />
			Reservas: (5)3569392 - (5)3569406 <br />
			Cels: 3163051121 - 3187116574</p>
	</div>
	<div class="col-sm-3">
		<p><b>Oficina Cartagena</b> <br />
		CR 3 # Calle 6A esquina Ofc. 404/405 <br />
		Edificio Jasban (Sector Bocagrande) <br />
		Teléfonos: (5)6658546 / 6650576 <br />
		e-mail: viptourscolombia@hotmail.es</p>
		</p>
	</div>
	<div class="col-sm-3"><p><b>Oficina Santa Marta</b><br />
		CR 4 # 26 - 40 Ofc. 505 C.C. Prado Plaza<br />
		Teléfonos: (5) 4234039 - (5) 4230579<br />
	</div>
	<div class="col-sm-2">
		<p><b>Oficina Pereira</b> <br />
		CR 13 # 15-35 LC 222 C.C. Pereira Plaza <br />
		Teléfonos: (6) 3413350 - (6) 3005489027 <br />
	</div>
</div>
<script type="text/javascript">
  (function(){
    $("#menu-{{$activo}}").addClass('active');
  })();
</script>
</body>
</html>