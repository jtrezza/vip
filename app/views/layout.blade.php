<!DOCTYPE html>
<html lang="en" class="csstransforms csstransforms3d csstransitions"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="">
<meta name="description" content="">
<title>Agencia de Viajes VIP Tours Colombia</title>

<link href="{{asset('assets/css/bootstrap.min.mezzanine.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/mezzanine.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/datepicker.css')}}" rel="stylesheet">

<link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/ekko-lightbox.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/main.css')}}" rel="stylesheet">

 <!--[if lt IE 9]>
    <script src="static/js/html5shiv.js"></script>
    <script src="static/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="http://127.0.0.1:8000/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://127.0.0.1:8000/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://127.0.0.1:8000/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://127.0.0.1:8000/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://127.0.0.1:8000/images/ico/apple-touch-icon-57-precomposed.png">

<script src="{{asset('assets/js/jquery-1.7.1.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-extras.js')}}"></script>
<script src="{{asset('assets/js/ekko-lightbox.js')}}"></script>
<script src="{{asset('assets/js/global.js')}}"></script>

<script>

</script>



<!--[if lt IE 9]>
<script src="static/js/html5shiv.js"></script>
<script src="static/js/respond.min.js"></script>
<![endif]-->


</head>

<body id="body">

<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
        <div class="container" id="arriba">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><img style="max-height:60px;" src="{{asset('assets/images/vip_transparente.png')}}"></a><a class="navbar-brand" style="margin-top:25px;" href="{{ url('/') }}"> Tours Colombia</a>
				<!--<p class="navbar-text visible-lg"></p>-->
            </div>
            <div class="collapse navbar-collapse">
               
                <ul class="nav navbar-nav navbar-right" style="margin-top:30px;">
                    <li class="dropdown" id="about">
                        <a href="#" class="dropdown-toggle disabled" data-toggle="dropdown">Planes<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="" id="about-team">
                                    <a href="{{ url('planes/nacionales') }}">Nacionales</a>
                                </li>
                                <li class="" id="about-team">
                                    <a href="{{ url('planes/internacionales') }}">Inernacionales</a>
                                </li>
                            </ul>
                    </li>

                    <li class="" id="cruceros">
                        <a href="{{ url('cruceros') }}">Cruceros</a>
                    </li>
                    <li class="" id="promociones">
                        <a href="{{ url('/promociones') }}">Promociones</a>
                    </li>
                    {{-- <li class="" id="seguro">
                        <a href="{{ url('/seguro') }}">Seguro</a>
                    </li> --}}
                    <li class="" id="documentacion">
                        <a href="{{ url('/documentacion') }}">Documentación</a>
                    </li>
                    <li class="" id="contact">
                        <a href="{{ url('/contacto') }}">Contacto</a>
                    </li>
                </ul>
            </div>
        </div>
    </header><!--/header-->


<div class="container">

</div>

@yield('content')
    
<section id="bottom" class="wet-asphalt">
	<div class="container">
	


                <div class="col-md-3 col-sm-6">
                    <h4>Barranquilla</h4>
                    <p><strong>Dirección</strong><br> CRA 54 # 72-147 Local 107 C.C. Boulevar 54</p>
                    <p><strong>Teléfonos</strong><br> (50) 3854169-3584174-3584178 Celular 3163051121-3187116574</p>
                </div><!--/.col-md-3-->
              
				<div class="col-md-3 col-sm-6">
                    <h4>Cartagena</h4>
                    <address>
                        <p><strong>Dirección</strong><br>
                        CRA 3 A CALLE 6 A esquina Oficina 404/405</p>
                        <p><strong>Teléfonos</strong><br> (51) 63933342-6650576 Celular 3163054634</p>
                    </address>
                </div> <!--/.col-md-3-->
                <div class="col-md-3 col-sm-6">
                    <h4>Pereira</h4>
                    <p><strong>Dirección</strong><br> CRA 13 # 15-35 Local 222 Centro Comercial Pereira Plaza</p>
                    <p><strong>Teléfonos</strong><br>  (60) 3401388 Celular 3163118747-3163704672</p>
                </div><!--/.col-md-3-->
                <div class="col-md-3 col-sm-6">
                    <h4>Manizales</h4>
                    <p><strong>Dirección</strong><br> CRA 27 A # 66-30 Local 841 Centro Comercial Sancancio</p>
                    <p><strong>Teléfonos</strong><br> (61) 8928016 Celular 3187123312</p>
                </div><!--/.col-md-3-->


</section>
<footer id="footer" class="midnight-blue">
<div class="container">


<div class="row">
<div class="col-sm-12" style="text-align:center;">
                    &copy; {{ date('Y') }} - VIP Tour Colombia 
</div><div class="col-sm-6">
    <ul class="pull-right">
        <li>
            <a id="gototop" class="gototop" href="#"><i class="icon-chevron-up"></i></a>
        </li>
    </ul>
    </div>

</div>
</div>
</footer>
    <script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
</body>
</html>