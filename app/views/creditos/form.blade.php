@extends('admin.layout')
<?php
    if ($credito->exists):
        $form_data = array('route' => array('panel.creditos.update', $credito->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.creditos.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Crédito <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($credito,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('numero', 'Número') }}
	     	{{ Form::text('numero', null, array('placeholder' => 'Número', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('fecha', 'Fecha') }}
	     	{{ Form::text('fecha', null, array('placeholder' => 'Fecha', 'class' => 'form-control datepicker')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('gl', 'G.L.') }}
	     	{{ Form::text('gl', null, array('placeholder' => 'Gastos legales', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('valor_contrato', 'Valor contrato') }}
	     	{{ Form::text('valor_contrato', null, array('placeholder' => 'Valor contrato', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('nombres_titular', 'Nombres titular') }}
	     	{{ Form::text('nombres_titular', null, array('placeholder' => 'Nombres del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('apellidos_titular', 'Apellidos titular') }}
	     	{{ Form::text('apellidos_titular', null, array('placeholder' => 'Apellidos del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('cedula_titular', 'Cedula titular') }}
	     	{{ Form::text('cedula_titular', null, array('placeholder' => 'Cédula del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('cootitulares', 'Cootitulares') }}
	     	{{ Form::text('cootitulares', null, array('placeholder' => 'Nombres de los cootitulares', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('beneficiarios', 'Beneficiarios') }}
	     	{{ Form::text('beneficiarios', null, array('placeholder' => 'Beneficiarios', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('direccion_titular', 'Dirección titular') }}
	     	{{ Form::text('direccion_titular', null, array('placeholder' => 'Dirección del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('telefono_titular', 'Teléfono titular') }}
	     	{{ Form::text('telefono_titular', null, array('placeholder' => 'Teléfono del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('celular_titular', 'Celular titular') }}
	     	{{ Form::text('celular_titular', null, array('placeholder' => 'Celular del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('email_titular', 'e-mail titular') }}
	     	{{ Form::text('email_titular', null, array('placeholder' => 'Correo electrónico del titular', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('vigencia', 'Vigencia') }}
	     	{{ Form::text('vigencia', null, array('placeholder' => 'Vigencia', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('producto_solicitado', 'Producto solicitado') }}
	     	{{ Form::text('producto_solicitado', null, array('placeholder' => 'Producto solicitado', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('liner', 'Liner') }}
	     	{{ Form::text('liner', null, array('placeholder' => 'Liner', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('closer_id', 'Closer') }}
	     	{{ Form::select('closer_id', $closers_a, null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
            {{ Form::label('servicio_cliente', 'Servicio al cliente') }}
            {{ Form::text('servicio_cliente', null, array('placeholder' => 'Servicio al cliente', 'class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('observaciones', 'Observaciones') }}
            {{ Form::text('observaciones', null, array('placeholder' => 'Observaciones', 'class' => 'form-control')) }}
        </div>
        <?php if (!$credito->exists): ?>
        <div class="form-group">
            {{ Form::label('primer_abono', 'Primer abono') }}
            {{ Form::text('primer_abono', null, array('placeholder' => 'Ingrese aquí el valor del primer abono del cliente', 'class' => 'form-control')) }}
        </div>
	    <?php endif; ?>
	    <div class="form-group">
	      	{{ Form::label('devolucion', '¿Devolución?') }}
	     	{{ Form::select('devolucion', array('0'=>'NO', '1'=>'SI'), null, array('class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop