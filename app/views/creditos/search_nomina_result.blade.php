@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Nomina <small>Búsqueda</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/nomina')}}"><i class="icon-dashboard"></i> Nómina</a></li>
      <li class="active"><i class="icon-file-alt"></i> Resultado búsqueda</li>
    </ol>
  </div>
</div><!-- /.row -->

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>Closer</th>
			<th>Tipo</th>
			<th>Fecha contrato</th>
			<th>No. contrato</th>
			<th>Valor Venta</th>
			<th>% Venta</th>
			<th>% comisión</th>
			<th>Tot. a pagar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($abonos as $abono)
			<?php $num++; ?>
			<tr>
				<td>{{$abono->credito->closer->nombre}}</td>
				<td>{{$abono->credito->closer->tipo}}</td>
				<td>{{$abono->credito->fecha}}</td>
				<td>{{$abono->credito->numero}}</td>
				<td>{{"$ ".number_format($abono->credito->valor_contrato,0,',','.')}}</td>
				<td>{{$abono->porcentaje . "%"}}</td>
				<td>{{$abono->porcentaje_comision . "%"}}</td>
				<td>{{"$ ".number_format($abono->total_a_pagar,0,',','.')}}</td>
			</tr>
			@endforeach
			<tr>
				<th colspan="3" class="text-right">Total vendido</th>
				<td>{{"$ ".number_format($total_total,0,',','.')}}</td>
				<th colspan="2" class="text-right">Total a pagar</th>
				<td>{{"$ ".number_format($total_comision,0,',','.')}}</td>
			</tr>
		</tbody>
	</table>
</div>
@stop