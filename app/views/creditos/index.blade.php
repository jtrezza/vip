@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Créditos <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.creditos.create') }}" class="btn btn-primary">Crear un nuevo crédito</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Número</th>
			<th>fecha</th>
			<th>Titular</th>
			<th>Cédula</th>
			<th>Valor</th>
			<th>Saldo</th>
			<th>Ver</th>
			<?php if(Auth::user()->role_id == 1): ?>
			<th>Editar</th>
			<th>Eliminar</th>
			<?php endif; ?>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($creditos as $credito)
			<?php $num++; ?>
			<tr class="<?= ($credito->devolucion == 1) ? 'bg-danger' : '' ?>">
				<td>{{$num}}</td>
				<td>{{$credito->numero}}</td>
				<td>{{$credito->fecha}}</td>
				<td>{{$credito->nombres_titular . ' ' . $credito->apellidos_titular}}</td>
				<td>{{$credito->cedula_titular}}</td>
				<td>{{ "$ ".number_format($credito->valor_contrato, 0, ',','.') }}</td>
				<td>{{ "$ ".number_format($credito->saldo, 0, ',','.') }}</td>
				<td><a href="{{ url('panel/creditos', $credito->id) }}" class="btn btn-success">Ver</a></td>
				<?php if(Auth::user()->role_id == 1): ?>
				<td><a href="{{ route('panel.creditos.edit', $credito->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($credito, array('route' => array('panel.creditos.destroy', $credito->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
				<?php endif; ?>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $creditos->links() }}
</div>
@stop