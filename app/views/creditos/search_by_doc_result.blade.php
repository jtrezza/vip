@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Créditos <small>Búsqueda</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li><a href="{{url('panel/search_by_doc')}}"><i class="icon-dashboard"></i> Búsqueda</a></li>
      <li class="active"><i class="icon-file-alt"></i> Resultado</li>
    </ol>
  </div>
</div><!-- /.row -->

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Número</th>
			<th>fecha</th>
			<th>Titular</th>
			<th>Cédula</th>
			<th>Valor</th>
			<th>Saldo</th>
			<th>Detalle</th>
			<?php if(Auth::user()->role_id == 1): ?>
			<th>Editar</th>
			<th>Eliminar</th>
			<?php endif; ?>
			<?php if( in_array (Auth::user()->role_id, array(1, 2))): ?>
			<th>Agregar pago</th>
			<?php endif; ?>
			<th>Historial pagos</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($creditos as $credito)
			<?php $num++; ?>
			<tr class="<?= ($credito->devolucion == 1) ? 'bg-danger' : '' ?>">
				<td>{{$num}}</td>
				<td>{{$credito->numero}}</td>
				<td>{{$credito->fecha}}</td>
				<td>{{$credito->nombres_titular . ' ' . $credito->apellidos_titular}}</td>
				<td>{{$credito->cedula_titular}}</td>
				<td>{{ "$ ".number_format($credito->valor_contrato, 0, ',','.') }}</td>
				<td>{{ "$ ".number_format($credito->saldo, 0, ',','.') }}</td>
				<td><a href="{{ url('panel/creditos', $credito->id) }}" class="btn btn-warning">Ver</a></td>
				<?php if(Auth::user()->role_id == 1): ?>
				<td><a href="{{ route('panel.creditos.edit', $credito->id) }}" class="btn btn-info">Editar</a></td>
				<td>
                    {{ Form::model($credito, array('route' => array('panel.creditos.destroy', $credito->id), 'method' => 'DELETE', 'role' => 'form')) }}
                    {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                </td>
                <?php endif; ?>
			    <?php if( in_array (Auth::user()->role_id, array(1, 2))): ?>
				<td><a href="{{ url('panel/nuevo_pago', $credito->id) }}" class="btn btn-success">Agregar</a></td>
			    <?php endif; ?>
				<td><a href="{{ url('panel/abonos/'. $credito->id) }}" class="btn btn-info">Ver pagos</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $creditos->links() }}
</div>
@stop