@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Crédito <small>búsqueda</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li class="active"><i class="icon-file-alt"></i> búsqueda</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::open(array('role' => 'form', 'url'=>'panel/search_by_doc_result'))}}
	    <div class="form-group">
	      	{{ Form::label('cedula_titular', 'Cedula titular') }}
	     	{{ Form::text('cedula_titular', null, array('placeholder' => 'Cédula del titular', 'class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Consultar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
	  
	{{ Form::close() }}
</div><!-- row -->
@stop