@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Nómina <small>consulta</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/nomina')}}"><i class="icon-dashboard"></i> Nómina</a></li>
      <li class="active"><i class="icon-file-alt"></i> consulta por fechas</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::open(array('role' => 'form', 'url'=>'panel/search_nomina_result', 'class'=>'form-horizontal'))}}
	    <div class="form-group">
	      	{{ Form::label('desde', 'Desde', array('class' => 'col-sm-1')) }}
	      	<div class="col-sm-2">
	     		{{ Form::text('desde', null, array('class'=>'form-control datepicker')) }}
	     	</div>
	     	{{ Form::label('hasta', 'Hasta', array('class' => 'col-sm-1')) }}
	     	<div class="col-sm-2">
	     		{{ Form::text('hasta', null, array('class'=>'form-control datepicker')) }}
	     	</div>
	    </div>
	    <div class="form-group">
	      	{{ Form::label('oficina_id', 'Oficina', array('class' => 'col-sm-1')) }}
	      	<div class="col-sm-2">
	     	{{ Form::select('oficina_id', $oficinas_a, null, 
	     		array('class' => 'form-control', 'onchange'=>'cambioOficina();')) }}
	     	</div>
	     	{{ Form::label('closer_id', 'Closer', array('class' => 'col-sm-1')) }}
	      	<div class="col-sm-2">
	     	{{ Form::select('closer_id', array('0'=>'TODOS'), null, array('class' => 'form-control')) }}
	     	</div>
	    </div>
	  {{ Form::button('Consultar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
	  
	{{ Form::close() }}
</div><!-- row -->
@stop