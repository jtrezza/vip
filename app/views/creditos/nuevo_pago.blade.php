@extends('admin.layout')
<?php
    if ($abono->exists):
        $form_data = array('route' => array('panel.abonos.update', $abono->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.abonos.store', 'method' => 'POST', 'files' => true);
    endif;

?>
<?php
	$credito = Credito::find($id_credito);
	if (is_null ($credito))
	{
		App::abort(404);
	}
?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Crédito <small>Nuevo pago</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li class="active"><i class="icon-file-alt"></i> Nuevo pago</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($abono, $form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('numero', 'Número') }}
	     	{{ Form::text('numero', $credito->numero, array('readonly' => 'readonly', 'class' => 'form-control')) }}
	     	{{ Form::hidden('credito_id', $credito->id) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('saldo', 'Cedula titular') }}
	     	{{ Form::text('saldo', $credito->cedula_titular, array('readonly' => 'readonly', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('nombres_titular', 'Nombre del titular') }}
	     	{{ Form::text('nombres_titular', $credito->nombres_titular. ' ' .$credito->apellidos_titular, array('readonly' => 'readonly', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('valor_contrato', 'Valor contrato') }}
	     	{{ Form::text('valor_contrato', $credito->valor_contrato, array('readonly' => 'readonly', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('saldo', 'Saldo de la deuda') }}
	     	{{ Form::text('saldo', $credito->saldo, array('readonly' => 'readonly', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('valor', 'Valor a abonar') }}
	     	{{ Form::text('valor', null, array('placeholder' => 'Ingrese el valor abonado a la deuda', 'class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop