@extends('admin.layout')
<?php
    if ($credito->exists):
        $form_data = array('route' => array('panel.creditos.update', $credito->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.creditos.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Crédito <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/creditos')}}"><i class="icon-dashboard"></i> Créditos</a></li>
      <li><a href="{{url('panel/search_by_doc')}}"><i class="icon-dashboard"></i> Búsqueda</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>

	    <div class="form-group">
	      	{{ Form::label('fecha', 'General') }}
	    </div>
	    <table class="table table-bordered">
			<tr>
				<th>Fecha</th>
				<th>Número de contrato</th>
				<th>Liner</th>
				<th>Closer</th>
			</tr>
			<tr>
				<td>{{ $credito->fecha }}</td>
				<td>{{ $credito->numero }}</td>
				<td>{{ $credito->liner }}</td>
				<td>{{ $credito->closer->nombre }}</td>
			</tr>
		</table>
	    <table class="table table-bordered">
			<tr>
				<th>G.A.</th>
				<th>VALOR</th>
				<th>SALDO</th>
				<th>VIGENCIA</th>
			</tr>
			<tr>
				<td>$ {{ number_format($credito->gl, 0, ',', '.') }}</td>
				<td>$ {{ number_format($credito->valor_contrato, 0, ',', '.') }}</td>
				<td>$ {{ number_format($credito->saldo, 0, ',', '.') }}</td>
				<td>{{ $credito->vigencia }}</td>
			</tr>
		</table>
		{{ Form::label('titular', 'TITULAR') }}
		<table class="table table-bordered">
			<tr>
				<th>NOMBRES</th>
				<th>APELLIDOS</th>
				<th>C.C.</th>
				<th>CELULAR</th>
				<th>E-MAIL</th>
			</tr>
			<tr>
				<td>{{ $credito->nombres_titular }}</td>
				<td>{{ $credito->apellidos_titular }}</td>
				<td>{{ $credito->cedula_titular }}</td>
				<td>{{ $credito->celular_titular }}</td>
				<td>{{ $credito->email_titular }}</td>
			</tr>
		</table>
		{{ Form::label('cotitular', 'COTITULAR') }}
		<table class="table table-bordered">
			<tr>
				<th colspan="2">NOMBRES Y APELLIDOS</th>
			</tr>
			<tr>
				<td colspan="2">{{ $credito->cootitulares }}</td>
			</tr>
		</table>
		<table class="table table-bordered">
			<tr>
				<th colspan="2">BENEFICIARIOS</th>
			</tr>
			<tr>
				<td colspan="2">{{ $credito->beneficiarios }}</td>
			</tr>
		</table>
		<table class="table table-bordered">
			<tr>
				<th colspan="2">PRODUCTO SOLICITADO</th>
			</tr>
			<tr>
				<td colspan="2">{{ $credito->producto_solicitado }}</td>
			</tr>
		</table>
		<table class="table table-bordered">
            <tr>
                <th colspan="2">SERVICIO AL CLIENTE</th>
            </tr>
            <tr>
                <td colspan="2">
                    <ul>
                        @foreach($servicio_cliente as $sc)
                        <li>{{$sc}}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        </table>
        <table class="table table-bordered">
            <tr>
                <th colspan="2">OBSERVACIONES</th>
            </tr>
            <tr>
                <td colspan="2">
                    <ul>
                        @foreach($observaciones as $ob)
                        <li>{{$ob}}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        </table>
	    {{ Form::label('cartera', 'CARTERA') }}
		<table class="table table-bordered">
			<tr>
				<th>#</th>
				<th>FECHA</th>
				<th>ABONO</th>
			</tr>
			@foreach($abonos as $i => $abono)
			<tr>
				<td>{{ ($i + 1) }}</td>
				<td>{{ substr($abono->created_at, 0, 10) }}</td>
				<td>$ {{ number_format($abono->valor, 0, ',', '.') }}</td>
			</tr>
			@endforeach
		</table>

        <table class="table table-bordered">
            <tr>
                <th colspan="2">SALDO</th>
            </tr>
            <tr>
                <td colspan="2">
                    $ <?= number_format($credito->saldo, 0, ',', '.') ?>
                </td>
            </tr>
        </table>
</div><!-- row -->
@stop