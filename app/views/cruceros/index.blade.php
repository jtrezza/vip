@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Cruceros <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/cruceros')}}"><i class="icon-dashboard"></i> Cruceros</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.cruceros.create') }}" class="btn btn-primary">Nuevo crucero</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Nombre</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($cruceros as $crucero)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$crucero->title}}</td>
				<td><a href="{{ route('panel.cruceros.edit', $crucero->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($crucero, array('route' => array('panel.cruceros.destroy', $crucero->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $cruceros->links() }}
</div>
@stop