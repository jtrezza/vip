<div class="row">
	<div class="col-md-5">
		
	</div>
	<div class="col-md-7">
		<h3>{{$c->title}}</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-5">
		<a href="{{url('uploads',$c->picture)}}" data-toggle="lightbox" data-title="{{$c->title}}" title="Click para ampliar">
			<img class="thumbnail" style="max-width:260px; margin:auto;" src="{{url('uploads',$c->picture)}}">
		</a>
	</div>
	<div class="col-md-7">
		<p style="text-align:justify">{{$c->short}}</p>
	</div>
</div>
{{-- <div class="blog-item">
    <img class="img-responsive img-blog" src="{{url('uploads',$c->picture)}}" width="100%" alt="" />
    <div class="blog-content">
        <a href="blog-item.html"><h3>{{$c->title}}</h3></a>
        <p>{{$c->short}}</p>
        <a class="btn btn-default" href="blog-item.html">Más info. <i class="icon-angle-right"></i></a>
    </div>
</div> --}}