@extends('admin.layout')
<?php
    if ($crucero->exists):
        $form_data = array('route' => array('panel.cruceros.update', $crucero->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.cruceros.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Crucero <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/cruceros')}}"><i class="icon-dashboard"></i> Cruceros</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($crucero,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('title', 'Título') }}
	     	{{ Form::text('title', null, array('placeholder' => 'Nombre', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      {{ Form::label('short', 'Descripción') }}
	      {{ Form::textarea('short', null, array('placeholder' => 'Descripción.', 'class' => 'form-control')) }}        
	    </div>
	    <div class="form-group">
            <label for="picture">Imagen</label>
            {{Form::file('picture',array('id'=>'picture'));}}
            <p class="help-block">Ésta es la imagen que acompañará la sección del Crucero.</p>
        </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop