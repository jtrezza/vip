@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Usuario <small>cambiar contraseña</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel')}}"><i class="icon-dashboard"></i> panel</a></li>
      <li class="active"><i class="icon-file-alt"></i> cambiar contraseña</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::open(array('url' => '/panel/cambiar_contrasena')) }}
	    <div class="form-group">
	      	{{ Form::label('contrasena_anterior', 'Contraseña anterior') }}
	     	{{ Form::password('contrasena_anterior', array('placeholder' => 'Contraseña anterior', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('nueva_contrasena', 'Nueva contraseña') }}
	     	{{ Form::password('nueva_contrasena', array('placeholder' => 'Nueva contraseña', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('nueva_contrasena_confirmation', 'Confirmar contraseña') }}
	     	{{ Form::password('nueva_contrasena_confirmation', array('placeholder' => 'Confirmar contraseña', 'class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop