<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>VIP Tours - Inicio de sesión</title>
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="container" style="margin-top:30px">
            <div class="col-md-4 col-md-offset-4">
                <h1 class="text-center">VIP tours <br/> <small>Panel de administración</small></h1>
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Iniciar sesión</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(array('url' => '/login')) }}
                            <fieldset>
                                <div class="form-group">
                                    {{ Form::label('usuario', 'Nombre de usuario') }}
                                    {{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Usuario', 'autofocus'=>"")); }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('contraseña', 'Contraseña') }}
                                    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Contraseña', 'autofocus'=>"")); }}
                                </div>
                                <div class="checkbox">
                                    <label for="rememberme">
                                        {{ Form::checkbox('rememberme', true) }}Recordar contraseña
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                {{ Form::submit('Enviar', array('class'=>'btn btn-success')) }}
                                @if(Session::has('mensaje_error'))
                                    <br/><span>&nbsp;</span>
                                    <div class="alert alert-danger">
                                        {{ Session::get('mensaje_error') }}
                                    </div>
                                @endif
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>