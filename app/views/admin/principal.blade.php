@extends('admin.layout')
<?php
    if ($datos->exists):
        $form_data = array('route' => array('panel.update', $datos->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Página principal <small>datos</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel')}}"><i class="icon-dashboard"></i> Página principal</a></li>
      <li class="active"><i class="icon-file-alt"></i> datos</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($datos,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('titulo_uno', 'Título 1') }}
	     	{{ Form::text('titulo_uno', null, array('placeholder' => 'Título 1', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('descripcion_uno', 'Descripción 1') }}
	     	{{ Form::text('descripcion_uno', null, array('placeholder' => 'Descripción 1', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('titulo_dos', 'Título 2') }}
	     	{{ Form::text('titulo_dos', null, array('placeholder' => 'Título 2', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('detalle_dos', 'Descripción 2') }}
	     	{{ Form::text('detalle_dos', null, array('placeholder' => 'Descripción 2', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('titulo_tres', 'Título 3') }}
	     	{{ Form::text('titulo_tres', null, array('placeholder' => 'Título 3', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('detalle_tres', 'Descripción tres') }}
	     	{{ Form::text('detalle_tres', null, array('placeholder' => 'Descripción 3', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('url_video', 'URL vídeo') }}
	     	{{ Form::text('url_video', null, array('placeholder' => 'URL del vídeo de YouTube', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('ver_mas', 'Ver más') }}
	     	{{ Form::textarea('ver_mas', null, array('placeholder' => 'Texto de la sección "Ver más"', 'class' => 'form-control')) }}
	    </div>
	    <div class="form-group">
	      	{{ Form::label('email_notificaciones', 'e-mail para notificaciones') }}
	     	{{ Form::text('email_notificaciones', null, array('placeholder' => 'e-maio donde recibirá las solicitudes de seguro y mensajes de contacto"', 'class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop