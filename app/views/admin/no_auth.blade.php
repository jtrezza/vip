<!DOCTYPE html>
<html>
<head>
	<title>Acceso no autorizado</title>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
</head>
<body>
	<div class="panel panel-primary" style="margin:3em;">
		<div class="panel-heading">
			<h3 class="panel-title">Acceso no autorizado</h3>
		</div>
		<div class="panel-body">Puede que su sesión haya caducado, o no tenga autorización para ver esta página.
		<a href="{{url('panel/logout')}}" class="btn btn-primary" style="float:right;">Ir al inicio</a>
		</div>
	</div>
</body>
</html>