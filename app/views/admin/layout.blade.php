<?php
  $activo = isset($activo) ? $activo : 'index';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VIP Tours Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/datepicker.css')}}" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="{{asset('assets/css/sb-admin.css')}}" rel="stylesheet">
    <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('panel')}}">VIP Tours Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li id="menu-empresa" class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bar-chart-o"></i> Empresa<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <?php if(Auth::user()->role_id == 1): ?>
                <li><a href="{{ route('panel.usuarios.index') }}">Usuarios</a></li>
                <li><a href="{{ route('panel.oficinas.index') }}">Oficinas</a></li>
                <li><a href="{{ route('panel.closers.index') }}">Closers</a></li>
                <?php endif; ?>
                <li><a href="{{ route('panel.creditos.index') }}">Créditos</a></li>
                <li><a href="{{ url('panel/search_by_doc') }}">Búsqueda</a></li>
                <?php if(Auth::user()->role_id == 1): ?>
                <li><a href="{{ url('panel/nomina') }}">Nómina</a></li>
                <?php endif; ?>
              </ul>
            </li>
            <?php if(Auth::user()->role_id == 1): ?>
            <li id="menu-planes"><a href="{{ route('panel.planes.index') }}"><i class="fa fa-table"></i> Planes turísticos</a></li>
            <li id="menu-cruceros"><a href="{{ route('panel.cruceros.index') }}"><i class="fa fa-anchor"></i> Cruceros</a></li>
            <li id="menu-promociones"><a href="{{ route('panel.promociones.index') }}"><i class="fa fa-money"></i> Promociones</a></li>
            <?php endif; ?>
            <!--<li id="menu-seguro"><a href="bootstrap-elements.html"><i class="fa fa-plus"></i> Seguro de viaje</a></li>

            <li id="menu-contacto"><a href="blank-page.html"><i class="fa fa-envelope-o"></i> Contacto</a></li>-->
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{Auth::user()->name}} <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{url('/panel/cambiar_contrasena')}}"><i class="fa fa-user"></i> Cambiar contraseña</a></li>
                <li class="divider"></li>
                <li><a href="{{url('/panel/logout')}}"><i class="fa fa-power-off"></i> Cerrar sesión</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">
        @yield('content')
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="{{asset('assets/js/jquery-2.1.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/js/global.js')}}"></script>
    <script type="text/javascript">
      (function(){
        $("#menu-{{$activo}}").addClass('active');
      })();
    </script>
  </body>
</html>