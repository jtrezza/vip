@extends('admin.layout')
<?php
    if ($oficina->exists):
        $form_data = array('route' => array('panel.oficinas.update', $oficina->id), 'method' => 'PATCH', 'files' => true);
    else:
        $form_data = array('route' => 'panel.oficinas.store', 'method' => 'POST', 'files' => true);
    endif;

?>
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Oficina <small>{{ $action }}</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/oficinas')}}"><i class="icon-dashboard"></i> Oficinas</a></li>
      <li class="active"><i class="icon-file-alt"></i> {{ $action }}</li>
    </ol>
  </div>
</div><!-- /.row -->
<div>
	@include ('errors', array('errors' => $errors)) 
</div>
<div>
	{{ Form::model($oficina,$form_data, array('role' => 'form')) }}
	    <div class="form-group">
	      	{{ Form::label('nombre', 'Nombre') }}
	     	{{ Form::text('nombre', null, array('placeholder' => 'Nombre de la oficina', 'class' => 'form-control')) }}
	    </div>
	  {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
	  
	{{ Form::close() }}
</div><!-- row -->
@stop