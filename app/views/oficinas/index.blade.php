@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Oficina <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/oficinas')}}"><i class="icon-dashboard"></i> Oficinas</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.oficinas.create') }}" class="btn btn-primary">Crear una nueva oficina</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Nombre</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($oficinas as $oficina)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$oficina->nombre}}</td>
				<td><a href="{{ route('panel.oficinas.edit', $oficina->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($oficina, array('route' => array('panel.oficinas.destroy', $oficina->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $oficinas->links() }}
</div>
@stop