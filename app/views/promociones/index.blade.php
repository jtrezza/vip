@extends('admin.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1>Promociones <small>Listado</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{url('panel/promociones')}}"><i class="icon-dashboard"></i> Promociones</a></li>
      <li class="active"><i class="icon-file-alt"></i> Listado</li>
    </ol>
  </div>
</div><!-- /.row -->
<p>
	<a href="{{ route('panel.promociones.create') }}" class="btn btn-primary">Nueva promoción</a>
</p>
<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<th>#</th>
			<th>Nombre</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php $num = 0; ?>
			@foreach($promociones as $promocion)
			<?php $num++; ?>
			<tr>
				<td>{{$num}}</td>
				<td>{{$promocion->title}}</td>
				<td><a href="{{ route('panel.promociones.edit', $promocion->id) }}" class="btn btn-info">Editar</a></td>
				<td>
					{{ Form::model($promocion, array('route' => array('panel.promociones.destroy', $promocion->id), 'method' => 'DELETE', 'role' => 'form')) }}
        			{{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
        			{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{ $promociones->links() }}
</div>
@stop