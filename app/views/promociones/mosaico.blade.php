<div class="row">
	<div class="col-md-5">
		
	</div>
	<div class="col-md-7">
		<h3>{{$p->title}}</h3>
	</div>
</div>
<div class="row">
	<div class="col-md-5">
		<a href="{{url('uploads',$p->picture)}}" data-toggle="lightbox" data-title="{{$p->title}}" title="Click para ampliar">
			<img class="thumbnail" style="max-width:260px; margin:auto;" src="{{url('uploads',$p->picture)}}">
		</a>
	</div>
	<div class="col-md-7">
		<p style="text-align:justify">{{$p->description}}</p>
	</div>
</div>