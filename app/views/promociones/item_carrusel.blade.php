<div class="col-xs-4">
    <div class="portfolio-item">
        <div class="item-inner">
            <img class="img-responsive" src="{{url('uploads',$p->picture)}}" alt="">
            <h5>
                {{$p->title}}
            </h5>
            <div class="overlay">
                <a class="preview btn btn-danger" title="Ir a promociones" href="{{url('promociones')}}" rel="prettyPhoto"><i class="icon-eye-open"></i></a>
            </div>
        </div>
    </div>
</div>