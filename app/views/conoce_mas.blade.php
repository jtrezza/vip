@extends('layout')
@section('content')
<?php
    $datos = PaginaPrincipal::find(1);
?>
<section id="title" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h1>Conoce más</h1>
                </div>
            </div>
        </div>
    </section><!--/#title-->
<div style="padding:4em 2em 2em 2em;">
	<div class="row">
		<div class="col-sm-8">
			<div class="blog">
                <p style="text-align: justify;">{{$datos->ver_mas}}</p>
            </div>
		</div>
		<div class="col-md-4 aside-der" style="padding-left:3em;">
			@include('aside')
		</div>
	</div>
	<div class="row" style="padding-left:4em;">
	</div>
</div>
@stop