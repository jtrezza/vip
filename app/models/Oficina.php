<?php

class Oficina extends Eloquent{
	public $errors;
	protected $table = 'oficinas';
	protected $fillable = array('nombre');

	public function isValid($data)
    {
        $rules = array(
        	'nombre'		=> 'required|max:255'
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

    public static function oficinasArray()
    {
        $oficinas = Oficina::orderBy('nombre')->get();
        $oficinas_a = array();
        foreach($oficinas as $o){
            $oficinas_a[$o->id] = $o->nombre;
        }

        return $oficinas_a;
    }
}