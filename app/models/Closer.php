<?php

class Closer extends Eloquent{
	public $errors;
	protected $table = 'closers';
	protected $guarded = array('id','created_at','updated_at');

	public function isValid($data)
    {
        $rules = array(
        	'cedula'		=> 'required|max:255',
            'nombre' 	    => 'required|max:255',
            'oficina_id' 	=> 'required',
            'porcentaje_100_mayor_seis' => 'required|numeric',
            'porcentaje_100_mayor_cuatro' => 'required|numeric',
            'porcentaje_100_mayor_uno_cuatro_cuarenta' => 'required|numeric',
            'porcentaje_50' => 'required|numeric',
            'tipo' =>       'required'
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

    public function oficina()
    {
        return $this->belongsTo('Oficina','oficina_id');
    }

    public static function closersArray()
    {
        $closers = Closer::orderBy('nombre')->get();
        $closers_a = array();
        foreach($closers as $c){
            $closers_a[$c->id] = $c->nombre;
        }

        return $closers_a;
    }
}