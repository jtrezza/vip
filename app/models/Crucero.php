<?php

class Crucero extends Eloquent{
	public $errors;
	protected $table = 'cruceros';
	protected $fillable = array('title','short','large','picture');

	public function isValid($data)
    {
        $rules = array(
        	'title'		=> 'required|max:255',
            'short' 	=> 'required',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }
}