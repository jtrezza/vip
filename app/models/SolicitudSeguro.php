<?php

class SolicitudSeguro extends Eloquent{
	public $errors;
	protected $table = 'solicitudes_seguro';
	protected $fillable = array('nombre','cedula','telefono','email','ciudad','numero_tiquete',
        'vuelo','fecha_salida','fecha_regreso');

	public function isValid($data)
    {
        $rules = array(
        	'nombre'		    => 'required|max:255',
            'cedula'            => 'required|max:255',
            'telefono'          => 'required|max:255',
            'email'             => 'email|max:255',
            'ciudad'            => 'required|max:255',
            'numero_tiquete'    => 'required|max:255',
            'vuelo'             => 'required|max:255',
            'fecha_salida'      => 'required|date',
            'fecha_regreso' 	=> 'required|date',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }
}