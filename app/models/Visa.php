<?php

class Visa
{
	private static $array_paises = array(
			'alemania'=>'Alemania',
			'andorra'=>'Andorra',
			'aruba'=>'Aruba',
			'australia'=>'Australia',
			'bahamas'=>'Bahamas',
			'belgica'=>'Bélgica',
			'brasil'=>'Brasil',
			'bulgaria'=>'Bulgaria',
			'canada'=>'Canadá',
			'china'=>'China',
			'costa_rica'=>'Costa Rica',
			'croacia'=>'Croacia',
			'cuba'=>'Cuba',
			'dominica'=>'Dominica',
			'ecuador'=>'Ecuador',
			'egipto'=>'Egipto',
			'el_salvador'=>'El salvador',
			'emiratos_arabes_unidos'=>'Emiratos Árabes Unidos',
			'espana'=>'España',
			'estados_unidos'=>'Estados Unidos de América',
			'filipinas'=>'Filipinas',
			'francia'=>'Francia',
			'georgia'=>'Georgia',
			'grecia'=>'Grecia',
			'guatemala'=>'Guatemala',
			'holanda'=>'Holanda',
			'honduras'=>'Honduras',
			'hungria'=>'Hungría',
			'india'=>'India',
			'israel'=>'Israel',
			'italia'=>'Italia',
			'jamaica'=>'Jamaica',
			'japon'=>'Japón',
			'jordania'=>'Jordania',
			'luxemburgo'=>'Luxemburgo',
			'marruecos'=>'Marruecos',
			'mexico'=>'México',
			'nicaragua'=>'Nicaragua',
			'noruega'=>'Noruega',
			'nueva_zelanda'=>'Nueva Zelanda',
			'panama'=>'Panamá',
			'reino_unido'=>'Reino Unido',
			'republica_checa'=>'República Checa',
			'republica_dominicana'=>'República Dominicana',
			'rumania'=>'Rumania',
			'rusia'=>'Rusia',
			'san_vicente_y_las_granadinas'=>'San Vicente y las Granadinas',
			'singapur'=>'Singapur',
			'siria'=>'Siria',
			'sudafrica'=>'Sudáfrica',
			'suecia'=>'Suecia',
			'suiza'=>'Suiza',
			'tailandia'=>'Tailandia',
			'taiwan'=>'Taiwan',
			'turquia'=>'Turquía',
			'venezuela'=>'Venezuela',
			);

	public static function paisesArray()
	{
		return self::$array_paises;
	}

	public static function getNombrePais($key)
	{
		return self::$array_paises[$key];
	}
}