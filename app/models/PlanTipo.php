<?php

class PlanTipo extends Eloquent{

	 protected $table = 'planes_tipos';

	 public static function planesTiposArray()
	 {
	 	$planes_tipos = PlanTipo::all();
	 	$planes_tipos_a = array();
		foreach($planes_tipos as $a){
			$planes_tipos_a[$a->id] = $a->descripcion;
		}
		
		return $planes_tipos_a;
	 }
}