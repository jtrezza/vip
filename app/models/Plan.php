<?php

class Plan extends Eloquent{
	public $errors;
	protected $table = 'planes';
	protected $fillable = array('title','short','large','picture','plan_tipo_id');

	public function isValid($data)
    {
        $rules = array(
        	'title'		=> 'required|max:255',
            'short' 	=> 'required',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

    public function plan_tipo()
    {
        return $this->belongsTo('PlanTipo','plan_tipo_id');
    }
}