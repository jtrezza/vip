<?php

class Promocion extends Eloquent{
	public $errors;
	protected $table = 'promociones';
	protected $fillable = array('title','description','picture');

	public function isValid($data)
    {
        $rules = array(
        	'title'		=> 'required|max:255',
            'description' 	=> 'required',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }
}