<?php

class SeccionEmpresa extends Eloquent{
	public $errors;
	protected $table = 'secciones_empresa';
	protected $fillable = array('title','text','picture');

	public function isValid($data)
    {
        $rules = array(
        	'title'		=> 'required|max:255',
            'text' 	=> 'required',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }
}