<?php

class Abono extends Eloquent{

	public $errors;
	protected $table = 'abonos';
	protected $fillable = array('credito_id', 'valor', 'nuevo_saldo');

	public function isValid($data)
    {
        $rules = array(
        	'credito_id'	=> 'required|integer',
            'valor' 	    => 'required|integer',
            'nuevo_saldo' 	=> 'required|integer',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

    public function credito()
    {
        return $this->belongsTo('Credito','credito_id');
    }
}