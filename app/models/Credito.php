<?php

class Credito extends Eloquent{
	public $errors;
	protected $table = 'creditos';
	protected $fillable = array('numero','fecha','gl','valor_contrato','saldo','nombres_titular','apellidos_titular',
        'cedula_titular','cootitulares','direccion_titular','telefono_titular','celular_titular','email_titular',
        'vigencia', 'duracion', 'producto_solicitado', 'liner', 'closer_id', 'beneficiarios', 'estado', 'user_id',
        'servicio_cliente', 'observaciones', 'devolucion');

	public function isValid($data)
    {
        $rules = array(
        	'numero'		    => 'required|max:255',
            'gl' 	            => 'required|integer',
            'valor_contrato' 	=> 'required|integer',
            'nombres_titular'   => 'required|max:255',
            'apellidos_titular'	=> 'required|max:255',
            'beneficiarios'     => 'max:255',
            'cedula_titular'	=> 'required|max:255',
            'closer_id'	        => 'required|integer',
            'primer_abono'      => 'numeric'
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

    public function closer()
    {
        return $this->belongsTo('Closer','closer_id');
    }

    public function user()
    {
        return $this->belongsTo('User','user_id');
    }
}