<?php

class PaginaPrincipal extends Eloquent{
	public $errors;
	protected $table = 'pagina_principal';
	protected $fillable = array('titulo_uno','descripcion_uno','ver_mas','titulo_dos','detalle_dos',
        'titulo_tres','detalle_tres','url_video', 'email_notificaciones');

}