<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $fillable = array('name','username','email', 'role_id', 'oficina_id','password');

	public function isValid($data)
    {
        $rules = array(
        	'username'		=> 'required|max:255|unique:users',
            'email' 	    => 'required|email|max:255|unique:users',
            'role_id' 		=> 'required|integer',
            'oficina_id'	=> 'required|integer',
            'name'			=> 'required|max:255'
        );

        if ($this->exists)
        {
			$rules['email'] 	.= ',email,' . $this->id;
			$rules['username'] 	.= ',username,' . $this->id;
        }
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function setPasswordAttribute($value)
    {
        if ( ! empty ($value))
        {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function oficina()
    {
        return $this->belongsTo('Oficina','oficina_id');
    }
}
