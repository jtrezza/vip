<?php

class Documentacion extends Eloquent{
	public $errors;
	protected $table = 'documentacion';
	protected $fillable = array('text');

	public function isValid($data)
    {
        $rules = array(
            'text' 	=> 'required',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }
}