<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Las contraseñas deben tener al menos seis carácteres y coincidir con el campo de confirmación.",

	"user" => "No se pudo encontrar un usuario con esta dirección de correo.",

	"token" => "El token de restauración de contraseña es inválido.",

	"sent" => "Recordatorio de contraseña enviado.",

);
