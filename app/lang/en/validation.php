<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "El campo :attribute debe ser aceptado.",
	"active_url"           => "El campo :attribute no es una URL válida.",
	"after"                => "El campo :attribute debe ser una fecha posterior a :date.",
	"alpha"                => "El campo :attribute sólo puede contener letras.",
	"alpha_dash"           => "El campo :attribute sólo puede contener letras, números y dashes.",
	"alpha_num"            => "El campo :attribute sólo puede contener letras y números.",
	"array"                => "El campo :attribute debe ser un array.",
	"before"               => "El campo :attribute debe ser una fecha anterior a :date.",
	"between"              => array(
		"numeric" => "El campo :attribute debe estar entre :min y :max.",
		"file"    => "El campo :attribute debe estar entre :min y :max kilobytes.",
		"string"  => "El campo :attribute debe estar entre :min y :max characters.",
		"array"   => "El campo :attribute debe estar entre :min y :max items.",
	),
	"confirmed"            => "El campo :attribute y su confirmación no coinciden.",
	"date"                 => "El campo :attribute no es una fecha válida.",
	"date_format"          => "El campo :attribute no coincide con el formato :format.",
	"different"            => "El campo :attribute y :other deben ser diferentes.",
	"digits"               => "El campo :attribute debe ser de :digits dígitos.",
	"digits_between"       => "El campo :attribute debe tener entre :min y :max dígitos.",
	"email"                => "El campo :attribute debe ser una dirección de email válida.",
	"exists"               => "El campo seleccionado en :attribute es inválido.",
	"image"                => "El campo :attribute debe ser una imágen.",
	"in"                   => "El campo selected :attribute es inválido.",
	"integer"              => "El campo :attribute debe ser un entero.",
	"ip"                   => "El campo :attribute debe ser una dirección IP válida.",
	"max"                  => array(
		"numeric" => "El campo :attribute no puede ser mayor a :max.",
		"file"    => "El campo :attribute no puede ser mayor a :max kilobytes.",
		"string"  => "El campo :attribute no puede ser mayor a :max carácteres.",
		"array"   => "El campo :attribute no puede ser mayor a :max elementos.",
	),
	"mimes"                => "El campo :attribute must be a file of type: :values.",
	"min"                  => array(
		"numeric" => "El campo :attribute must debe medir al menos :min.",
		"file"    => "El campo :attribute must debe ocupar al menos :min kilobytes.",
		"string"  => "El campo :attribute must debe ser de al menos :min carácteres.",
		"array"   => "El campo :attribute must debe contener al menos :min elementos.",
	),
	"not_in"               => "El :attribute seleccionado es inválido.",
	"numeric"              => "El :attribute debe ser un número.",
	"regex"                => "El formato de :attribute es inválido.",
	"required"             => "El campo :attribute es requerido.",
	"required_if"          => "El campo :attribute es requerido cuando :other es :value.",
	"required_with"        => "El campo :attribute es requerido cuando :values está presente.",
	"required_with_all"    => "El campo :attribute es requerido cuando :values está presente.",
	"required_without"     => "El campo :attribute es requerido cuando :values no está presente.",
	"required_without_all" => "El campo :attribute es requerido cuando ninguno de :values está presente.",
	"same"                 => "El campo :attribute y :other deben coincidir.",
	"size"                 => array(
		"numeric" => "El campo :attribute debe ser :size.",
		"file"    => "El campo :attribute debe ocupar :size kilobytes.",
		"string"  => "El campo :attribute debe ser de :size characters.",
		"array"   => "El campo :attribute debe contener :size items.",
	),
	"unique"               => "El campo :attribute ya ha sido tomado.",
	"url"                  => "El campo :attribute tiene un formato inválido.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
