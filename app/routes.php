<?php

// Nos mostrará el formulario de login.
Route::get('login', 'AuthController@showLogin');

// Validamos los datos de inicio de sesión.
Route::post('login', 'AuthController@postLogin');
Route::get('/panel/logout', 'AuthController@logOut');
Route::get('prueba', function(){
	return Hash::make('admin');
});
Route::get('/', function()
{
	return View::make('home', array(
		'activo'=>'inicio'
	));
});

Route::post('documentacion_pais', function()
{
	$pais = Input::get('pais');

	if (!View::exists('visas/'.$pais))
	{
	    App::abort(404);
	}

	return View::make('documentacion_pais', array(
		'pais'=>$pais,
		'nombre_pais'=>Visa::getNombrePais($pais)
	));
});

Route::get('empresa', function()
{
	$secciones_empresa = SeccionEmpresa::orderBy('id')->get();
	return View::make('empresa', array(
		'secciones_empresa'=>$secciones_empresa,
		'activo'=>'empresa'
	));
});
Route::get('/planes/nacionales', function()
{
	$planes = Plan::with('plan_tipo')->where('plan_tipo_id', 1)->paginate(6);
	return View::make('planes_nacionales', array(
		'planes'=> $planes,
		'activo'=>'planes'
	));
});
Route::get('/planes/internacionales', function()
{
	$planes = Plan::with('plan_tipo')->where('plan_tipo_id', 2)->paginate(6);
	return View::make('planes_internacionales', array(
		'planes'=> $planes,
		'activo'=>'planes'
	));
});
Route::get('/cruceros', function()
{
	$cruceros = Crucero::paginate(4);
	return View::make('cruceros', array(
		'cruceros'=> $cruceros,
		'activo'=>'cruceros'
	));
});
Route::get('/promociones', function()
{
	$promociones = Promocion::paginate(4);
	return View::make('promociones', array(
		'promociones'=> $promociones,
		'activo'=>'promociones'
	));
});
Route::resource('/seguro', 'SolicitudSeguroController');
Route::resource('/contacto', 'ContactoController');

Route::get('seguro_success', function(){
	return View::make('seguro_registrado', array(
		'activo'=>'seguro'
	));
});
Route::get('documentacion', function()
{
	return View::make('documentacion', array(
		'activo'=>'documentacion',
		'array_paises'=>Visa::paisesArray()
	));
});
Route::get('conoce_mas', function()
{
    return View::make('conoce_mas', array(
        'activo'=>'conoce_mas'
    ));
});

Route::get('no_auth', function()
{
	return View::make('admin/no_auth');
});

Route::group(array('before' => 'auth'), function()
{
	Route::get('/panel/cambiar_contrasena', 'AuthController@changePassword');
    Route::post('/panel/cambiar_contrasena', 'AuthController@updatePassword');

    Route::resource('/panel/creditos', 'CreditoController');
    Route::get('panel/search_by_doc', 'CreditoController@searchByDoc');
    Route::post('panel/search_by_doc_result', 'CreditoController@searchByDocResult');
    Route::get('panel/nuevo_pago/{id_credito}', function($id_credito)
    {
        $abono = new Abono();
        return View::make('creditos/nuevo_pago', array(
            'activo'=>'empresa',
            'id_credito'=>$id_credito,
            'abono'=>$abono
        ));
    });

    Route::resource('/panel/abonos', 'AbonoController');
});
Route::group(array('before' => 'auth|roles:1,no_auth'), function()
{
    Route::get('panel/nomina', 'CreditoController@searchNomina');
    Route::get('panel/closers_by_office', 'CreditoController@closersByOffice');
    Route::post('panel/search_nomina_result', 'CreditoController@searchNominaResult');

    Route::resource('panel/usuarios', 'UserController');
    Route::resource('/panel/planes', 'PlanController');
    Route::resource('/panel/oficinas', 'OficinaController');
    Route::resource('/panel/closers', 'CloserController');
    Route::resource('/panel/cruceros', 'CruceroController');
    Route::resource('/panel/promociones', 'PromocionController');
    Route::resource('/panel/documentacion', 'DocumentacionController');
    Route::resource('/panel', 'PaginaPrincipalController');

});

Route::group(array('before' => 'auth|roles:2-3,no_auth'), function()
{
	Route::get('ventas', function()
    {
        return View::make('admin/ventas', array(
            'activo'=>'empresa',
        ));
    });
});